<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DepartmentTranslation extends Model
{
    /**
     * Table name.
     *
     * @var string
     */
    protected $table = 'departments_translations';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'department_trans_id';

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Timestamps.
     *
     * @var boolean
     */    public $timestamps = false;


}
