<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ServiceSubcategory extends Model
{
    use HelperTrait,Translatable;
    protected $table='services_subcategories';
    protected $fillable=['service_category_id', 'image','status'];
    public $translatedAttributes =  ['name'];
    public function serviceCategory()
    {
        return $this->belongsTo('App\Models\ServiceCategory','service_category_id');
    }


    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/service_subcategories/'.$value);
        } else {
            return asset('uploads/service_subcategories/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/service_subcategories/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }

//    public function getImageAttribute(){
//
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('service_subcategories') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('service_subcategories') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file){
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'service_subcategories/original');
//            $this->mediumImage($file, $fileName,150,150,'service_subcategories/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'service_subcategories/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//
//
//
//    }
}
