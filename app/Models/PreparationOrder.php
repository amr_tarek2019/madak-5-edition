<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreparationOrder extends Model
{
    protected $table='preparations_order';
    protected $fillable=['preparation_booking_id', 'services_sub_sub_category_id', 'quantity'];

    public function preparationBooking()
    {
        return $this->belongsTo('App\Models\PreparationBooking','preparation_booking_id');
    }

    public function subSubCategory()
    {
        return $this->belongsTo('App\Models\SubSubCategory','services_sub_sub_category_id');
    }
}
