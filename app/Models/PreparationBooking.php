<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PreparationBooking extends Model
{
    protected $table='preparation_booking';
    protected $fillable=['user_id','message', 'date', 'total','status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service','service_id');
    }
}
