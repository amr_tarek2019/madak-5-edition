<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{
    use HelperTrait,Translatable;
    protected $table='sub_subcategories';
    protected $fillable=['service_subcategory_id', 'image','status'];
    public $translatedAttributes =  ['name'];
    public function serviceSubCategory()
    {
        return $this->belongsTo('App\Models\ServiceSubcategory','service_subcategory_id');
    }


    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/service_sub_sub_categories/'.$value);
        } else {
            return asset('uploads/service_sub_sub_categories/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/service_sub_sub_categories/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }

//    public function getImageAttribute(){
//
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('service_sub_sub_categories') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('service_sub_sub_categories') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file){
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'service_subsubcategories/original');
//            $this->mediumImage($file, $fileName,150,150,'service_subsubcategories/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'service_subsubcategories/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//
//    }


}
