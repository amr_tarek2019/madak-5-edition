<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use Translatable;
    protected $table='cities';
    protected $fillable=['status'];
    public $translatedAttributes =  ['name'];
//    public function getCity()
//    {
//        return static::all();
//    }
}
