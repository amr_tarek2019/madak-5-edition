<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class PlaceImage extends Model
{
    use HelperTrait ;
    protected $table='places_images';
    protected $fillable=['place_id', 'image'];

    public function place()
    {
        return $this->belongsTo('App\Models\Place','place_id');
    }

//    public function getImageAttribute(){
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('places') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('places') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file)
//    {
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'places/original');
//            $this->mediumImage($file, $fileName,150,150,'places/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'places/thumbnail');
//            $this->attributes['image'] = $fileName;
//        }
//
//    }
    public function related_images(){

        return $this->hasMany(PlaceImage::class, 'place_id', 'id')->select('image','place_id');
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/place/sliders/'.$value);
        } else {
            return asset('uploads/place/sliders/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/place/sliders/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
}
