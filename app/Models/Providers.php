<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    protected $table='providers';
    protected $fillable=['user_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function related_places(){
        return $this->hasMany('App\Models\Place', 'provider_id')
            ->with('related_images');
    }
    public function related_services(){
        return $this->hasMany('App\Models\Service', 'provider_id')
            ->with('related_images');
    }

    public function related_images(){

        return $this->hasMany(PlaceImage::class, 'place_id', 'id')->select('image','place_id');
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/providers/'.$value);
        } else {
            return asset('uploads/providers/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/providers/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }

//    public function getImageAttribute(){
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('places') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('places') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file)
//    {
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'places/original');
//            $this->mediumImage($file, $fileName,150,150,'places/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'places/thumbnail');
//            $this->attributes['image'] = $fileName;
//        }
//
//    }
}
