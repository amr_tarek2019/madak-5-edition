<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChatImage extends Model
{
    use HelperTrait;
    protected $table='chat_images';
    protected $fillable=['chat_id', 'image'];

    public function chat()
    {
        return $this->belongsTo('App\Models\Chat','chat_id');
    }

//    public function getImageAttribute(){
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('chat_images') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('chat_images') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file)
//    {
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'chat_images/original');
//            $this->mediumImage($file, $fileName,150,150,'chat_images/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'chat_images/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//
//    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/chat_images/'.$value);
        } else {
            return '';
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/chat_images/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
}
