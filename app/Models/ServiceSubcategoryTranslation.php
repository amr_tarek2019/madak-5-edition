<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceSubcategoryTranslation extends Model
{
    protected $table = 'service_subcat_translation';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'service_subcat_trans_id';

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;
}
