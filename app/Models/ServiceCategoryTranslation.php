<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceCategoryTranslation extends Model
{
    protected $table = 'services_cat_translations';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'service_cat_trans_id';

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;
}
