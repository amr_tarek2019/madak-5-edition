<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ServiceCategory extends Model
{
    use Translatable,HelperTrait;
    protected $table='services_categories';
    protected $fillable=['image','status'];
    public $translatedAttributes =  ['name'];
    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id');
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/service_categories/'.$value);
        } else {
            return asset('uploads/service_categories/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/service_categories/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }


//    public function getImageAttribute(){
//
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('service_categories') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('service_categories') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file)
//    {
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName, 'service_categories/original');
//            $this->mediumImage($file, $fileName, 150, 150, 'service_categories/meduim');
//            $this->thumbImage($file, $fileName, 70, 70, 'service_categories/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//    }
}
