<?php

namespace App\Models;
use Astrotomic\Translatable\Translatable;


use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{

    use HelperTrait,Translatable;

    protected $table='issues';
    protected $fillable=['status'];

    public $translatedAttributes =  ['text'];
    protected $hidden=['laravel_through_key'];
//    public function getAllIssues()
//    {
//        return static::all();
//    }


}
