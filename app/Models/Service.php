<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HelperTrait;
    protected $table='services';
    protected $fillable=['provider_id', 'service_category_id', 'service_subcategory_id',
'service_sub_sub_category_id','gender','city_id', 'present_type', 'price_before', 'price_after', 'quantity', 'lat', 'lng',
'address', 'date','desc','image',];

    public function provider()
    {
        return $this->belongsTo('App\Models\Providers','provider_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }


    public function serviceCategory()
    {
        return $this->belongsTo('App\Models\ServiceCategory','service_category_id');
    }

    public function serviceSubCategory()
    {
        return $this->belongsTo('App\Models\ServiceSubCategory','service_subcategory_id');
    }

    public function servicesSubSubCategory()
    {
        return $this->belongsTo('App\Models\SubSubCategory','service_sub_sub_category_id');
    }

    public function related_images(){

        return $this->hasMany(ServiceImage::class, 'service_id', 'id')->select('image','place_id');
    }

    public function related_provider(){
        return $this->belongsTo(Providers::class, 'provider_id','provider_id');
    }

    public function related_service_category(){
        return $this->belongsTo('App\Models\ServiceCategory', 'service_category_id','id');
    }

    public function related_service_sub_category(){
        return $this->belongsTo('App\Models\ServiceSubCategory', 'service_subcategory_id','id');
    }

    public function related_service_sub_sub_category(){
        return $this->belongsTo('App\Models\SubSubCategory', 'service_sub_sub_category_id','id');
    }

//    public function getImageAttribute(){
//
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('services') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('services') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file){
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'services/original');
//            $this->mediumImage($file, $fileName,150,150,'services/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'services/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//
//
//
//    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/services/'.$value);
        } else {
            return asset('uploads/services/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/services/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }


    public static function getData($request)
    {
        $queryOne = Service::where('id', '>', 0);


        if ($request->department_id == 2 && $request->city_id) {
            $queryOne->where('city_id', $request->city_id);
        }

        if ($request->department_id == 2 && $request->gender_id) {
            $queryOne->where('gender_id', $request->gender_id);
        }

        if ($request->department_id == 2 && $request->min_price && $request->max_price) {
            $queryOne->where('price', '>=', $request->min_price)->where('price', '<=', $request->max_price);
        }

        return $queryOne->get();
    }

}
