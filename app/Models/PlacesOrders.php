<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlacesOrders extends Model
{
    protected $table='places_orders';
    protected $fillable=['user_id','provider_id','order_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function provider()
    {
        return $this->belongsTo('App\Models\Provider','provider_id');
    }
    public function placeBooking()
    {
        return $this->belongsTo('App\Models\PlaceBooking','order_id');
    }
}
