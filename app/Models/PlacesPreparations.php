<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlacesPreparations extends Model
{
    protected $table='places_preparations';
    protected $fillable=['place_id', 'preparations_id'];

    public function place()
    {
        return $this->belongsTo('App\Models\Place','place_id');
    }
    public function service()
    {
        return $this->belongsTo('App\Models\Service','preparations_id');
    }
}
