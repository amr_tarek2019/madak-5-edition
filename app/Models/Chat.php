<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table='chats';
    protected $fillable=['sender', 'receiver', 'message','place_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function place()
    {
        return $this->belongsTo('App\Models\Place','place_id');
    }

    public function sender(){
        return $this->belongsTo('App\Models\User', 'sender','user_id')->select('user_id');

    }

    public function related_images(){

        return $this->hasMany(ChatImage::class, 'chat_id', 'id')->select('image','chat_id');
    }

    public function getImageAttribute(){
        if($this->attributes['image']!=null) {
            return SiteImages_path('chat_images') . '/original/' . $this->attributes['image'];
        }
        else{

            return SiteImages_path('chat_images') . '/default.png';

        }


    }
    public function setImageAttribute($file)
    {
        if ($file) {
            $fileName = $this->createFileName($file);
            $this->originalImage($file, $fileName,'chat_images/original');
            $this->mediumImage($file, $fileName,150,150,'chat_images/meduim');
            $this->thumbImage($file, $fileName, 70,70,'chat_images/thumbnail');

            $this->attributes['image'] = $fileName;
        }

    }

    public function getMessagesCreatedAtAttribute(){
        return Carbon::createFromTimeStamp(strtotime($this->attributes['messages_created_at']))->diffForHumans();


    }
}
