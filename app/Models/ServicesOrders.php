<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServicesOrders extends Model
{
    protected $table='services_orders';
    protected $fillable=['user_id','provider_id','order_id'];
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function provide()
    {
        return $this->belongsTo('App\Models\Providers','provider_id');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\PreparationBooking','order_id');
    }
}
