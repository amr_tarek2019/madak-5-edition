<?php

namespace App\Models;

use Intervention\Image\Facades\Image;
use Intervention\Image\File;


trait HelperTrait {
	public function createFileName($file)
	{
        $originalName = $file->getClientOriginalExtension();
        $fileName = time().uniqid().'.'.$originalName;

		return $fileName;
	}

	public function saveFile($file, $fileName)
	{
		$file->move(resource_path('assets/files/'), $fileName);
	}

    /**
     * Save different sizes for images
     */
    public function originalImage($file, $current_name,$path)
    {
        $accountOriginalDestination = resource_path('assets/img/'.$path);

        Image::make($file)->save($accountOriginalDestination .'/'. $current_name);
    }

    public function mediumImage($file, $current_name, $width=600, $height=300,$path)
    {
        $accountMediumDestination =resource_path('assets/img/'.$path);

        Image::make($file)->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        })->save($accountMediumDestination .'/'. $current_name);
    }

    public function thumbImage($file, $current_name, $width=100, $height=100,$path)
    {
        $accountThumbnailDestination =resource_path('assets/img/'.$path);

        Image::make($file)->resize($width, $height)->save($accountThumbnailDestination .'/'. $current_name);
    }

}
