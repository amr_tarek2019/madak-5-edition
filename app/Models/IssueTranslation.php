<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IssueTranslation extends Model
{
    protected $table = 'issues_translations';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'issue_trans_id';

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = ['text'];

    /**
     * Timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

}
