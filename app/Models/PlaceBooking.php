<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceBooking extends Model
{
    protected $table='places_booking';
    protected $fillable=['user_id', 'place_id', 'date_from', 'date_to','status'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place','place_id');
    }
}
