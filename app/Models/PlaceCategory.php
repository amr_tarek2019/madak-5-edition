<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PlaceCategory extends Model
{
    use HelperTrait,Translatable;
    protected $table='places_categories';
    protected $fillable=['image','status'];
    public $translatedAttributes =  ['name'];

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/place_categories/'.$value);
        } else {
            return asset('uploads/place_categories/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/place_categories/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }



//    public function getImageAttribute(){
//
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('place_categories') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('place_categories') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file){
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'place_categories/original');
//            $this->mediumImage($file, $fileName,150,150,'place_categories/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'place_categories/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//
//
//
//    }
}
