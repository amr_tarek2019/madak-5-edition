<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use Translatable,HelperTrait;
    protected $table = 'departments';
    protected $fillable=['image'];
    public $translatedAttributes =  ['name'];

//    public function setImageAttribute($file)
//    {
//        if (is_file($file)) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'department/original');
//            $this->mediumImage($file, $fileName,150,150,'department/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'department/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//
//    }
//    public function getImageAttribute()
//    {
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('department') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('department') . '/defaultuser.png';
//
//        }
//    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/department/'.$value);
        } else {
            return asset('uploads/department/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/department/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
}
