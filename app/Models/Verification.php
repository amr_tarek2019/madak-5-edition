<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;
class Verification extends Model
{
    protected $table="verifications";

    protected $primaryKey='id';


    protected $fillable=['verify_code','user_id'];



    public function related_user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');

    }

}
