<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use HelperTrait ;
    protected $table='places';
    protected $fillable=['provider_id', 'place_category_id','gender','city_id',
        'name', 'present_type', 'price', 'quantity', 'date_from',
        'date_to','lat', 'lng', 'address', 'desc','main_image'];

    public function provider()
    {
        return $this->belongsTo('App\Models\Providers','provider_id');
    }

    public function city()
    {
        return $this->belongsTo('App\Models\City','city_id');
    }


    public function placeCategory()
    {
        return $this->belongsTo('App\Models\PlaceCategory','place_category_id');
    }


    public function related_images(){
        return $this->hasMany(PlaceImage::class, 'place_id', 'id')->select('image','place_id');
    }

    public function related_provider(){
        return $this->belongsTo(Providers::class, 'provider_id','provider_id');
    }

    public function related_category(){
        return $this->hasMany('App\Models\PlaceCategory', 'place_category_id','id');
    }

    public function GetImage($place){
        return $place->related_default_image;
    }

//    public function getMainImageAttribute(){
//        if($this->attributes['main_image']!=null) {
//            return SiteImages_path('places/main_image') . '/original/' . $this->attributes['main_image'];
//        }
//        else{
//
//            return SiteImages_path('places/main_image') . '/default.png';
//
//        }
//
//
//    }
//    public function setMainImageAttribute($file)
//    {
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'places/main_image/original');
//            $this->mediumImage($file, $fileName,150,150,'places/main_image/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'places/main_image/thumbnail');
//
//            $this->attributes['main_image'] = $fileName;
//        }
//
//    }
//
//    public function getImageAttribute(){
//        if($this->attributes['image']!=null) {
//            return SiteImages_path('places') . '/original/' . $this->attributes['image'];
//        }
//        else{
//
//            return SiteImages_path('places') . '/default.png';
//
//        }
//
//
//    }
//    public function setImageAttribute($file)
//    {
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'places/original');
//            $this->mediumImage($file, $fileName,150,150,'places/meduim');
//            $this->thumbImage($file, $fileName, 70,70,'places/thumbnail');
//            $this->attributes['image'] = $fileName;
//        }
//
//    }


//    public function ratings(){
//        return $this->hasMany(Rate::class, 'place_id', 'id')->select('rate')->where('place_id',$place_id);
//    }

        public static function getData($request)
        {
            $queryOne = Place::where('id', '>', 0);


            if ($request->department_id == 1 && $request->city_id) {
                $queryOne->where('city_id', $request->city_id);
            }

            if ($request->department_id == 1 && $request->gender_id) {
                $queryOne->where('gender_id', $request->gender_id);
            }

            if ($request->department_id == 1 && $request->min_price && $request->max_price) {
                $queryOne->where('price', '>=', $request->min_price)->where('price', '<=', $request->max_price);
            }

            return $queryOne->get();
        }


    public function getMainImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/place/images/'.$value);
        } else {
            return asset('uploads/department/default.png');
        }
    }

    public function setMainImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/place/images/'),$imageName);
            $this->attributes['main_image']=$imageName;
        }
    }
}
