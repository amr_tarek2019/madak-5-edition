<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlaceCategoryTranslation extends Model
{
    protected $table = 'places_categories_translations';
    protected $primaryKey = 'place_category_trans_id';
    protected $fillable = ['name'];
    public $timestamps = false;
}
