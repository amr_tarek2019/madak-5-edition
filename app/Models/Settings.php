<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table='settings';
    protected $fillable=['app_name','logo','phone'];

    public function getLogoAttribute($value)
    {
        if ($value) {
            return asset('uploads/settings/'.$value);
        } else {
            return asset('uploads/settings/default.png');
        }
    }

    public function setLogoAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/settings/'),$imageName);
            $this->attributes['logo']=$imageName;
        }
    }
}
