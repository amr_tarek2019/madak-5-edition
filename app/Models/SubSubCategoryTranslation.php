<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubSubCategoryTranslation extends Model
{
    protected $table = 'sub_subcategories_trans';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'sub_subcategory_trans_id';

    /**
     * Fillable fields.
     *
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * Timestamps.
     *
     * @var boolean
     */
    public $timestamps = false;

}
