<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PushNotification extends Model
{

    public static function send($tokens, $msg , $type)
    {

        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'data' => [
                'title' => "",
                'sound' => 'default',
                'message' => $msg,
                'type' => $type
            ],
            'notification' => [
                'type'    => $type,
                'text' => $msg,
                'title' => "",
                'sound' => 'default'
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .
            // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
            // 'AAAA1kqMc_o:APA91bHgPRiYdC5e7yDmXEWaxSnmfOQPUXOJGRtIitRiIhlwXsnCzVyOmayal1tRHIygIZKGAoWsyYUNl7PJAAF0qfSRQVsrDJ8GXRRxWpQxKSttXkRGPnXMRyow99ncfUFbVIewF7aBXnJszQsIL5XHgUtSmBw1Aw'
            //'APA91bE_dEDuiJp-nYOZP6urgp7lF7b-83z2Kpk5jqSZAVe0wcodV_9HOjAsUibH-obqBf6siF5ZUnIJFNWb84D4rX-XtyCVEP4JTubgQc5D76HgBhc3wkovAhrd6nMzq3kuF5jSVhtn'
            'AAAAS3iky-0:APA91bFWYORanttwST901dkwz6wB23oaKDhUEo-p5sxAuEwkxizN8U20wOt7KsQhGEmvj3djJKdbVYekEllETFdi1daMQbdROyc0zsdN87c_V7J3yIKjRq34MlvrNoK051C8HD8V6E0I'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }


    public static function send_details($tokens,$title,$message, $details ,$type='')
    {
        if($type==1){
            $array=[
                'title' => $title,
                'body' => $message,
                'chat' => $details,
                'type' => $type,
            ];
        }
        else{
            $array=  [
                'title' => $title,
                'body' => $message,
                'id' => $details,
                'type' => $type,
            ];
        }
        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'data' => $array,
            'notification' => [
//                  'title' => $title,
                'message' => $message,
                'body' => $message,
                'details' => $details,
                'type' => $type,
                'title' => "",
                'sound' => 'default'
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .
            // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
            // 'AAAA1kqMc_o:APA91bHgPRiYdC5e7yDmXEWaxSnmfOQPUXOJGRtIitRiIhlwXsnCzVyOmayal1tRHIygIZKGAoWsyYUNl7PJAAF0qfSRQVsrDJ8GXRRxWpQxKSttXkRGPnXMRyow99ncfUFbVIewF7aBXnJszQsIL5XHgUtSmBw1Aw'
            // 'AAAAXHIOOno:APA91bEUH19-Cw9e80uj_GLYfOHPMfF7Zn4AfBViphboa3Jp8xdsxUgyvDn8ChElA2qQl_S2R-VRw1-InuKSKR9M11sxZ2COuvhBsrNFWO5SEpFF_PiNfgJF5L6t6mT2JKmSjKpx4PlV'
            'AAAAS3iky-0:APA91bFWYORanttwST901dkwz6wB23oaKDhUEo-p5sxAuEwkxizN8U20wOt7KsQhGEmvj3djJKdbVYekEllETFdi1daMQbdROyc0zsdN87c_V7J3yIKjRq34MlvrNoK051C8HD8V6E0I'

        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
    public static function send_details_driver($tokens,$title,$message, $details , $q=null,$type='')
    {

        $fields = array
        (
            "registration_ids" => $tokens,
            "priority" => 10,
            'data' => [
                // 'title' => $msg,
//                  'title' => $title,
                'message' => $message,
                'details' => $details,
                'type' => $type,
                'title' => ""
            ],
            'notification' => [
//                    'title' => $title,
                'message' => $message,
                'body' => $message,
                'details' => $details,
                'type' => $type,
                'title' => ""
            ],
            'vibrate' => 1,
            'sound' => 1
        );
        $headers = array
        (
            'accept: application/json',
            'Content-Type: application/json',
            'Authorization: key=' .
            // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
            // 'AAAA1kqMc_o:APA91bHgPRiYdC5e7yDmXEWaxSnmfOQPUXOJGRtIitRiIhlwXsnCzVyOmayal1tRHIygIZKGAoWsyYUNl7PJAAF0qfSRQVsrDJ8GXRRxWpQxKSttXkRGPnXMRyow99ncfUFbVIewF7aBXnJszQsIL5XHgUtSmBw1Aw'
            'AAAAS3iky-0:APA91bFWYORanttwST901dkwz6wB23oaKDhUEo-p5sxAuEwkxizN8U20wOt7KsQhGEmvj3djJKdbVYekEllETFdi1daMQbdROyc0zsdN87c_V7J3yIKjRq34MlvrNoK051C8HD8V6E0I'

        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        //  var_dump($result);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }
}
