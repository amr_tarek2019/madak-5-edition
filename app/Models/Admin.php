<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
    use Notifiable,HelperTrait,HasRoles;
    protected $guard = 'admin';

    protected $table="admins";

    protected $primaryKey='id';




    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = [
        'name', 'email', 'password','image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','email_verified_at','created_at','updated_at'
    ];
//    public function setImageAttribute($file)
//    {
////        dd($file);
//        if ($file) {
//            $fileName = $this->createFileName($file);
//            $this->originalImage($file, $fileName,'admin/original');
//            $this->mediumImage($file, $fileName,150,150,'admin/medium');
//            $this->thumbImage($file, $fileName, 50,50,'admin/thumbnail');
//
//            $this->attributes['image'] = $fileName;
//        }
//
//    }
//    public function getImageAttribute(){
//        if($this->attributes['image']!=null){
//            return $this->attributes['image'];
//        }
//        else{
//            return 'default.png';
//        }
//    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/admin/'.$value);
        } else {
            return asset('uploads/admin/default.png');
        }
    }

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/admin/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = Hash::make($password);
    }

}
