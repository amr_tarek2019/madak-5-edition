<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    protected $table='ratings';
    protected $fillable=['user_id', 'place_id', 'rate', 'review'];

    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }

    public function place()
    {
        return $this->belongsTo('App\Models\Place','place_id');
    }

    public function getCreatedAtAttribute(){
        return Carbon::createFromTimeStamp(strtotime($this->attributes['created_at']))->diffForHumans();
    }
}
