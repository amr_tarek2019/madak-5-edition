<?php

namespace App\Modules\Department\Http\Controllers\Api;

use App\Modules\Department\Http\Interfaces\DepartmentRepositoryInterface;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    protected $departmentObject;
    public function __construct(DepartmentRepositoryInterface $departmentRepository)
    {
        $this->departmentObject = $departmentRepository;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getDepartments(){
        $lang=getallheaders()['Lang'];
        $result= $this->departmentObject->getDepartments($lang);
        return response(res_msg($lang, success(),200,'all_departments', $result));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
