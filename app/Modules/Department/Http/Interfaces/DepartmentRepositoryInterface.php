<?php
namespace App\Modules\Department\Http\Interfaces;

interface DepartmentRepositoryInterface
{
    public function getDepartments($lang);
}



