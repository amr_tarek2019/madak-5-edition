<?php
namespace App\Modules\Department\Http\Eloquent;

use App\Models\Department;
use App\Modules\Department\Http\Interfaces\DepartmentRepositoryInterface;
use Illuminate\Support\Facades\Validator;

class DepartmentRepository implements DepartmentRepositoryInterface
{
    public function getDepartments($lang)
    {
        $departments = Department::all();
        $alldepartments = array();
        $i = 0;
        foreach ($departments as $department) {

            $alldepartments[$i] = array(
                'id'=>$department->id,
                'name' => $department->translate($lang)->name,
                'image'=>$department->image);
            $i++;
        }
        return $alldepartments;


    }

}
