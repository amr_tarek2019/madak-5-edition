<?php

namespace App\Modules\Provider\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('provider', 'Resources/Lang', 'app'), 'provider');
        $this->loadViewsFrom(module_path('provider', 'Resources/Views', 'app'), 'provider');
        $this->loadMigrationsFrom(module_path('provider', 'Database/Migrations', 'app'), 'provider');
        $this->loadConfigsFrom(module_path('provider', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('provider', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
