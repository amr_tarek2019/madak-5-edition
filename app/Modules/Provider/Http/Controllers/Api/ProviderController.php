<?php

namespace App\Modules\Provider\Http\Controllers\Api;

use App\Models\Place;
use App\Models\Providers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Modules\Provider\Http\Interfaces\ServiceProviderRepositoryInterface;
use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;


class ProviderController extends Controller
{
    protected $providerObject;
    protected $userObject;

    public function __construct(ServiceProviderRepositoryInterface $providerRepository,AuthRepositoryInterface $userRepository)
    {
        $this->providerObject = $providerRepository;
        $this->userObject = $userRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function serviceProviderRequest(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $request['Jwt']=$Jwt;
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $this->providerObject->serviceProviderRequest($request);
//        $data1['message'] = "hello";
//        Mail::send('CheckMail.mail', $data1, function ($message) use ($request) {
//            $message->to(request('email'))->subject
//            ('hello');
//            $message->from('info@madak.my-staff.net', 'madak application');
//        });
        return response()->json(res_msg($lang, success(), 200, 'request_sent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getAllPlacesCategories()
    {
        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllPlacesCategories($lang);
        return response(res_msg($lang, success(),200,'all_categories', $result));
    }

    public function getAllServicesCategories()
    {
        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesCategories($lang);
        return response(res_msg($lang, success(),200,'all_categories', $result));
    }

    public function getAllServicesSubCategories($category_id){
        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesSubCategories($lang,$category_id);
        return response(res_msg($lang, success(),200,'all_subcategories', $result));

    }

    public function getAllServicesSubSubCategories($subcategory_id){

        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesSubSubCategories($lang,$subcategory_id);
        return response(res_msg($lang, success(),200,'all_subsubcategories', $result));

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllProviderPlacesCategoriesAndServices()
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->getAllProviderPlacesCategories($lang);
        $data['places_categories']=$result;

        $result = $this->providerObject->getAllProviderServices($lang);
        $data['servies']=$result;
        return response(res_msg($lang, success(), 200, 'all_provider_places_categories', $data));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getAllPlacesCategoriesItems($place_category_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->providerObject->getAllPlacesCategoriesItems($lang, $place_category_id);
        return response(res_msg($lang, success(), 200, 'all_places_categories_items', $result));

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getPlaceDetails($place_id)
    {
        $lang = getallheaders()['Lang'];

        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->getPlaceDetails($place_id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'place_details', $result));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editPlace(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->CheckJwt($Jwt);

        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $provider=Providers::where('user_id',$result->id)->pluck('id')->first();
        $request['provider_id']=$provider;
        $this->providerObject->editPlace($request);
        return response(res_msg($lang, success(), 200, 'done'));
    }

    public function deletePlace(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $this->providerObject->deletePlace($request);
        return response(res_msg($lang, success(), 200, 'deleted'));
    }

    public function getAllServicesOfSubCategories($service_category_id){

        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesOfSubCategories($lang,$service_category_id);
        return response(res_msg($lang, success(),200,'all_services_subcategories', $result));
    }

    public function getAllServicesOfSubSubCategories($service_subcategory_id){

        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesOfSubSubCategories($lang,$service_subcategory_id);
        return response(res_msg($lang, success(),200,'all_services_subsubcategories', $result));
    }

    public function getAllServicesOfItemsOfSubSubCategories($service_subsubcategory_id){

        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllServicesOfItemsOfSubSubCategories($lang,$service_subsubcategory_id);
        return response(res_msg($lang, success(),200,'all_services_items_of_subsubcategories', $result));
    }

    public function GetServiceDetails($id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->providerObject->GetServiceDetails($id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'service_details', $result));

    }

    public function editService(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->CheckJwt($Jwt);

        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $provider=Providers::where('user_id',$result->id)->pluck('id')->first();
        $request['provider_id']=$provider;
        $this->providerObject->editService($request);
        return response(res_msg($lang, success(), 200, 'done'));
    }


    public function getPlacesPreparationsOrdersList(Request $request)
    {
        $lang = getallheaders()['Lang'];

        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $placesresult = $this->providerObject->getPlacesPreparationsOrdersList($request);
        return response()->json(res_msg($lang, success(), 200, 'all_orders', $placesresult));
    }
    public function getOrderPlaceDetails($order_id)
    {
        $lang = getallheaders()['Lang'];

        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->getOrderPlaceDetails($order_id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'place_order_details', $result));

    }

    public function getOrderServiceDetails($order_id)
    {
        $lang = getallheaders()['Lang'];

        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->getOrderServiceDetails($order_id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'preparations_order_details', $result));

    }


    public function replyOnPlaceOrder(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $request['user_id']=$result->user_id;
        $this->providerObject->replyOnPlaceOrder($request);

        return response(res_msg($lang, success(), 200, 'done'));
    }

    public function replyOnServiceOrder(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $request['user_id']=$result->user_id;
        $this->providerObject->replyOnServiceOrder($request);

        return response(res_msg($lang, success(), 200, 'done'));
    }



    public function providerSendMessage(Request $request)
    {

        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $validator = Validator::make($request->all(),
            array(
                'place_id' => 'required',
            )
        );


        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->message()]);
        }

        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }

        $request['sender'] = $result->id;
        $this->providerObject->providerSendMessage($request);
        return response()->json(res_msg($lang, success(), 200, 'done'));

    }

    public function getAllusersChat()
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->getAllusersChat($Jwt);
        return response(res_msg('en', success(), 200, 'Messages', $result));


    }

    public function GetAllMessages(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->providerObject->GetAllMessages($Jwt, $request);
        return response(res_msg('en', success(), 200, 'Messages', $result));
    }

    public function getAllPlacesCategoriesAndServices(){

        $lang=getallheaders()['Lang'];
        $result= $this->providerObject->getAllPlacesCategoriesAndServices($lang);
        return response(res_msg($lang, success(),200,'all_categories', $result));

    }
}
