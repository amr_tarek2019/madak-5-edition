<?php
namespace App\Modules\Provider\Http\Interfaces;

interface ServiceProviderRepositoryInterface
{
    public function serviceProviderRequest($request);
    public function getAllPlacesCategories($lang);
    public function getAllServicesCategories($lang);
    public function getAllServicesSubCategories($lang,$category_id);
    public function getAllServicesSubSubCategories($lang,$category_id);
    public function getAllProviderPlacesCategories($lang);
    public function getAllProviderServices($lang);
    public function getAllPlacesCategoriesItems($lang,$place_category_id);
    public function getPlaceDetails($place_id,$lang);
    public function editPlace($request);
    public function deletePlace($request);
    public function getAllServicesOfSubCategories($lang,$service_category_id);
    public function getAllServicesOfSubSubCategories($lang,$service_subcategory_id);
    public function getAllServicesOfItemsOfSubSubCategories($lang,$service_subsubcategory_id);
    public function GetServiceDetails($service_id,$lang);
    public function editService($request);
    public function getPlacesPreparationsOrdersList($request);
    public function replyOnPlaceOrder($request);
    public function replyOnServiceOrder($request);
    public function providerSendMessage($request);
    public function getAllusersChat($Jwt);
    public function GetAllMessages($Jwt,$request);
    public function getOrderPlaceDetails($order_id,$lang);
    public function getOrderServiceDetails($order_id,$lang);
    public function getAllPlacesCategoriesAndServices($lang);
}
