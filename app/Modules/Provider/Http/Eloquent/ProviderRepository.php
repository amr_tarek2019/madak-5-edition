<?php
namespace App\Modules\Provider\Http\Eloquent;

use App\Models\Chat;
use App\Models\ChatImage;
use App\Models\Notification;
use App\Models\Place;
use App\Models\PlaceBooking;
use App\Models\PlaceCategory;
use App\Models\PlaceImage;
use App\Models\PlacesOrders;
use App\Models\PlacesPreparations;
use App\Models\PreparationBooking;
use App\Models\PreparationOrder;
use App\Models\Providers;
use App\Models\PushNotification;
use App\Models\Rate;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\ServiceProviderRequest;
use App\Models\ServicesOrders;
use App\Models\ServiceSubcategory;
use App\Models\SubSubCategory;
use App\Models\User;
use App\Modules\Provider\Http\Interfaces\ServiceProviderRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use function foo\func;

class ProviderRepository implements ServiceProviderRepositoryInterface
{
    public function CheckJwt($jwt)
    {
        $user=User::where('jwt_token',$jwt)->first();
        if (isset($user->id)){
            return $user;
        }else{
            return "false";
        }
    }

    public function serviceProviderRequest($request)
{
    $validator = Validator::make($request->all(),
        array(
            'Jwt' => 'required'
        )
    );

    if ($validator->fails()) {
        return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
    }
    $user = $this->CheckJwt($request->Jwt);
    $request['user_id'] = $user->id;

    ServiceProviderRequest::create($request->all());
    return 'true';
}

    public function getAllPlacesCategories($lang){
        $allcategories=array();
        $data=array();
        $categories = PlaceCategory::where('status','1')->get();
        foreach ($categories as $category) {
            $data['id']=$category->id;
            $data['name']=$category->translate($lang)->name;
            array_push($allcategories,$data);
        }
        return $allcategories;
    }

    public function getAllServicesCategories($lang){
        $allcategories=array();
        $data=array();
        $categories = ServiceCategory::where('status','1')->get();
        foreach ($categories as $category) {
            $data['id']=$category->id;
            $data['name']=$category->translate($lang)->name;
            array_push($allcategories,$data);
        }
        return $allcategories;
    }

    public function getAllServicesSubCategories($lang,$category_id){
        $allsubcategories=array();
        $data=array();
        $subcategories = ServiceSubcategory::where('service_category_id',$category_id)->where('status','1')->get();

        foreach ($subcategories as $subcategory) {
            $data['id']=$subcategory->id;
            $data['name']=$subcategory->translate($lang)->name;
            array_push($allsubcategories,$data);
        }
        return $allsubcategories;
    }

    public function getAllServicesSubSubCategories($lang,$subcategory_id){
        $allsubsubcategories=array();
        $data=array();
        $subsubcategories = SubSubCategory::where('service_subcategory_id',$subcategory_id)->where('status','1')->get();

        foreach ($subsubcategories as $subsubcategory) {
            $data['id']=$subsubcategory->id;
            $data['name']=$subsubcategory->translate($lang)->name;
            array_push($allsubsubcategories,$data);
        }
        return $allsubsubcategories;
    }

    public function getAllProviderPlacesCategories($lang){
        $allcategories=array();
        $data=array();
        $Jwt = getallheaders()['Jwt'];
        $user = $this->CheckJwt($Jwt);
        $provider_id=Providers::where('user_id',$user->id)->pluck('id')->first();
        $categories = PlaceCategory::where('status','1')->get();
        foreach ($categories as $category) {
            $data['id']=$category->id;
            $data['category_name']=$category->translate($lang)->name;
            $data['main_image']=$category->image;
            $data['count']=Place::select('id')->where('provider_id',$provider_id)->where('place_category_id',$category->id)->count();
            array_push($allcategories,$data);
        }
        return $allcategories;
    }

    public function getAllProviderServices($lang){
        $allservices=array();
        $data=array();
        $services = ServiceCategory::where('status','1')->get();
        foreach ($services as $service) {
            $data['id']=$service->id;
            $data['service_category_name']=$service->translate($lang)->name;
            $data['image']=$service->image;
            array_push($allservices,$data);
        }

        return $allservices;
    }
    public function getAllPlacesCategoriesItems($lang,$place_category_id)
    {
        $allitems = array();
        $data = array();
        $Jwt = getallheaders()['Jwt'];
        $user = $this->CheckJwt($Jwt);
        $provider_id=Providers::where('user_id',$user->id)->pluck('id')->first();
        $placesCategories = Place::where('place_category_id', $place_category_id)->where('provider_id',$provider_id)->orderBy('id', 'desc')->get();
        foreach ($placesCategories as $place) {
            $data['id']=$place->id;
            $data['image']=$place->main_image;
            $data['name']=$place->name;
            $data['desc']=$place->desc;
            $data['price']=$place->price;


            $ratePlace=Rate::where('place_id',$place->id)->select('rate')->avg('rate');
            if (!empty($ratePlace))
            {
            $data['rate']=(string)$ratePlace;
            }else{
                $data['rate']="0";
            }
            array_push($allitems,$data);
        }
        return $allitems;
    }

    public function getPlaceDetails($place_id,$lang){
        $place= Place::where('id',$place_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($place as $res) {
            $res_item['id'] = $res->id;
            $res_item['name'] = $res->name;
            $category=PlaceCategory::where('id',$res->place_category_id)->first();
            $res_item['category_id']=$category->id;
            $res_item['category_name']=$category->translate($lang)->name;
            $images=PlaceImage::where('place_id',$res->id)->select('image')->get();
            $res_item['images']=$images;
            $res_item['present_type'] = $res->present_type;
            $res_item['present_type'] = $res->present_type;
            $res_item['quantity'] = $res->quantity;
            $res_item['price'] = $res->price;
            $res_item['date_from'] = $res->date_from;
            $res_item['date_to'] = $res->date_to;
            $res_item['lat'] = $res->lat;
            $res_item['lng'] = $res->lng;
            $res_item['address'] = $res->address;
            $res_item['desc'] = $res->desc;
            $res_list = $res_item;
        }
        return $res_list;
    }

    public function editPlace($request)
    {
        $place=Place::where('id',$request->place_id)->first();
        $place->update([
            'name'=>$request->name,
            'place_category_id'=>$request->place_category_id,
            'present_type'=>$request->present_type,
            'price'=>$request->price,
            'city_id' => $request->city_id,
            'gender' => $request->gender,
            'quantity'=>$request->quantity,
            'date_from'=>$request->date_from,
            'date_to'=>$request->date_to,
            'lat'=>$request->lat,
            'lng'=>$request->lng,
            'address'=>$request->address,
            'desc'=>$request->desc,
            'main_image'=>$request->imgs[0],
            'preparations_id'=>$request->preparations_id,
        ]);
        for ($i = 1; $i < count($request->imgs); $i++) {
            $placeImage=PlaceImage::find($request->place_id);
            $placeImage->update(['image' => $request->imgs[$i],
                'image' =>  $request->imgs[$i],
            ]);
        }

        for ($i = 0; $i < count($request->preparations_id); $i++) {
            $placePreparation=PlacesPreparations::find($request->place_prep_id[$i]);
            $placePreparation->update(['preparations_id' => $request->preparations_id[$i],
                'preparations_id' =>  $request->preparations_id[$i],
            ]);
        }

    }

    public function deletePlace($request)
    {
        Place::destroy('id',$request->place_id);
    }

//    public function GetAllSubCategories($lang,$category_id){
//        $allsubcategories=array();
//        $data=array();
//        $subcategories = SubCategory::where('categories_id',$category_id)->get();
//        foreach ($subcategories as $subcategory) {
//            $data['id']=$subcategory->id;
//            $data['name']=$subcategory->getTranslation($lang)->sub_categories_name;
//            array_push($allsubcategories,$data);
//        }
//        return $allsubcategories;
//    }

    public function getAllServicesOfSubCategories($lang,$service_category_id){
        $allsubcategories=array();
        $data=array();
        $subcategories = ServiceSubcategory::where('service_category_id',$service_category_id)
            ->where('status','1')->get();
        foreach ($subcategories as $subcategory) {
            $data['id']=$subcategory->id;
            $data['name']=$subcategory->translate($lang)->name;
            $data['image']=$subcategory->image;
            array_push($allsubcategories,$data);
        }
        return $allsubcategories;
    }

    public function getAllServicesOfSubSubCategories($lang,$service_subcategory_id){
        $allsubsubcategories=array();
        $data=array();
        $subsubcategories = SubSubCategory::where('service_subcategory_id',$service_subcategory_id)
            ->where('status','1')->get();
        foreach ($subsubcategories as $subsubcategory) {
            $data['id']=$subsubcategory->id;
            $data['name']=$subsubcategory->translate($lang)->name;
            $data['image']=$subsubcategory->image;
            array_push($allsubsubcategories,$data);
        }
        return $allsubsubcategories;
    }

    public function getAllServicesOfItemsOfSubSubCategories($lang,$service_subsubcategory_id){
        $allsubsubcategories=array();
        $data=array();
        $subsubcategories = Service::where('service_sub_sub_category_id',$service_subsubcategory_id)->get();
        foreach ($subsubcategories as $subsubcategory) {
            $data['id']=$subsubcategory->id;
            $data['present_type']=$subsubcategory->present_type;
            $data['image']=$subsubcategory->image;
            $data['desc']=$subsubcategory->desc;
            $data['price_before']=$subsubcategory->price_before;
            $data['price_after']=$subsubcategory->price_after;
            array_push($allsubsubcategories,$data);
        }
        return $allsubsubcategories;
    }

    public function GetServiceDetails($service_id,$lang){
        $service= Service::where('id',$service_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($service as $res) {
            $res_item['id'] = $res->id;
            $res_item['image'] = $res->image;
            $res_item['present_type'] = $res->present_type;
            $res_item['price_before'] = $res->price_before;
            $res_item['price_after'] = $res->price_after;
            $res_item['quantity'] = $res->quantity;
            $res_item['lat'] = $res->lat;
            $res_item['lng'] = $res->lng;
            $res_item['address'] = $res->address;
            $res_item['date'] = $res->date;
            $res_item['desc'] = $res->desc;
            $getservicecategorydata=ServiceCategory::where('id',$res->service_category_id)->first();
            $res_item['cat_id']=$getservicecategorydata->id;
            $res_item['cat_name']=$getservicecategorydata->translate($lang)->name;
            $getserviceSubcategorydata=ServiceSubcategory::where('id',$res->service_subcategory_id)->first();
            $res_item['subcat_id']=$getserviceSubcategorydata->id;
            $res_item['subcat_name']=$getserviceSubcategorydata->translate($lang)->name;
            $getserviceSubSubcategorydata=SubSubCategory::where('id',$res->service_sub_sub_category_id)->first();
            $res_item['subsubcat_id']=$getserviceSubSubcategorydata->id;
            $res_item['subsubcat_name']=$getserviceSubSubcategorydata->translate($lang)->name;
            $res_list= $res_item;
        }
        return $res_list;
    }

    public function editService($request)
    {
        $service=Service::where('id',$request->id)->first();
        $service->update([
            'service_category_id'=>$request->service_category_id,
            'service_subcategory_id'=>$request->service_subcategory_id,
            'service_sub_sub_category_id'=>$request->service_sub_sub_category_id,
            'present_type'=>$request->present_type,
            'price_before'=>$request->price_before,
            'price_after'=>$request->price_after,
            'quantity'=>$request->quantity,
            'date'=>$request->date,
            'lat'=>$request->lat,
            'lng'=>$request->lng,
            'address'=>$request->address,
            'desc'=>$request->desc,
            'image'=>$request->image,
        ]);
       return $service;
    }
    public function getPlacesPreparationsOrdersList($request){
        $Jwt = getallheaders()['Jwt'];
        $user = $this->CheckJwt($Jwt);
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $getuser=User::where('id',$user->id)->first();
        $provider=Providers::where('user_id',$getuser->id)->pluck('id')->first();
        $getPlacesId=PlacesOrders::where('provider_id',$provider)->pluck('order_id');
        $places=PlaceBooking::whereIn('id',$getPlacesId)->where('date_from',$date_from)->orWhere('date_to',$date_to)
            ->get();
        $res_item = [];
        $res_list  = [];
        foreach ($places as $res) {
            $res_item['id'] = $res->id;
            $getUserData=User::where('id',$res->user_id)->select('id','name','email','phone','image')->first();
            $res_item['user_id'] = $getUserData->id;
            $res_item['user_name'] = $getUserData->name;
            $res_item['user_email'] = $getUserData->email;
            $res_item['user_phone'] = $getUserData->phone;
            $res_item['user_image'] = $getUserData->image;
            $getPlaceData=PlaceBooking::where('id',$res->id)->pluck('place_id')->first();
            $getPlace=Place::where('id',$getPlaceData)->select('name','desc')->first();
            $res_item['place_name'] = $getPlace->name;
            $res_item['place_desc'] = $getPlace->desc;
            $res_item['order_date_from'] = $res->date_from;
            $res_item['order_date_to'] = $res->date_to;
            $res_item['order_status'] = $res->status;
            $res_list[] = $res_item;
        }
        $getpreparationsid=ServicesOrders::where('provider_id',$provider)->pluck('order_id');
        $preparations=PreparationBooking::whereIn('id',$getpreparationsid)->where('date',$date_from)->get();
        $result_item = [];
        $result_list  = [];
        foreach ($preparations as $result) {
            $result_item['id'] = $result->id;
            $getUserData=User::where('id',$result->user_id)->select('id','name','email','phone','image')->first();
            $result_item['user_id'] = $getUserData->id;
            $result_item['user_name'] = $getUserData->name;
            $result_item['user_email'] = $getUserData->email;
            $result_item['user_phone'] = $getUserData->phone;
            $result_item['user_image'] = $getUserData->image;
            $result_item['order_date'] = $result->date;
            $result_item['order_status'] = $result->status;
            $getServiceData=PreparationOrder::where('preparation_booking_id',$result->id)->pluck('services_sub_sub_category_id')->first();
            $service=Service::where('service_sub_sub_category_id',$getServiceData)->select('id','image','desc','price','present_type')->get();
            $result_item['services'] = $service;
            $result_list[] = $result_item;
        }
        $data = array_merge($res_list, $result_list);
        return $data;
    }

    public function getOrderPlaceDetails($order_id,$lang){
        $order= PlaceBooking::where('id',$order_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($order as $details){
            $res_item['id']=$details->id;
            $res_item['date_from']=$details->date_from;
            $res_item['date_to']=$details->date_to;
            $getPlaceData=Place::where('id',$details->place_id)->first();
            $res_item['desc'] = $getPlaceData->desc;
            $bookingPlaces=PlaceBooking::where('place_id',  $details->place_id)->select('id','user_id','date_from','date_to','status')->first();
            $res_item['place_id'] = $bookingPlaces->id;
            $res_item['place_name'] = $getPlaceData->name;
            $res_item['user_id'] = $bookingPlaces->user_id;
            $getUser=User::where('id',$bookingPlaces->user_id)->select('name','image','phone')->first();
            $res_item['user_name'] = $getUser->name;
            $res_item['user_image'] = $getUser->image;
            $res_item['user_phone'] = $getUser->phone;
            $res_item['date_from'] = $bookingPlaces->date_from;
            $res_item['date_to'] = $bookingPlaces->date_to;
            $res_item['status'] = $bookingPlaces->status;
            $res_list[] = $res_item;
        }
        return $res_list;
    }



    public function getOrderServiceDetails($order_id,$lang){
        $order= PreparationBooking::where('id',$order_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($order as $details){
            $res_item['id']=$details->id;
            $getUserData=User::where('id',$details->user_id)->first();
            $res_item['user_id']=$getUserData->id;
            $res_item['user_name']=$getUserData->name;
            $res_item['user_image']=$getUserData->image;
            $res_item['user_phone']=$getUserData->phone;
            $res_item['date']=$details->date;
            $res_item['status']=$details->status;
            $getSubSubCatData=PreparationOrder::where('preparation_booking_id',$details->id)->pluck('services_sub_sub_category_id')->first();
            $getServiceData=Service::where('service_sub_sub_category_id',$getSubSubCatData)->select('id','present_type','price','desc','image')->get();
            $res_item['service']=$getServiceData;
            $res_list = $res_item;
        }
        return $res_list;
    }




    public function replyOnPlaceOrder($request){
        $order=PlaceBooking::where('id',$request->id)->first();
        $order->update([
            'status'=>$request->status
        ]);

    }

    public function replyOnServiceOrder($request){
        $order=PreparationBooking::where('id',$request->id)->first();
        $order->update([
            'status'=>$request->status
        ]);

    }

    public function AddNotification($img, $user_id = [],
                                    $text = array(), $title = array())
    {
        for ($i = 0; $i < count($user_id); $i++) {
            Notification::create([
                'icon' => $img,
                'user_id' => $user_id[$i],
                'en' => [
                    'text' => $text['texten'],
                    'title' => $title['titleen'],
                ],
                'ar' => [
                    'text' => $text['textar'],
                    'title' => $title['titlear'],
                ],

            ]);
        }


    }

    public function providerSendMessage($request){
        if (isset($request->message)) {
            $message = Chat::create([
                'sender' => $request->sender,
                'receiver' => $request->receiver,
                'message' => $request->message,
                'place_id' => $request->place_id,
            ]);
            $message['sender'] = $message->sender;
            $user = User::where('id', $request->receiver)->first();
            $this->AddNotification($user->image, [$user->id], 1, $request->place_id, $text = ['texten' => 'Sent you a new message',
                'textar' => 'لقد ارسل لك رسالة جديدة'], $title = ['titleen' => $user->name,
                'titlear' => $user->name]);
            // PushNotification::send_details([$user->firebase_token],'New Message',$request->message,$message,1);

        } elseif (isset($request->image)) {
            $message=  Chat::create([
                'sender'=>$request->sender,
                'receiver'=>$request->receiver,
                'message'=>'',
                'place_id'=>$request->place_id,
            ]);
            $message['sender']=$message->sender;
            $user=User::where('id',$request->receiver)->first();
            $this->AddNotification($user->image,[$user->id],1,$request->place_id,$text=['texten'=>'Sent you a new message',
                'textar'=>'لقد ارسل لك رسالة جديدة'],$title=['titleen'=>$user->name,
                'titlear'=> $user->name]);
            for ($i = 0; $i < count($request->image); $i++) {
                $message->related_images()->create(['image' => $request->image[$i],
                    'default_image' => 0,
                ]);
            }
        }

        return $message;
    }

    public function getAllusersChat($Jwt){
        $user=User::where('jwt_token',$Jwt)->first();
        $getChatWithOtherUsers=Chat::groupBy('place_id')->where('sender',$user->id)->orWhere('receiver',$user->id)->orderBy('created_at','desc')->get();
        $res_item = [];
        $res_list  = [];
        foreach($getChatWithOtherUsers as $chat){
            $res_item['chat_id']= $chat->id;
            $res_item['created_at']= $chat->created_at;
//            $User =User::where('id',$chat->receiver)->first();
//            $res_item['user_id']=$User->id;
//            $res_item['user_name']=$User->name;
//            $res_item['user_image']=$User->image;
//            $res_item['receiver']=$chat->receiver;
            if ($chat->sender == $user->id)
            {
                $data=User::where('id',$chat->receiver)->select('id','name','image')->first();
                $res_item['user_name']=$data->name;
                $res_item['user_id']=$data->id;
                $res_item['user_image']=$data->image;
            }
            if ($chat->receiver == $user->id){
                $data=User::where('id',$chat->sender)->select('id','name','image')->first();
                $res_item['user_name']=$data->name;
                $res_item['user_id']=$data->id;
                $res_item['user_image']=$data->image;
            }
            $res_list[] = $res_item;
        }

        return $res_list;

    }

    public function GetAllMessages($Jwt,$request){
        $user=User::where('jwt_token',$Jwt)->first();
        $user_id=$user->id;
        $reciever_id= $request->receiver;
//        $chat=Chat::where('id',$chat_id)->get();
        $chat = DB::select('SELECT *
                FROM chats
                Where
                (sender = "'.$reciever_id.'" and receiver = "'.$user_id.'")
                OR
                (sender = "'.$user_id.'" and receiver = "'.$reciever_id.'")
                ');

        $res_item = [];
        $res_list  = [];
        foreach ($chat as $res) {

            $res_item['id']=$res->id;
//            if ($user->id==$res->reciever)
//            {
//                $res_item['is_reciever']='yes';
//                $anotherUser=User::where('id',$res->sender)->select('id','name')->first();
//                $res_item['another_user_name']=$anotherUser->name;
//                $res_item['another_user_id']=$anotherUser->id;
//            }else{
//                $res_item['is_sender']='yes';
//                $anotherUser=User::where('id',$res->receiver)->select('id','name')->first();
//                $res_item['another_user_name']=$anotherUser->name;
//                $res_item['another_user_id']=$anotherUser->id;
//            }
            $senderUser = User::where('id', $res->sender)->select('id', 'name')->first();
            $res_item['sender_name'] = $senderUser->name;
            $res_item['sender_id'] = $senderUser->id;
            $recieverUser = User::where('id', $res->receiver)->select('id', 'name')->first();
            $res_item['receiver_name'] = $recieverUser->name;
            $res_item['receiver_id'] = $recieverUser->id;
            $res_item['created_at']=$res->created_at;
            if ($res->message == '') {
//                $getImageId = Chat::where('id', $res->id)->pluck('id');
                $getChatImages = ChatImage::where('chat_id', $res->id)->select('id', 'image')->get();
                $res_item['chat_images'] = $getChatImages;
                $res_item['message'] ='';

            } else {
                $res_item['message'] = $res->message;
                $res_item['chat_images']="";
            }
            $res_list[] = $res_item;
        }
        return $res_list;
    }

    public function getAllPlacesCategoriesAndServices($lang){
        global $data;
        $categories = PlaceCategory::where('status','1')->get();
        $cat_Items = [];
        $cat_list =[];
        foreach ($categories as $category) {
            $cat_Items['id']=$category->id;
            $cat_Items['name']=$category->translate($lang)->name;
            $cat_list[] = $cat_Items;
        }
        $data['cat_list']=$cat_list;

        $services = Service::get();
        $ser_Items = [];
        $ser_list =[];
        foreach ($services as $service) {
            $ser_Items['id']=$service->id;
            $ser_Items['name']=$service->present_type;
            $ser_list[]=$ser_Items;
        }
        $data['ser_list']=$ser_list;


        return $data;



    }

}
