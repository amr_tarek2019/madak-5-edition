<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/provider', function (Request $request) {
//    // return $request->provider();
//})->middleware('auth:api');

Route::group(['prefix' => 'provider'], function () {
    Route::post('/request','Api\ProviderController@serviceProviderRequest');
    Route::get('/allplacescategories','Api\ProviderController@getAllPlacesCategories');
    Route::get('/allservicescategories','Api\ProviderController@getAllServicesCategories');
    Route::get('/allservicessubcategories/{category_id}','Api\ProviderController@getAllServicesSubCategories');
    Route::get('/allservicessubsubcategories/{subcategory_id}','Api\ProviderController@getAllServicesSubSubCategories');
    Route::get('/allproviderplacescategoriesandservices','Api\ProviderController@getAllProviderPlacesCategoriesAndServices');
    Route::get('/getallplacescategoriesitems/{place_category_id}','Api\ProviderController@getAllPlacesCategoriesItems');
    Route::get('/place/{place_id}','Api\ProviderController@getPlaceDetails');
    Route::Post('/updateplace','Api\ProviderController@editPlace');
    Route::post('place/delete','Api\ProviderController@deletePlace');
    Route::get('service/subcategories/{service_category_id}','Api\ProviderController@getAllServicesOfSubCategories');
    Route::get('service/subsubcategories/{service_subcategory_id}','Api\ProviderController@getAllServicesOfSubSubCategories');
    Route::get('service/items/{service_subsubcategory_id}','Api\ProviderController@getAllServicesOfItemsOfSubSubCategories');
    Route::get('/service/details/{service_id}','Api\ProviderController@GetServiceDetails');
    Route::Post('/updateservice','Api\ProviderController@editService');
    Route::post('/getplacespreparationsorders','Api\ProviderController@getPlacesPreparationsOrdersList');
    Route::Post('/replyOnPlaceOrder','Api\ProviderController@replyOnPlaceOrder');
    Route::Post('/replyOnServiceOrder','Api\ProviderController@replyOnServiceOrder');
    Route::post('/sendmessage','Api\ProviderController@providerSendMessage');
    Route::get('/getAllusersChat','Api\ProviderController@getAllusersChat');
    Route::get('/getallmessages','Api\ProviderController@GetAllMessages');
    Route::get('/placeorder/{order_id}','Api\ProviderController@getOrderPlaceDetails');
    Route::get('/serviceorder/{order_id}','Api\ProviderController@getOrderServiceDetails');
    Route::get('/places/preparations','Api\ProviderController@getAllPlacesCategoriesAndServices');
});
