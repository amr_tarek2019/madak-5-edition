<?php
namespace App\Modules\User\Http\Interfaces;

interface UserRepositoryInterface
{
    public function getAllPlacesCategories($lang);
    public function getAllPreparationsCategories($lang);
    public function getGender($lang);
    public function getDepartmentsItems($lang , $department_id);
    public function getAllDepartments($lang);
    public function filterMinMaxPrices($request);
    public function search($request);
    public function getAllPlacesCategory($lang,$place_category_id);
    public function getPlaceDetails($place_id,$lang);
    public function bookingPlace($request);
    public function getPlaceReviewsDetails($place_id,$lang);
    public function RatePlace($request);
    public function getPlaceOrderDetails($order_id,$lang);
    public function editPlaceBooking($request);
    public function getAllServiceSubCategories($lang,$service_category_id);
    public function getAllServiceSubSubCategories($lang,$service_subcategory_id);
    public function getAllItemsOfServicesOfSubSubCategories($lang,$service_subsubcategory_id);
    public function editBookingService($request);
    public function getReservations($lang,$user_id);
    public function getPreparationsReservations($lang,$user_id);
    public function getOrders($user_id,$request);
    public function userSendMessage($request);
    public function getAllusersChat($Jwt);
    public function GetAllMessages($Jwt,$request);
}
