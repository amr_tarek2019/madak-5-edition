<?php

namespace App\Modules\User\Http\Controllers\Api;

use App\Modules\User\Http\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    protected $userObject;

    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userObject = $userRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllPlacesCategories(){

        $lang=getallheaders()['Lang'];
        $result= $this->userObject->getAllPlacesCategories($lang);
        return response(res_msg($lang, success(),200,'all_places_categories', $result));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllPreparationsCategories()
    {
        $lang=getallheaders()['Lang'];
        $result= $this->userObject->getAllPreparationsCategories($lang);
        return response(res_msg($lang, success(),200,'all_preparations_categories', $result));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getGender(){
        $lang=getallheaders()['Lang'];
        $result= $this->userObject->getGender($lang);
        return response(res_msg($lang, success(),200,'all_genders', $result));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getDepartmentsItems($department_id){
        $lang=getallheaders()['Lang'];
        $result= $this->userObject->getDepartmentsItems($lang,$department_id);
        return response(res_msg($lang, success(),200,'all_department_items', $result));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getAllDepartments(){

        $lang=getallheaders()['Lang'];
        $result= $this->userObject->getAlldepartments($lang);
        return response(res_msg($lang, success(),200,'all_departments', $result));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function filterMinMaxPrices(Request $request){
        $lang = getallheaders()['Lang'];
        $request->lang=$lang;
        $result = $this->userObject->filterMinMaxPrices($request);
        return response()->json(res_msg($lang, success(), 200, 'one_item', $result));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request){
        $lang = getallheaders()['Lang'];
        $request->lang=$lang;
        $result = $this->userObject->search($request);
        return response()->json(res_msg($lang, success(), 200, 'result', $result));

    }

    public function getAllPlacesCategory($place_category_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->getAllPlacesCategory($lang, $place_category_id);
        return response(res_msg($lang, success(), 200, 'all_places', $result));

    }

    public function getPlaceDetails($place_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->getPlaceDetails($place_id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'place_details', $result));
    }

    public function bookingPlace(Request $request){
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $request['Jwt']=$Jwt;
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $this->userObject->bookingPlace($request);
        return response()->json(res_msg($lang, success(), 200, 'request_sent'));
    }

    public function getPlaceReviewsDetails($place_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->getPlaceReviewsDetails($place_id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'place_reviews_details', $result));
    }

    public function RatePlace(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $validator = Validator::make($request->all(),
            array(
                'rate' => 'required',
                'review' => 'required',
            )
        );
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $request['user_id'] = $result->id;
        $this->userObject->RatePlace($request);
        return response()->json(res_msg($lang, success(), 200, 'done'));


    }


    public function getPlaceOrderDetails($order_id)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->userObject->getPlaceOrderDetails($order_id,$lang);
        return response()->json(res_msg($lang, success(), 200, 'order_details', $result));
    }

    public function editPlaceBooking(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $this->userObject->editPlaceBooking($request);
        return response(res_msg($lang, success(), 200, 'done'));
    }

    public function getAllServiceSubCategories($service_category_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->getAllServiceSubCategories($lang, $service_category_id);
        return response(res_msg($lang, success(), 200, 'all_preparations_subcategories', $result));
    }

    public function getAllServiceSubSubCategories($service_subcategory_id)
    {
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->getAllServiceSubSubCategories($lang, $service_subcategory_id);
        return response(res_msg($lang, success(), 200, 'all_preparations_subsubcategories', $result));
    }

    public function getAllItemsOfServicesOfSubSubCategories($service_subsubcategory_id){

        $lang=getallheaders()['Lang'];
        $result= $this->userObject->getAllItemsOfServicesOfSubSubCategories($lang,$service_subsubcategory_id);
        return response(res_msg($lang, success(),200,'all_services_items_of_subsubcategories', $result));
    }

    public function CreateServiceBooking(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $validator = Validator::make($request->all(),
            array(
                'message' => 'required',
                'date' => 'required',
                'services_sub_sub_category_id' => 'required',
                'quantity' => 'required',
            )
        );
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $request['user_id'] = $result->id;
        $this->userObject->CreateServiceBooking($request);
        return response()->json(res_msg($lang, success(), 200, 'done'));
    }

    public function editBookingService(Request $request)
    {
        $Jwt = getallheaders()['Jwt'];
        $lang = getallheaders()['Lang'];
        $result = $this->userObject->CheckJwt($Jwt);

        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $this->userObject->editBookingService($request);
        return response(res_msg($lang, success(), 200, 'done'));
    }

    public function getReservations(){
        $lang=getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result= $this->userObject->getReservations($lang,$result->id);
        return response(res_msg($lang, success(),200,'all_notifications', $result));
    }

    public function getPreparationsReservations(){
        $lang=getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result=$this->userObject->CheckJwt($Jwt);
        if($result=='false'){
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result= $this->userObject->getPreparationsReservations($lang,$result->id);
        return response(res_msg($lang, success(),200,'all_notifications', $result));
    }

    public function getOrders(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->userObject->getOrders($result->id,$request);
        return response()->json(res_msg($lang, success(), 200, 'all_orders', $result));
    }

    public function userSendMessage(Request $request)
    {

        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $validator = Validator::make($request->all(),
            array(
                'place_id' => 'required',
            )
        );


        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->message()]);
        }

        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }

        $request['sender'] = $result->id;
        $this->userObject->userSendMessage($request);
        return response()->json(res_msg($lang, success(), 200, 'done'));

    }

    public function getAllusersChat()
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }
        $result = $this->userObject->getAllusersChat($Jwt);
        return response(res_msg('en', success(), 200, 'Messages', $result));


    }

    public function GetAllMessages(Request $request)
    {
        $lang = getallheaders()['Lang'];
        $Jwt = getallheaders()['Jwt'];
        $result = $this->userObject->CheckJwt($Jwt);
        if ($result == 'false') {
            return response()->json(res_msg($lang, expired(), 403, 'Jwt_expired'));
        }

        $result = $this->userObject->GetAllMessages($Jwt,$request);
        return response(res_msg('en', success(), 200, 'Messages', $result));


    }
}
