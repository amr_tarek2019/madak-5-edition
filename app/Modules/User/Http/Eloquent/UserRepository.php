<?php

namespace App\Modules\User\Http\Eloquent;

use App\Models\Chat;
use App\Models\ChatImage;
use App\Models\Department;
use App\Models\Gender;
use App\Models\Notification;
use App\Models\Place;
use App\Models\PlaceBooking;
use App\Models\PlaceCategory;
use App\Models\PlaceImage;
use App\Models\PlacesPreparations;
use App\Models\PreparationBooking;
use App\Models\PreparationOrder;
use App\Models\PushNotification;
use App\Models\Rate;
use App\Models\Service;
use App\Models\ServiceCategory;
use App\Models\ServicesOrders;
use App\Models\ServiceSubcategory;
use App\Models\SubSubCategory;
use App\Models\User;
use App\Modules\User\Http\Interfaces\UserRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;

class UserRepository implements UserRepositoryInterface
{
    public function CheckJwt($jwt)
    {
        $user = User::where('jwt_token', $jwt)->first();
        if (isset($user->id)) {
            return $user;
        } else {
            return "false";
        }
    }


    public function AddNotification($img, $user_id = [], $title = array(), $text = array())
    {
        for ($i = 0; $i < count($user_id); $i++) {
            Notification::create([
                'icon' => $img,
                'user_id' => $user_id[$i],
                'en' => [
                    'title' => $title['titleen'],
                    'text' => $text['texten'],
                ],
                'ar' => [
                    'title' => $title['titlear'],
                    'text' => $text['textar'],
                ],

            ]);
        }


    }

    public function getAllPlacesCategories($lang)
    {
        $allcategories = array();
        $data = array();
        $categories = PlaceCategory::where('status', '1')->get();
        foreach ($categories as $category) {
            $data['id'] = $category->id;
            $data['name'] = $category->translate($lang)->name;
            $data['image'] = $category->image;
            $data['count'] = Place::select('id')->where('place_category_id', $category->id)->count();
            array_push($allcategories, $data);
        }
        return $allcategories;
    }

    public function getAllPreparationsCategories($lang)
    {
        $allcategories = array();
        $data = array();
        $categories = ServiceCategory::where('status', '1')->get();
        foreach ($categories as $category) {
            $data['id'] = $category->id;
            $data['name'] = $category->translate($lang)->name;
            $data['image'] = $category->image;
            array_push($allcategories, $data);
        }
        return $allcategories;
    }

    public function getGender($lang)
    {
        $genders = Gender::get();
        $allgenders = array();
        $i = 0;
        foreach ($genders as $gender) {

            $allgenders[$i]['name'] = $gender->translate($lang)->name;
            $allgenders[$i]['id'] = $gender->id;
            $i++;
        }
        return $allgenders;


    }

    public function getDepartmentsItems($lang, $department_id)
    {
        if ($department_id == '1') {
            $allcategories = array();
            $data = array();
            $categories = PlaceCategory::where('department_id', '1')->where('status', '1')->get();
            foreach ($categories as $category) {
                $data['id'] = $category->id;
                $data['name'] = $category->translate($lang)->name;
                $data['image'] = $category->image;
                $data['count'] = Place::select('id')->where('place_category_id', $category->id)->count();
                array_push($allcategories, $data);
            }
            return $allcategories;
        } elseif ($department_id == '2') {
            $allcategories = array();
            $data = array();
            $categories = ServiceCategory::where('department_id', '2')->where('status', '1')->get();
            foreach ($categories as $category) {
                $data['id'] = $category->id;
                $data['name'] = $category->translate($lang)->name;
                $data['image'] = $category->image;
                array_push($allcategories, $data);
            }
            return $allcategories;
        }


    }

    public function getAllDepartments($lang)
    {
        $alldepartments = array();
        $data = array();
        $departments = Department::get();
        foreach ($departments as $department) {
            $data['id'] = $department->id;
            $data['name'] = $department->translate($lang)->name;
            array_push($alldepartments, $data);
        }
        return $alldepartments;
    }

    public function filterMinMaxPrices($request)
    {
        if ($request->department_id == 1) {
            $data = DB::select("select MIN(price) AS max_price, MAX(price) AS min_price from places");
        } else {
            $data = DB::select("select MIN(price) AS max_price, MAX(price) AS min_price from services");
        }

        return $data[0];


    }

    public function search($request)
    {
        if ($request->department_id == 1) {
            return Place::getData($request);
        } else if ($request->department_id == 2) {
            return Service::getData($request);
        } else if ($request->department_id == 1 && $request->city_id) {
            return Place::getData($request);
        } else if ($request->department_id == 2 && $request->city_id) {
            return Service::getData($request);
        } else if ($request->department_id == 1 && $request->gender_id) {
            return Place::getData($request);
        } else if ($request->department_id == 2 && $request->gender_id) {
            return Service::getData($request);
        } else if ($request->department_id == 1 && $request->min_price && $request->max_price) {
            return Place::getData($request);
        } else {
            return Service::getData($request);
        }


    }

    public function getAllPlacesCategory($lang, $place_category_id)
    {
        $query = Place::where('place_category_id', $place_category_id)->get();
        $res_item = [];
        $res_list = [];
        foreach ($query as $place) {
            $res_item['id'] = $place->id;
            $res_item['name'] = $place->name;
            $res_item['desc'] = $place->desc;
            $res_item['main_image'] = $place->main_image;
            $res_item['price'] = $place->price;
            $ratePlace = round(Rate::where('place_id', $place->id)->select('rate')->avg('rate'));
            if (!empty($ratePlace)) {
                $res_item['rate'] = (string)$ratePlace;
            } else {
                $res_item['rate'] = "0";
            }
            $res_list[] = $res_item;
        }
        return $res_list;
    }

    public function getPlaceDetails($place_id, $lang)
    {
        $place = Place::where('id', $place_id)->get();
        $res_item = [];
        $res_list = [];
        foreach ($place as $res) {
            $res_item['id'] = $res->id;
            $res_item['name'] = $res->name;
            $res_item['desc'] = $res->desc;
            $res_item['main_image'] = $res->main_image;
            $res_item['lat'] = $res->lat;
            $res_item['lng'] = $res->lng;
            $res_item['address'] = $res->address;
            $res_item['price'] = $res->price;
            $getImagesSliders = PlaceImage::where('place_id', $res->id)->select('image')->get();
            if (!empty($getImagesSliders)) {
                $res_item['slider'] = $getImagesSliders;
            } else {
                $res_item['slider'] = "no sliders found";
            }
            $getPlacePreparations = PlacesPreparations::where('place_id', $res->id)->pluck('preparations_id');
            $preparations = Service::whereIn('id', $getPlacePreparations)->select('present_type','image')->get();
            if (!empty($preparations)) {
                $res_item['preparations'] = $preparations;
            } else {
                $res_item['preparations'] = "no preparations found";
            }
            $ratePlace = round(Rate::where('place_id', $res->id)->select('rate')->avg('rate'));
            if (!empty($ratePlace)) {
                $res_item['rate'] = (string)$ratePlace;
            } else {
                $res_item['rate'] = "0";
            }
            $res_list = $res_item;
        }
        $place = $res_list;
        return $place;

    }

    public function bookingPlace($request)
    {
        $validator = Validator::make($request->all(),
            array(
                'Jwt' => 'required',
                'place_id' => 'required',
                'date_from' => 'required',
                'date_to' => 'required',
            )
        );
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $user = $this->CheckJwt($request->Jwt);
        $request['user_id'] = $user->id;
        $booking= PlaceBooking::create($request->all());
        $this->AddNotification($user->image, [$user->id],
            $title = ['titleen' => 'you booking place successfully',
                'titlear' =>'لقد قمت بحجز المكان بنجاح'],
            $text = ['texten' => 'sent you a new message',
                'textar' => 'لقد ارسل لك رسالة جديدة']);
        PushNotification::send_details([$user->firebase_token],'congratulations','your order has been booking',$booking->id,1);
        return 'true';
    }

    public function getPlaceReviewsDetails($place_id, $lang)
    {
        $reviews = Rate::where('place_id', $place_id)->get();
        $review_Items = [];
        $review_list = [];
        foreach ($reviews as $review) {
            $review_Items['id'] = $review->id;
            $review_Items['rating'] = $review->rate;
            $review_Items['review'] = $review->review;
            $user = User::where('id', $review->user_id)->select('id', 'name', 'image')->first();
            $review_Items['user_id'] = $user->id;
            $review_Items['user_name'] = $user->name;
            $review_Items['user_image'] = $user->image;
            $review_Items['created_at'] = $review->created_at;
            $review_list[] = $review_Items;
        }

        return $review_list;
    }

    public function RatePlace($request)
    {
        $Jwt = getallheaders()['Jwt'];
        $user = $this->CheckJwt($Jwt);

        $rate=Rate::where('place_id',$request->place_id)->where('user_id',$user->id)->first();
        if (isset($rate)){
            $rate->update($request->all());
            return true;
        }else{
            Rate::create($request->all());
            return true;
        }

    }

    public function getPlaceOrderDetails($order_id, $lang)
    {
        $order = PlaceBooking::where('id', $order_id)->get();
        $order_Items = [];
        $order_list = [];
        foreach ($order as $res) {
            $order_Items['order_id'] = $res->id;
            $order_Items['from'] = $res->date_from;
            $order_Items['to'] = $res->date_to;
            $placeImage = Place::where('id', $res->place_id)->select('main_image')->first();
            $order_Items['place_image'] = $placeImage->main_image;
            $order_list[] = $order_Items;
        }

        return $order_list;

    }

    public function editPlaceBooking($request)
    {
        $place = PlaceBooking::where('id', $request->order_id)->first();
        $place->update([
            'date_from' => $request->date_from,
            'date_to' => $request->date_to,
        ]);
    }

    public function getAllServiceSubCategories($lang, $service_category_id)
    {
        $allsubcategories = array();
        $data = array();
        $subcategories = ServiceSubcategory::where('service_category_id', $service_category_id)
            ->where('status', '1')->get();
        foreach ($subcategories as $subcategory) {
            $data['id'] = $subcategory->id;
            $data['name'] = $subcategory->translate($lang)->name;
            $data['image'] = $subcategory->image;
            array_push($allsubcategories, $data);
        }
        return $allsubcategories;
    }

    public function getAllServiceSubSubCategories($lang, $service_subcategory_id)
    {
        $allsubsubcategories = array();
        $data = array();
        $subsubcategories = SubSubCategory::where('service_subcategory_id', $service_subcategory_id)
            ->where('status', '1')->get();
        foreach ($subsubcategories as $subsubcategory) {
            $data['id'] = $subsubcategory->id;
            $data['name'] = $subsubcategory->translate($lang)->name;
            $data['image'] = $subsubcategory->image;
            array_push($allsubsubcategories, $data);
        }
        return $allsubsubcategories;
    }

    public function getAllItemsOfServicesOfSubSubCategories($lang, $service_subsubcategory_id)
    {
        $allsubsubcategories = array();
        $data = array();
        $subsubcategories = Service::where('service_sub_sub_category_id', $service_subsubcategory_id)->get();
        foreach ($subsubcategories as $subsubcategory) {
            $data['id'] = $subsubcategory->id;
            $data['present_type'] = $subsubcategory->present_type;
            $data['quantity'] = $subsubcategory->quantity;
            $data['image'] = $subsubcategory->image;
            $data['desc'] = $subsubcategory->desc;
            $data['price_before'] = $subsubcategory->price_before;
            $data['price_after'] = $subsubcategory->price_after;
            array_push($allsubsubcategories, $data);
        }
        return $allsubsubcategories;
    }

    public function CreateServiceBooking($request)
    {
        $order = PreparationBooking::create(
            array(
                'message' => $request->message,
                'date' => $request->date,
                'user_id' => $request->user_id,
            )
        );

        $services_sub_sub_category_id = $request->services_sub_sub_category_id;
        for ($i = 0; $i < count($services_sub_sub_category_id); $i++) {
            $price = Service::where('service_sub_sub_category_id', $services_sub_sub_category_id)->select('price_after')->first();
            $quantity = $request->quantity[$i];
            $total = $quantity * $price['price_after'];
            PreparationOrder::create(
                array(
                    'services_sub_sub_category_id' => $services_sub_sub_category_id[$i],
                    'preparation_booking_id' => $order->id,
                    'quantity' => $quantity[$i],
                )
            );
        }
        $order->total = $total;
        $order->save();
        return $order->orders_id;
    }

    public function editBookingService($request)
    {
        $order = PreparationBooking::where('id', $request->order_id)->first();
        $order->update([
            'date' => $request->date,
        ]);
        return $order;
    }

    public function getReservations($lang, $user_id)
    {
        Carbon::setlocale($lang);
        $reservations = PlaceBooking::where('user_id', $user_id)->where('status', '1')->orderBy('created_at', 'desc')->get();
        $orders_Items = [];
        $orders_list = [];
        foreach ($reservations as $res) {
            $orders_Items['order_id'] = $res->id;
            $orders_Items['from'] = $res->date_from;
            $orders_Items['to'] = $res->date_to;
            $places = Place::where('id', $res->place_id)->select('name', 'desc')->first();
            $orders_Items['place_name'] = $places->name;
            $orders_Items['place_desc'] = $places->desc;
            $orders_list[] = $orders_Items;
        }

        return $orders_list;
    }

    public function getPreparationsReservations($lang, $user_id)
    {
        Carbon::setlocale($lang);
        $reservations = PreparationBooking::where('user_id', $user_id)->where('status', '1')->orderBy('created_at', 'desc')->get();
        $orders_Items = [];
        $orders_list = [];
        foreach ($reservations as $res) {
            $orders_Items['order_id'] = $res->id;
            $orders_Items['message'] = $res->message;
            $orders_Items['date'] = $res->date;
            $servicesOrder = PreparationOrder::where('preparation_booking_id', $res->id)->pluck('services_sub_sub_category_id');
            $services=Service::whereIn('id',$servicesOrder)->select('present_type','desc')->get();
            $orders_Items['services']=$services;
            $orders_list[] = $orders_Items;
        }

        return $orders_list;
    }

    public function getOrders($user_id, $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;


        $getPlacesOrder = PlaceBooking::where('user_id', $user_id)->
        where('date_from', $date_from)->orWhere('date_to', $date_to)->get();

        $orders_Items = [];
        $orders_list = [];
        foreach ($getPlacesOrder as $res) {
            $orders_Items['order_id'] = $res->id;
            $orders_Items['from'] = $res->date_from;
            $orders_Items['to'] = $res->date_to;
            $places = Place::where('id', $res->place_id)->select('name', 'desc')->first();
            $orders_Items['place_name'] = $places->name;
            $orders_Items['place_desc'] = $places->desc;
            $orders_list[] = $orders_Items;
        }

        $getServicesOrder = PreparationBooking::where('user_id', $user_id)->where('date', $date_from)->get();
        $services_orders_Items = [];
        $services_orders_list = [];
        foreach ($getServicesOrder as $services_orders_res) {
            $services_orders_Items['order_id'] = $services_orders_res->id;
            $services_orders_Items['date'] = $services_orders_res->date;
            $getServiceData = PreparationOrder::where('preparation_booking_id', $services_orders_res->id)->pluck('services_sub_sub_category_id')->first();
            $service = Service::where('service_sub_sub_category_id', $getServiceData)->select('id', 'desc', 'present_type')->first();
            $services_orders_Items['services_id'] = $service->id;
            $services_orders_Items['services_name'] = $service->present_type;
            $services_orders_Items['services_desc'] = $service->desc;
            $services_orders_list[] = $services_orders_Items;
        }
        $data = array_merge($orders_list, $services_orders_list);

        return $data;
    }


    public function userSendMessage($request)
    {
        if (isset($request->message)) {
            $message = Chat::create([
                'sender' => $request->sender,
                'receiver' => $request->receiver,
                'message' => $request->message,
                'place_id' => $request->place_id,
            ]);
            $message['sender'] = $message->sender;
            $user = User::where('id', $request->receiver)->first();
            $this->AddNotification($user->image, [$user->id], $title = ['titleen' => $user->name,
                'titlear' => $user->name],
                $text = ['texten' => 'Sent you a new message',
                    'textar' => 'لقد ارسل لك رسالة جديدة']);
//            $this->AddNotification($user->image, [$user->id],
//                $title = ['titleen' => 'Sent you a new message',
//                    'titlear' =>'لقد قمت بحجز المكان بنجاح'],
//                $text = ['texten' => 'sent you a new message',
//                    'textar' => 'لقد ارسل لك رسالة جديدة']);

        } elseif (isset($request->image)) {
            $message = Chat::create([
                'sender' => $request->sender,
                'receiver' => $request->receiver,
                'message' => '',
                'place_id' => $request->place_id,
            ]);
            $message['sender'] = $message->sender;
            $user = User::where('id', $request->receiver)->first();
//            $this->AddNotification($user->image, [$user->id], 1, $request->place_id, $text = ['texten' => 'Sent you a new message',
//                'textar' => 'لقد ارسل لك رسالة جديدة'], $title = ['titleen' => $user->name,
//                'titlear' => $user->name]);
            $this->AddNotification($user->image, [$user->id], $title = ['titleen' => $user->name,
                'titlear' => $user->name],
                $text = ['texten' => 'Sent you a new message',
                    'textar' => 'لقد ارسل لك رسالة جديدة']);
            for ($i = 0; $i < count($request->image); $i++) {
                $message->related_images()->create(['image' => $request->image[$i],
                    'default_image' => 0,
                ]);
            }
        }
        return $message;
    }

    public function getAllusersChat($Jwt)
    {
        $user = User::where('jwt_token', $Jwt)->first();
        $getChatWithOtherUsers = Chat::groupBy('place_id')->where('sender', $user->id)->orWhere('receiver', $user->id)->orderBy('created_at', 'desc')->get();
        $res_item = [];
        $res_list = [];
        foreach ($getChatWithOtherUsers as $chat) {
            $res_item['chat_id'] = $chat->id;
            $res_item['created_at'] = $chat->created_at;

            if ($chat->sender == $user->id) {
                $data = User::where('id', $chat->receiver)->select('id', 'name', 'image')->first();
                $res_item['user_name'] = $data->name;
                $res_item['user_id'] = $data->id;
                $res_item['user_image'] = $data->image;
            }
            if ($chat->receiver == $user->id) {
                $data = User::where('id', $chat->sender)->select('id', 'name', 'image')->first();
                $res_item['user_name'] = $data->name;
                $res_item['user_id'] = $data->id;
                $res_item['user_image'] = $data->image;
            }
            $res_list[] = $res_item;
        }

        return $res_list;

    }

    public function GetAllMessages($Jwt, $request)
    {
        $user = User::where('jwt_token', $Jwt)->first();
        $user_id=$user->id;
        $reciever_id= $request->receiver;
        ////$chat = Chat::where('sender', $user->id)->orWhere('receiver', $request->receiver)->get();
        $chat = DB::select('SELECT *
                FROM chats
                Where
                (sender = "'.$user_id.'" and receiver = "'.$reciever_id.'")
                OR
                (sender = "'.$reciever_id.'" and receiver = "'.$user_id.'")
                ');

        $res_item = [];
        $res_list = [];
        foreach ($chat as $res) {
            $res_item['id'] = $res->id;
            $senderUser = User::where('id', $res->sender)->select('id', 'name')->first();
            $res_item['sender_name'] = $senderUser->name;
            $res_item['sender_id'] = $senderUser->id;
            $recieverUser = User::where('id', $res->receiver)->select('id', 'name')->first();
            $res_item['receiver_name'] = $recieverUser->name;
            $res_item['receiver_id'] = $recieverUser->id;
//            if ($user->id == 11) {
//                $res_item['is_reciever'] = 'yes';
//                $anotherUser = User::where('id', $res->sender)->select('id', 'name')->first();
//                $res_item['another_user_name'] = $anotherUser->name;
//                $res_item['another_user_id'] = $anotherUser->id;
//            } else {
//                $res_item['is_sender'] = 'yes';
//                $anotherUser = User::where('id', $res->receiver)->select('id', 'name')->first();
//                $res_item['another_user_name'] = $anotherUser->name;
//                $res_item['another_user_id'] = $anotherUser->id;
//            }

            $res_item['created_at'] = $res->created_at;
            if ($res->message == '') {
//                $getImageId = Chat::where('id', $res->id)->pluck('id');
                $getChatImages = ChatImage::where('chat_id', $res->id)->select('id', 'image')->get();
                $res_item['chat_images'] = $getChatImages;
                $res_item['message'] ='';

            } else {
                $res_item['message'] = $res->message;
                $res_item['chat_images']="";
            }
            $res_list[] = $res_item;
        }
        return $res_list;
    }
}
