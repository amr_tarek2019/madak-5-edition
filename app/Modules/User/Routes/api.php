<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/user', function (Request $request) {
//    // return $request->user();
//})->middleware('auth:api');

Route::group(['prefix' => 'user'], function () {
//    Route::get('/getplacescategories','Api\UserController@getAllPlacesCategories');
//    Route::get('/getpreparationscategories','Api\UserController@getAllPreparationsCategories');
//    Route::get('/getgenders','Api\UserController@getGender');
    Route::get('/department/items/{department_id}','Api\UserController@getDepartmentsItems');
    Route::get('/departments','Api\UserController@getAllDepartments');
    Route::get('/filterminmaxprices/{department_id}','Api\UserController@filterMinMaxPrices');
    Route::post('/search','Api\UserController@search');
    Route::get('/allplaces/{place_category_id}','Api\UserController@getAllPlacesCategory');
    Route::get('/place/{place_id}','Api\UserController@getPlaceDetails');
    Route::post('/bookingplace','Api\UserController@bookingPlace');
    Route::get('/place/reviews/{place_id}','Api\UserController@getPlaceReviewsDetails');
    Route::post('/rateplace','Api\UserController@RatePlace');
    Route::get('/orderplacedetails/{order_id}','Api\UserController@getPlaceOrderDetails');
    Route::Post('/updatebookingplace','Api\UserController@editPlaceBooking');
    Route::get('/allservicessubcategories/{service_category_id}','Api\UserController@getAllServiceSubCategories');
    Route::get('/allservicessubsubcategories/{service_subcategory_id}','Api\UserController@getAllServiceSubSubCategories');
    Route::get('service/items/{service_subsubcategory_id}','Api\UserController@getAllItemsOfServicesOfSubSubCategories');
    Route::post('/booking/services','Api\UserController@CreateServiceBooking');
    Route::Post('/updatebookingservice','Api\UserController@editBookingService');
    Route::get('/getReservations','Api\UserController@getReservations');
    Route::get('/getPreparationsReservations','Api\UserController@getPreparationsReservations');
    Route::get('/orders','Api\UserController@getOrders');
    Route::post('/sendmessage','Api\UserController@userSendMessage');
    Route::get('/getAllusersChat','Api\UserController@getAllusersChat');
    Route::get('/getallmessages','Api\UserController@GetAllMessages');

});
