<?php

namespace App\Modules\Contact\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('contact', 'Resources/Lang', 'app'), 'contact');
        $this->loadViewsFrom(module_path('contact', 'Resources/Views', 'app'), 'contact');
        $this->loadMigrationsFrom(module_path('contact', 'Database/Migrations', 'app'), 'contact');
        $this->loadConfigsFrom(module_path('contact', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('contact', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
