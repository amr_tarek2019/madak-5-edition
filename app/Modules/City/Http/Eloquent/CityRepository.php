<?php
namespace App\Modules\City\Http\Eloquent;

use App\Models\City;
use App\Modules\City\Http\Interfaces\CityRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class CityRepository implements CityRepositoryInterface
{
    public function getCities($lang)
    {
        $cities = City::where('status','1')->get();

        $allCities = array();
        $i = 0;
        foreach ($cities as $city) {
            $allCities[$i] = array(
                'id'=>$city->id,
                'name' =>$city->translate($lang)->name
            );
            $i++;
        }
        return $allCities;


    }

}
