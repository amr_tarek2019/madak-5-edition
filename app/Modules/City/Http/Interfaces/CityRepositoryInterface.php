<?php
namespace App\Modules\City\Http\Interfaces;

interface CityRepositoryInterface
{
    public function getCities($lang);
}



