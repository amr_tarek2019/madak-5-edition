<?php
namespace App\Modules\Place\Http\Eloquent;

use App\Models\Place;
use App\Models\PlacesPreparations;
use App\Modules\Place\Http\Interfaces\PlaceRepositoryInterface;

class PlaceRepository implements PlaceRepositoryInterface
{
    public function addPlace($request)
    {
            $place = Place::create([
                'place_category_id' => $request->place_category_id,
                'city_id' => $request->city_id,
                'gender' => $request->gender,
                'name' => $request->name,
                'present_type' => $request->present_type,
                'price' => $request->price,
                'provider_id' => $request->provider_id,
                'quantity' => $request->quantity,
                'date_from' => $request->date_from,
                'date_to' => $request->date_to,
                'lat' => $request->lat,
                'lng' => $request->lng,
                'address' => $request->address,
                'desc' => $request->desc,
                'preparations_id'=>$request->preparations_id,
                'main_image'=>$request->imgs[0],
            ]);

        for ($i = 1; $i < count($request->imgs); $i++) {
            $place->related_images()->create(['image' => $request->imgs[$i],
                'default_image' => 0,
            ]);
        }
        for($i=0;$i<count($request->preparations_id);$i++)
        {
            $placesPreparations=new PlacesPreparations();
            $placesPreparations->place_id=$place->id;
            $placesPreparations->preparations_id=$request->preparations_id[$i];
            $placesPreparations->save();
        }

    }

}
