<?php
namespace App\Modules\Place\Http\Interfaces;

interface PlaceRepositoryInterface
{
    public function addPlace($request);
}
