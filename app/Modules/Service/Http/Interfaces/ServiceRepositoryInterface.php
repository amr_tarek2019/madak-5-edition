<?php
namespace App\Modules\Service\Http\Interfaces;

interface ServiceRepositoryInterface
{
    public function addService($request);
}
