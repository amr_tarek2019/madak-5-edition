<?php
namespace App\Modules\Service\Http\Eloquent;

use App\Models\Service;
use App\Modules\Service\Http\Interfaces\ServiceRepositoryInterface;

class ServiceRepository implements ServiceRepositoryInterface
{
    public function addService($request)
    {
        $service= Service::create([
            'service_category_id' => $request->service_category_id,
            'service_subcategory_id' => $request->service_subcategory_id,
            'service_sub_sub_category_id' => $request->service_sub_sub_category_id,
            'city_id' => $request->city_id,
            'gender' => $request->gender,
            'present_type' => $request->present_type,
            'price_before' => $request->price_before,
            'price_after' => $request->price_after,
            'provider_id' => $request->provider_id,
            'quantity' => $request->quantity,
            'date' => $request->date,
            'lat' => $request->lat,
            'lng' => $request->lng,
            'address' => $request->address,
            'desc' => $request->desc,
            'image' => $request->image,
        ]);

         return $service;
    }

}
