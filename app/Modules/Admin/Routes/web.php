<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/clear',function (){
    \Illuminate\Support\Facades\Artisan::call('config:clear');
});

Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::group(['middleware' => ['role:Super Admin|Settings Admin']], function () {

            Route::get('/dashboard', 'DashboardController@index')->name('dashboard.index');


            Route::prefix('city')->group(function (){
                Route::get('','CityController@index')->name('cities.index');
                Route::get('/create','CityController@create')->name('cities.create');
                Route::post('/store','CityController@store')->name('cities.store');
                Route::get('/edit/{id}','CityController@edit')->name('cities.edit');
                Route::post('/update/{id}','CityController@update')->name('cities.update');
                Route::post('/{id}','CityController@destroy')->name('cities.destroy');
                Route::post('/status/{id}', 'CityController@updateStatus')->name('cities.status');
            });


            Route::prefix('contact')->group(function (){
                Route::get('','ContactsController@index')->name('contacts.index');
                Route::get('/show/{id}','ContactsController@show')->name('contacts.show');
                Route::post('/{id}','ContactsController@destroy')->name('contacts.destroy');
            });

            Route::prefix('issue')->group(function (){
                Route::get('','IssuesController@index')->name('issues.index');
                Route::get('/create','IssuesController@create')->name('issues.create');
                Route::post('/store','IssuesController@store')->name('issues.store');
                Route::get('/edit/{id}','IssuesController@edit')->name('issues.edit');
                Route::post('/update/{id}','IssuesController@update')->name('issues.update');
                Route::post('/{id}','IssuesController@destroy')->name('issues.destroy');
                Route::post('/status/{id}', 'IssuesController@updateStatus')->name('issues.status');
            });


            Route::prefix('service-provider-requests')->group(function (){
                Route::get('','ServiceProviderRequestController@index')->name('services.providers.requests.index');
                Route::get('/edit/{id}','ServiceProviderRequestController@edit')->name('services.providers.requests.edit');
                Route::post('/update/{id}','ServiceProviderRequestController@update')->name('services.providers.requests.update');
                Route::post('/{id}','ServiceProviderRequestController@destroy')->name('services.providers.requests.destroy');
            });


            Route::prefix('users')->group(function (){
                Route::get('','UserController@index')->name('users.index');
                Route::get('/details/{id}','UserController@show')->name('users.show');
                Route::post('/{id}','UserController@destroy')->name('users.destroy');
                Route::post('/status/{id}', 'UserController@updateStatus')->name('users.status');
            });

            Route::prefix('providers')->group(function (){
                Route::get('','ProviderController@index')->name('providers.index');
                Route::get('/details/{id}','ProviderController@show')->name('providers.show');
                Route::post('/{id}','ProviderController@destroy')->name('providers.destroy');
                Route::post('/status/{id}', 'ProviderController@updateStatus')->name('providers.status');
            });


            Route::prefix('admins')->group(function (){
                Route::get('','AdminsController@index')->name('admins.index');
                Route::get('/create','AdminsController@create')->name('admins.create');
                Route::post('/store','AdminsController@store')->name('admins.store');
                Route::get('/edit/{id}','AdminsController@edit')->name('admins.edit');
                Route::post('/update/{id}','AdminsController@update')->name('admins.update');
                Route::post('/{id}','AdminsController@destroy')->name('admins.destroy');
            });

            Route::prefix('place/category')->group(function (){
                Route::get('','PlacesCategoriesController@index')->name('places.categories.index');
                Route::get('/create','PlacesCategoriesController@create')->name('places.categories.create');
                Route::post('/store','PlacesCategoriesController@store')->name('places.categories.store');
                Route::get('/edit/{id}','PlacesCategoriesController@edit')->name('places.categories.edit');
                Route::post('/update/{id}','PlacesCategoriesController@update')->name('places.categories.update');
                Route::post('/{id}','PlacesCategoriesController@destroy')->name('places.categories.destroy');
                Route::post('/status/{id}', 'PlacesCategoriesController@updateStatus')->name('places.categories.status');
            });


            Route::prefix('places')->group(function (){
                Route::get('','PlacesController@index')->name('places.index');
                Route::get('/details/{id}','PlacesController@show')->name('places.show');
                Route::get('/edit/{id}','PlacesController@edit')->name('places.edit');
                Route::post('/update/{id}','PlacesController@update')->name('places.update');
                Route::post('/{id}','PlacesController@destroy')->name('places.destroy');
            });

            Route::prefix('service/category')->group(function (){
                Route::get('','ServiceCategoryController@index')->name('services.categories.index');
                Route::get('/create','ServiceCategoryController@create')->name('services.categories.create');
                Route::post('/store','ServiceCategoryController@store')->name('services.categories.store');
                Route::get('/edit/{id}','ServiceCategoryController@edit')->name('services.categories.edit');
                Route::post('/update/{id}','ServiceCategoryController@update')->name('services.categories.update');
                Route::post('/{id}','ServiceCategoryController@destroy')->name('services.categories.destroy');
                Route::post('/status/{id}', 'ServiceCategoryController@updateStatus')->name('services.categories.status');
            });

            Route::prefix('service/subcategory')->group(function (){
                Route::get('','ServiceSubcategoryController@index')->name('services.subcategories.index');
                Route::get('/create','ServiceSubcategoryController@create')->name('services.subcategories.create');
                Route::post('/store','ServiceSubcategoryController@store')->name('services.subcategories.store');
                Route::get('/edit/{id}','ServiceSubcategoryController@edit')->name('services.subcategories.edit');
                Route::post('/update/{id}','ServiceSubcategoryController@update')->name('services.subcategories.update');
                Route::post('/{id}','ServiceSubcategoryController@destroy')->name('services.subcategories.destroy');
                Route::post('/status/{id}', 'ServiceSubcategoryController@updateStatus')->name('services.subcategories.status');
            });

            Route::prefix('service/subsubcategory')->group(function (){
                Route::get('','ServiceSubsubcategoryController@index')->name('services.subsubcategories.index');
                Route::get('/create','ServiceSubsubcategoryController@create')->name('services.subsubcategories.create');
                Route::post('/store','ServiceSubsubcategoryController@store')->name('services.subsubcategories.store');
                Route::get('/edit/{id}','ServiceSubsubcategoryController@edit')->name('services.subsubcategories.edit');
                Route::post('/update/{id}','ServiceSubsubcategoryController@update')->name('services.subsubcategories.update');
                Route::post('/{id}','ServiceSubsubcategoryController@destroy')->name('services.subsubcategories.destroy');
                Route::post('/status/{id}', 'ServiceSubsubcategoryController@updateStatus')->name('services.subsubcategories.status');
            });

            Route::prefix('services')->group(function (){
                Route::get('','ServiceController@index')->name('services.index');
                Route::get('/details/{id}','ServiceController@show')->name('services.show');
                Route::get('/edit/{id}','ServiceController@edit')->name('services.edit');
                Route::post('/update/{id}','ServiceController@update')->name('services.update');
                Route::post('/{id}','ServiceController@destroy')->name('services.destroy');
            });

            Route::prefix('places/reservations')->group(function (){
                Route::get('','PlacesOrdersController@index')->name('places.reservations.index');
                Route::get('/details/{id}','PlacesOrdersController@show')->name('places.reservations.show');
                Route::get('/edit/{id}','PlacesOrdersController@edit')->name('places.reservations.edit');
                Route::post('/update/{id}','PlacesOrdersController@update')->name('places.reservations.update');
                Route::post('/{id}','PlacesOrdersController@destroy')->name('places.reservations.destroy');
                Route::get('/invoice/{id}','PlacesOrdersController@generatePdf')->name('places.reservations.invoice');
            });

            Route::prefix('services/reservations')->group(function (){
                Route::get('','ServicesOrdersController@index')->name('services.reservations.index');
                Route::get('/details/{id}','ServicesOrdersController@show')->name('services.reservations.show');
                Route::get('/edit/{id}','ServicesOrdersController@edit')->name('services.reservations.edit');
                Route::post('/update/{id}','ServicesOrdersController@update')->name('services.reservations.update');
                Route::post('/{id}','ServicesOrdersController@destroy')->name('services.reservations.destroy');
                Route::get('/invoice/{id}','ServicesOrdersController@generatePdf')->name('services.reservations.invoice');
            });

            Route::group(['prefix' => 'settings'], function () {
                Route::get('', 'SettingsController@index')->name('settings');
                Route::post('/update','SettingsController@update')->name('settings.update');
            });
        });
    });
    Route::get('/login', 'AdminsController@login_form')->name('login');
    Route::post('/login', 'AdminsController@login')->name('admins.login');
    Route::get('/logout', 'AdminsController@logout')->name('logout');
    Route::get('/profile', 'AdminsController@profile')->name('admins.profile');
    Route::post('/update-profile/{id}', 'AdminsController@updateProfile')->name('admins.profile.update');
    Route::get('/backup','AdminsController@backup')->name('backup');
});
