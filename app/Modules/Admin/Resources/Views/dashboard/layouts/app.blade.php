@php
    $generalsetting = \App\Models\Settings::first();
@endphp
<!DOCTYPE html>
@if(request()->segment(1)=='en')
    <html lang="en" dir="ltr">
    @else
        <html lang="ar" dir="rtl">
        @endif
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="endless admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, endless admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="{{$generalsetting->logo}}" type="image/x-icon">
    <link rel="shortcut icon" href="{{$generalsetting->logo}}" type="image/x-icon">
    <title>{{$generalsetting->app_name}} - Dashboard</title>
    <!-- Google font-->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/fontawesome.css') }}">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/icofont.css') }}">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/themify.css') }}">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/flag-icon.css') }}">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/feather-icon.css') }}">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/owlcarousel.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/chartist.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/prism.css') }}">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/bootstrap.css') }}">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/style.css') }}">
    <link id="color" rel="stylesheet" href="{{ asset('admin/assets/css/light-1.css') }}" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/css/datatables.css') }}">

</head>
        @if(request()->segment(1)=='ar')
            <body main-theme-layout="rtl">

            @else
                <body>

                @endif
<!-- Loader starts-->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="whirly-loader"> </div>
    </div>
</div>
<!-- Loader ends-->
<!-- page-wrapper Start-->
                @if(request()->segment(1)=='en')
                    <div class="page-wrapper">
                        @else
                            <div class="page-wrapper" dir="rtl">
                            @endif
    <!-- Page Header Start-->
@include('admin::dashboard.layouts.header')
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
                                @if(request()->segment(1)=='en')
                                    <div class="page-body-wrapper">
                                        @else
                                            <div class="page-body-wrapper" dir="rtl">
                                            @endif
        <!-- Page Sidebar Start-->
    @include('admin::dashboard.layouts.sidebar')
        <!-- Page Sidebar Ends-->
        <!-- Right sidebar Start-->
        <div class="right-sidebar" id="right_side_bar">
            <div class="container p-0">
                <div class="modal-header p-l-20 p-r-20">
                    <div class="col-sm-8 p-0">
                        <h6 class="modal-title font-weight-bold">FRIEND LIST</h6>
                    </div>
                    <div class="col-sm-4 text-right p-0"><i class="mr-2" data-feather="settings"></i></div>
                </div>
            </div>
            <div class="friend-list-search mt-0">
                <input type="text" placeholder="search friend"><i class="fa fa-search"></i>
            </div>
            <div class="chat-box">
                <div class="people-list friend-list">
                    <ul class="list">
                        <li class="clearfix"><img class="rounded-circle user-image" src="{{ asset('admin/assets/images/user/1.jpg') }}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Vincent Porter</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image" src="{{ asset('admin/assets/images/user/2.png') }}" alt="">
                            <div class="status-circle away"></div>
                            <div class="about">
                                <div class="name">Ain Chavez</div>
                                <div class="status"> 28 minutes ago</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image" src="{{ asset('admin/assets/images/user/8.jpg') }}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Kori Thomas</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image" src="{{ asset('admin/assets/images/user/4.jpg') }}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Erica Hughes</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image" src="{{ asset('admin/assets/images/user/5.jpg') }}" alt="">
                            <div class="status-circle offline"></div>
                            <div class="about">
                                <div class="name">Ginger Johnston</div>
                                <div class="status"> 2 minutes ago</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image" src="{{ asset('admin/assets/images/user/6.jpg') }}" alt="">
                            <div class="status-circle away"></div>
                            <div class="about">
                                <div class="name">Prasanth Anand</div>
                                <div class="status"> 2 hour ago</div>
                            </div>
                        </li>
                        <li class="clearfix"><img class="rounded-circle user-image" src="{{ asset('admin/assets/images/user/7.jpg') }}" alt="">
                            <div class="status-circle online"></div>
                            <div class="about">
                                <div class="name">Hileri Jecno</div>
                                <div class="status"> Online</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Right sidebar Ends-->
        <div class="page-body">
            @yield('content')
        </div>
        <!-- footer start-->
        @include('admin::dashboard.layouts.footer')
    </div>
</div>
<!-- latest jquery-->

<script src="{{ asset('admin/assets/js/jquery-3.2.1.min.js') }}"></script>
<!-- Bootstrap js-->
<script src="{{ asset('admin/assets/js/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/bootstrap/bootstrap.js') }}"></script>
<!-- feather icon js-->
<script src="{{ asset('admin/assets/js/icons/feather-icon/feather.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/icons/feather-icon/feather-icon.js') }}"></script>
<!-- Sidebar jquery-->
<script src="{{ asset('admin/assets/js/sidebar-menu.js') }}"></script>
<script src="{{ asset('admin/assets/js/config.js') }}"></script>
<!-- Plugins JS start-->
<script src="{{ asset('admin/assets/js/chart/morris-chart/raphael.js') }}"></script>
<script src="{{ asset('admin/assets/js/chart/morris-chart/morris.js') }}"></script>
<script src="{{ asset('admin/assets/js/chart/morris-chart/prettify.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/chart/chartjs/chart.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/chart/chartist/chartist.js') }}"></script>
<script src="{{ asset('admin/assets/js/chart/knob/knob.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/chart/knob/knob-chart.js') }}"></script>
<script src="{{ asset('admin/assets/js/prism/prism.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/clipboard/clipboard.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/counter/jquery.waypoints.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/counter/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/counter/counter-custom.js') }}"></script>
<script src="{{ asset('admin/assets/js/custom-card/custom-card.js') }}"></script>
<script src="{{ asset('admin/assets/js/dashboard/dashboard-ecommerce/chart.custom.js') }}"></script>
<script src="{{ asset('admin/assets/js/dashboard/dashboard-ecommerce/morris-script.js') }}"></script>
<script src="{{ asset('admin/assets/js/dashboard/dashboard-ecommerce/owl-carousel.js') }}"></script>
<script src="{{ asset('admin/assets/js/dashboard/default.js') }}"></script>
<script src="{{ asset('admin/assets/js/typeahead/handlebars.js') }}"></script>
<script src="{{ asset('admin/assets/js/typeahead/typeahead.bundle.js') }}"></script>
<script src="{{ asset('admin/assets/js/typeahead/typeahead.custom.js') }}"></script>
<script src="{{ asset('admin/assets/js/chat-menu.js') }}"></script>
<script src="{{ asset('admin/assets/js/height-equal.js') }}"></script>
<script src="{{ asset('admin/assets/js/tooltip-init.js') }}"></script>
<script src="{{ asset('admin/assets/js/typeahead-search/handlebars.js') }}"></script>
<script src="{{ asset('admin/assets/js/typeahead-search/typeahead-custom.js') }}"></script>
<script src="{{ asset('admin/assets/js/datatable/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('admin/assets/js/datatable/datatables/datatable.custom.js') }}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{ asset('admin/assets/js/script.js') }}"></script>
<!-- Plugin used-->

<script>
    $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
</script>
@yield('myjsfile')
</body>
</html>
