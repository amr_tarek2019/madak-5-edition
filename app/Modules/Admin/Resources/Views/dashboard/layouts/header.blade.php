<div class="page-main-header">
    <div class="main-header-right row">
        <div class="main-header-left d-lg-none">
            <div class="logo-wrapper"><a href="index.html" data-original-title="" title=""><img src="{{ asset('admin/assets/images/endless-logo.png')}}" alt="" data-original-title="" title=""></a></div>
        </div>
        <div class="mobile-sidebar d-block">
            <div class="media-body text-left switch-sm">
                <label class="switch"><a href="#" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-align-left" id="sidebar-toggle"><line x1="17" y1="10" x2="3" y2="10"></line><line x1="21" y1="6" x2="3" y2="6"></line><line x1="21" y1="14" x2="3" y2="14"></line><line x1="17" y1="18" x2="3" y2="18"></line></svg></a></label>
            </div>
        </div>
        <div class="nav-right col p-0">
            <ul class="nav-menus">
                <li>

                </li>
                <li class="onhover-dropdown"><a class="txt-dark" data-original-title="" title="">
                        <h6>     @if(request()->segment(1)=='en')
                                EN
                            @else
                                AR
                            @endif</h6></a>
                    <ul class="language-dropdown onhover-show-div p-20">
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li><a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}" data-lng="en"><i class="flag-icon"></i> {{ $properties['native'] }}</a></li>
                        @endforeach
                    </ul>
                </li>
                <li class="onhover-dropdown"><i data-feather="mail"></i><span class="dot"></span>
                    <ul class="notification-dropdown onhover-show-div">
                        <li>{{trans('translate.Contacts')}}<span class="badge badge-pill badge-primary pull-right">{{countUnreadMsg()}}</span></li>
                        @if(countUnreadMsg()>0)
                            @foreach(unreadMsg() as $keyMessages => $valueMessage)
                                <li>
                                    <a href="{{route('contacts.show',$valueMessage->id)}}">
                                        <div class="media">
                                            <div class="media-body">
                                                <h6 class="mt-0"><span><i class="shopping-color" data-feather="mail"></i></span>{{$valueMessage->name}}<small class="pull-right">{{$valueMessage->created_at}}</small></h6>
                                                <p class="mb-0">{{str_limit($valueMessage->message,10)}}.</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                            <a href="{{route('contacts.index')}}"><li class="bg-light txt-dark">{{trans('translate.seeAllMessages')}}</li></a>
                        @else
                            <li class="all-msgs text-center">
                                <p class="m-0"><a href="#">{{trans('translate.nomessagesfound')}}</a></p>
                            </li>
                        @endif
                    </ul>
                </li>
                <li class="onhover-dropdown">
                    <div class="media align-items-center"><img class="align-self-center pull-right img-50 rounded-circle" src="{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->image}}" alt="header-user" data-original-title="" title="">
                        <div class="dotted-animation"><span class="animate-circle"></span><span class="main-circle"></span></div>
                    </div>
                    @if(request()->segment(1)=='en')
                    <ul class="profile-dropdown onhover-show-div p-20">
                        @else
                            <ul class="profile-dropdown onhover-show-div p-20" style="margin-right: -35px">
                                @endif
                        <li><a href="{{route('admins.profile')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>   {{trans('translate.edit profile')}}</a></li>
                        <li><a href="{{route('contacts.index')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>  {{trans('translate.Contacts')}} </a></li>
                        <li><a href="{{route('backup')}}"><i data-feather="database"></i> {{trans('translate.database backup')}}</a></li>
                        <li><a href="{{route('logout')}}" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg>  {{trans('translate.logout')}}                                  </a></li>
                    </ul>
                </li>
            </ul>
            <div class="d-lg-none mobile-toggle pull-right"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-more-horizontal"><circle cx="12" cy="12" r="1"></circle><circle cx="19" cy="12" r="1"></circle><circle cx="5" cy="12" r="1"></circle></svg></div>
        </div>


    </div>
</div>
