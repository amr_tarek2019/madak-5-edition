<div class="page-sidebar">
    @if(request()->segment(1)=='en')
    <div class="main-header-left d-none d-lg-block">
        <div class="logo-wrapper"><a href="{{route('dashboard.index')}}"><img src="{{$generalsetting->logo}}" alt=""></a></div>
    </div>
    @else
        <div class="main-header-right d-none d-lg-block">
            <div class="logo-wrapper"><a href="{{route('dashboard.index')}}"><img src="{{$generalsetting->logo}}" style="margin-left: 80px" width="100px" alt=""></a></div>
        </div>
    @endif
    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle" src="{{Illuminate\Support\Facades\Auth::guard('admin')->user()->image}}" alt="#">
                <div class="profile-edit"><a href="edit-profile.html" target="_blank"><i data-feather="edit"></i></a></div>
            </div>
            <h6 class="mt-3 f-14">{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->name}}</h6>
            <p>{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->email}}</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{route('dashboard.index')}}"><i data-feather="home"></i><span>{{trans('translate.Dashboard')}}</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('cities.index')}}"><i data-feather="map"></i>
                    <span>{{trans('translate.city')}}</span></a></li>
            <li><a class="sidebar-header" href="{{route('issues.index')}}"><i data-feather="alert-circle"></i>
                    <span>{{trans('translate.issues')}}</span></a></li>

{{--            <li><a class="sidebar-header" href="index.html#"><i data-feather="disc"></i>--}}
{{--                    <span>Places Categories</span></a>--}}
{{--            </li>--}}

{{--            <li><a class="sidebar-header" href="index.html#"><i data-feather="disc"></i>--}}
{{--                    <span>Services Categories</span></a>--}}
{{--            </li>--}}

{{--            <li><a class="sidebar-header" href="index.html#"><i data-feather="disc"></i>--}}
{{--                    <span>Services Subcategories</span></a>--}}
{{--            </li>--}}

{{--            <li><a class="sidebar-header" href="index.html#"><i data-feather="disc"></i>--}}
{{--                    <span>Services Subsubcategories</span></a>--}}
{{--            </li>--}}

            <li><a class="sidebar-header" href="{{route('users.index')}}"><i data-feather="users"></i>
                    <span>{{trans('translate.users')}}</span></a>
            </li>

            <li><a class="sidebar-header" href="{{route('providers.index')}}"><i data-feather="users"></i>
                    <span>{{trans('translate.providers')}}</span></a>
            </li>

            <li><a class="sidebar-header" href="{{route('admins.index')}}"><i data-feather="users"></i>
                    <span>{{trans('translate.admins')}}</span></a>
            </li>

            <li><a class="sidebar-header" href="{{route('services.providers.requests.index')}}"><i data-feather="edit-3"></i>
                    <span>{{trans('translate.service provider requests')}}</span></a>
            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="map-pin"></i>
                    <span>{{trans('translate.places')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('places.categories.index')}}"><i class="fa fa-circle"></i>{{trans('translate.categories')}}</a></li>
                    <li><a href="{{route('places.index')}}"><i class="fa fa-circle"></i>{{trans('translate.operations')}}</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="index.html#"><i data-feather="server"></i>
                    <span>{{trans('translate.services')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('services.categories.index')}}"><i class="fa fa-circle"></i>{{trans('translate.categories')}}</a></li>
                    <li><a href="{{route('services.subcategories.index')}}"><i class="fa fa-circle"></i>{{trans('translate.subcategories')}}</a></li>
                    <li><a href="{{route('services.subsubcategories.index')}}"><i class="fa fa-circle"></i>{{trans('translate.subsubcategories')}}</a></li>
                    <li><a href="{{route('services.index')}}"><i class="fa fa-circle"></i>{{trans('translate.operations')}}</a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href="index.html#"><i data-feather="layers"></i><span>{{trans('translate.orders')}}</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="{{route('places.reservations.index')}}"><i class="fa fa-circle"></i>{{trans('translate.places')}}</a></li>
                    <li><a href="{{route('services.reservations.index')}}"><i class="fa fa-circle"></i>{{trans('translate.services')}}</a></li>
                </ul>
            </li>
            <li><a class="sidebar-header" href="{{route('settings')}}"><i data-feather="settings"></i>
                    <span>{{trans('translate.settings')}}</span></a>
            </li>

            <li><a class="sidebar-header" href="{{route('contacts.index')}}"><i data-feather="mail"></i>
                    <span>{{trans('translate.contact us')}}</span></a>
            </li>



        </ul>
    </div>
</div>
