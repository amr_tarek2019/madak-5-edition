@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('translate.CONTACTS TABLE')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('translate.Contacts')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('flash::message')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('translate.CONTACTS TABLE')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('translate.name')}}</th>
                                        <th>{{trans('translate.email')}}</th>
                                        <th>{{trans('translate.issues')}}</th>
                                        <th>{{trans('translate.message')}}</th>
                                        <th>{{trans('translate.view')}}</th>
                                        <th>{{trans('translate.created at')}}</th>
                                        <th>{{trans('translate.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($suggestions as $key=>$suggestion)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$suggestion->name}}</td>
                                            <td>{{$suggestion->email}}</td>
                                            <td>{{str_limit($suggestion->issue->translate('en')->text,100)}}</td>
                                            <td>{{$suggestion->message}}</td>
                                            <td>      @if($suggestion->view == true)
                                                    <span class="label label-info">viewd</span>
                                                @else
                                                    <span class="label label-danger">not viewd yet</span>
                                                @endif</td>
                                            <td>{{$suggestion->created_at}}</td>
                                            <td>
                                                <a href="{{ route('contacts.show',$suggestion->id) }}" class="btn btn-success btn-lg active"><span class="fa fa-eye"></span></a>

                                                <form id="delete-form-{{ $suggestion->id }}" action="{{ route('contacts.destroy',$suggestion->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-success btn-lg active" onclick="if(confirm('{{trans('translate.are you sure ? yo want to delete this field ?')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $suggestion->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





@endsection
