@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.SERVICES PROVIDERS REQUESTS')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.SERVICES PROVIDERS REQUESTS')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.update SERVICES PROVIDERS REQUESTS')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                <p>{{ $error }}</p>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

            </div>

        @endforeach
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.update SERVICES PROVIDERS REQUESTS')}}</h5>
                    </div>
                    <form class="form theme-form" action="{{route('services.providers.requests.update',$requestProvider->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.status')}}</label>
                                        <select class="form-control digits" name="status" id="status">
                                            <option value="0" {{ isset($requestProvider) && $requestProvider->status == 0 ? 'selected'  :'' }}>refused</option>
                                            <option value="1" {{ isset($requestProvider) && $requestProvider->status == 1 ? 'selected'  :'' }}>accepted</option>
                                        </select>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.name')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" name="name" id="name" placeholder="{{trans('translate.name')}}">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.email')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="email" name="email" id="email" placeholder="{{trans('translate.email')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.phone')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="tel" name="phone" id="phone" placeholder="{{trans('translate.phone')}}">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.password')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="password" id="password" name="password" placeholder="{{trans('translate.password')}}">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <button class="btn btn-primary" type="submit">{{trans('translate.submit')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
