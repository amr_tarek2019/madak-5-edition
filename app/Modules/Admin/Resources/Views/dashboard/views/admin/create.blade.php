@extends('admin::dashboard.layouts.app')
@section('content')

    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}"><i data-feather="home"> </i> {{trans('translate.Dashboard')}} </a></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid starts-->
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                        <p>{{ $error }}</p>
                        <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

                    </div>

                @endforeach
            @endif

            <div class="container-fluid">
                <form  action="{{route('admins.store')}}" method="post" enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    <fieldset>

                        <!-- Form Name -->
                        <h6 class="m-t-10">{{trans('translate.addnewadmin')}}</h6><hr>

                        <!-- Text input-->
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-left" for="textinput">{{trans('translate.name')}} </label>
                            <div class="col-lg-6">
                                <input id="textinput1" name="name" value="{{old('name')}}" type="text" placeholder="{{trans('translate.name')}}" class="form-control btn-square input-md">
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-left" for="textinput">{{trans('translate.email')}}</label>
                            <div class="col-lg-6">
                                <input id="textinput2" name="email" value="{{old('email')}}" type="email" placeholder="{{trans('translate.email')}}" class="form-control btn-square input-md">
                            </div>
                        </div>

                        <!-- Password input-->
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-left" for="passwordinput">{{trans('translate.password')}}</label>
                            <div class="col-lg-6">
                                <input id="passwordinput" name="password" value="{{old('password')}}" type="password" placeholder="{{trans('translate.password')}}" class="form-control btn-square input-md">
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-left" for="selectbasic">{{trans('translate.role')}}</label>
                            <div class="col-lg-6">
                                <select id="selectbasic" name="role" class="form-control btn-square">
                                    @foreach($allroles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>


                        <!-- File Button -->
                        <div class="form-group row">
                            <label class="col-lg-3 control-label text-lg-left" for="filebutton">{{trans('translate.image')}}</label>
                            <div class="col-lg-3">
                                <input id="image" name="image" class="input-file" type="file">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <button id="Submit" name="Submit" class="btn btn-primary">{{trans('translate.submit')}}</button>
                            </div>
                        </div>

                    </fieldset>
                </form>


            </div>
            <!-- Container-fluid Ends-->
        </div>
    </div>
@endsection
