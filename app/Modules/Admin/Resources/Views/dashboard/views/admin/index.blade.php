@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>{{trans('translate.admins table')}}</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('translate.admins')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('admins.create') }}" class="btn btn-primary">{{trans('translate.add new')}}</a>
                    @include('flash::message')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('translate.admins table')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('translate.name')}}</th>
                                        <th>{{trans('translate.email')}}</th>
                                        <th>{{trans('translate.image')}}</th>
                                        <th>{{trans('translate.role')}}</th>
                                        <th>{{trans('translate.created at')}}</th>
                                        <th>{{trans('translate.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($admins as $key=>$admin)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$admin->name}}</td>
                                            <td>{{$admin->email}}</td>
                                            <td><img src="{{$admin->image}}" style="width:70px; height:70px; "></td>

                                            <td>
                                                {{$admin->getRoleNames()[0]}}
                                            </td>
                                            <td>{{$admin->created_at}}</td>
                                            <td>
                                                <a href="{{ route('admins.edit',$admin->id) }}" class="btn btn-success btn-lg active"> <span class="fa fa-pencil"></span></a>

                                                <form id="delete-form-{{ $admin->id }}" action="{{ route('admins.destroy',$admin->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-success btn-lg active" onclick="if(confirm('{{trans('translate.are you sure ? yo want to delete this field ?')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $admin->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

