@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>Profile</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Profile</li>
                            <li class="breadcrumb-item active">Update Profile</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                <p>{{ $error }}</p>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

            </div>

        @endforeach
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>Update Profile Data</h5>
                    </div>
                    <form class="form theme-form" action="{{route('admins.profile.update',\Illuminate\Support\Facades\Auth::guard('admin')->user()->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">name</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" name="name" value="{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->name}}" id="name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">email</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="email" name="email" id="email" value="{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->email}}" placeholder="Type your email in Placeholder">
                                        </div>
                                    </div>

                                    <div class="avatar">
                                        <img src="{{Illuminate\Support\Facades\Auth::guard('admin')->user()->image}}"
                                             style="width:70px; height:70px;margin-left: 250px;margin-bottom: 20px ">
                                        <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload File</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="image" type="file">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Password</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="password" placeholder="Password input">
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col-sm-9 offset-sm-3">
                                <button class="btn btn-primary" type="submit">Submit</button>
                                <input class="btn btn-light" type="reset" value="Cancel">
                            </div>
                        </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
