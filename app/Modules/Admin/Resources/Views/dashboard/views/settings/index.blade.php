@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.SETTINGS')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.SETTINGS')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.Update Settings')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                <p>{{ $error }}</p>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

            </div>

        @endforeach
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.UPDATE SETTINGS DATA')}}</h5>
                    </div>
                    <form class="form theme-form" action="{{route('settings.update')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.app name')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" name="app_name" value="{{$settings->app_name}}" id="name">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.phone')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="tel" name="phone" id="phone" value="{{$settings->phone}}" placeholder="Type your email in Placeholder">
                                        </div>
                                    </div>

                                    <div class="avatar">
                                        <img src="{{$settings->logo}}"
                                             style="width:70px; height:70px;margin-left: 250px;margin-bottom: 20px ">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">{{trans('translate.Upload File')}}</label>
                                            <div class="col-sm-9">
                                                <input class="form-control" name="logo" type="file">
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="col-sm-9 offset-sm-3">
                                    <button class="btn btn-primary" type="submit">{{trans('translate.submit')}}</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
