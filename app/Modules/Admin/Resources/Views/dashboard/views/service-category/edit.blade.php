@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.Service Category')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.Service Category')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.update service category')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                <p>{{ $error }}</p>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

            </div>

        @endforeach
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>Basic form control</h5>
                    </div>
                    <form class="form theme-form" action="{{route('services.categories.update',$category->id)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('translate.service category name en')}}</label>
                                        <input class="form-control" name="en[name]" id="en[name]" type="text" value="{{$category->translate('en')->name}}" placeholder="{{trans('translate.service category name en')}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('translate.service category name ar')}}</label>
                                        <input class="form-control" id="ar[name]" name="ar[name]" type="text" value="{{$category->translate('ar')->name}}" placeholder="{{trans('translate.service category name ar')}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <img style="width: 150px;height: 150px;" src="{{$category->image}}">
                            </div>
                            <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-left" for="filebutton">{{trans('translate.image')}}</label>
                                <div class="col-lg-3">
                                    <input id="image" name="image" class="input-file" type="file">
                                </div>

                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">{{trans('translate.submit')}}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>



@endsection
