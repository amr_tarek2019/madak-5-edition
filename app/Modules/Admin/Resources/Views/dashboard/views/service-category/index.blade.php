@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.Service Category')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">{{trans('translate.Service Category')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                <a href="{{ route('services.categories.create') }}" class="btn btn-primary">{{trans('translate.add new')}}</a>
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.SERVICE CATEGORY TABLE')}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('translate.name')}}</th>
                                    <th>{{trans('translate.image')}}</th>
                                    <th>{{trans('translate.status')}}</th>
                                    <th>{{trans('translate.created at')}}</th>
                                    <th>{{trans('translate.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $key=> $category)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$category->name}}</td>
                                        <td><img src="{{$category->image}}" style="width:70px; height:70px; "></td>
                                        <td>
                                            <div class="media-body text-left icon-state">
                                                <label class="switch">
                                                    <input onchange="updateServiceCategoryStatus(this)" value="{{ $category->id }}" type="checkbox"
                                                    <?php if($category->status == 1) echo "checked";?>>
                                                    <span class="switch-state bg-primary"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>{{$category->created_at}}</td>
                                        <td>
                                            <a href="{{route('services.categories.edit',$category->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-edit"></span></a>
                                            <form id="delete-form-{{ $category->id }}" action="{{ route('services.categories.destroy',$category->id) }}" style="display: none;" method="POST">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-success btn-lg active"
                                                    onclick="if(confirm('{{trans('translate.are you sure ? yo want to delete this field ?')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $category->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"

                                            > <span class="fa fa-trash"></span></button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Zero Configuration  Ends-->



        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection
@section('myjsfile')
    <script>
        function updateServiceCategoryStatus(elUser){
            if(elUser.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('services.categories.status',isset($category) ? $category->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, status:status}, function(data){
                if(data == 1){
                    alert('{{trans('translate.status changed successfully')}}');
                }
                else{
                    alert('{{trans('translate.errorinchangestatus')}}');
                }
            });
        }
    </script>
@endsection
