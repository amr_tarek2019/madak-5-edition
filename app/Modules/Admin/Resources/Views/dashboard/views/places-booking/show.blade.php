@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.place booking details')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.places bookings')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.place booking details')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.USER DATA')}}</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('translate.name')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="text" readonly value="{{$reservation->user->name}}" placeholder="name@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('translate.email')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="email" readonly value="{{$reservation->user->email}}" placeholder="name@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.phone')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" type="tel"  readonly value="{{$reservation->user->phone}}"  placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.image')}}</label>
                                        <img src="{{$reservation->user->image}}" style="width:70px; height:70px; ">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.PLACE DATA')}}</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.name')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" value="{{$reservation->place->name}}" readonly type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.category')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" value="{{$reservation->place->placeCategory->name}}" readonly type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.present type')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" value="{{$reservation->place->present_type}}" readonly placeholder="Type your title in Placeholder">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.price')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="number" value="{{$reservation->place->price}}" readonly placeholder="Password input">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.quantity')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control digits" type="number" value="{{$reservation->place->quantity}}" readonly placeholder="Number">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.date from')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control m-input digits" type="text" value="{{$reservation->place->date_from}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.date to')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" type="text" value="{{$reservation->place->date_to}}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.address')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control digits" id="example-datetime-local-input" type="text" value="{{$reservation->place->address}}" readonly>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.description')}}</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" rows="5" cols="5" readonly placeholder="Default textarea">{{$reservation->place->desc}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.RESERVATION DATA')}}</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput5">{{trans('translate.date from')}}</label>
                                        <input class="form-control btn-pill" id="exampleFormControlInput5" type="text" value="{{$reservation->date_from}}" readonly placeholder="name@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword6">{{trans('translate.date to')}}</label>
                                        <input class="form-control btn-pill" id="exampleInputPassword6" type="text" value="{{$reservation->date_to}}" readonly placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword6">{{trans('translate.status')}}</label>
                                        <input class="form-control btn-pill" id="exampleInputPassword6" type="text" value="@if($reservation->status==0)accept without deposit @else accept with deposit @endif" readonly placeholder="Password">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                        <div class="card-footer">
                            <a class="btn btn-light" href="{{route('places.reservations.index')}}">{{trans('translate.back')}}</a>
                        </div>

                </div>

            </div>
        </div>

    <!-- Container-fluid Ends-->






@endsection
