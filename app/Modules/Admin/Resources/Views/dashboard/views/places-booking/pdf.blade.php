<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <link rel="stylesheet" href="{{asset('admin/invoice/style.css')}}" media="all" />
</head>
<body onload="window.print();">
<header class="clearfix">
    <div id="logo">
        <img src="{{$settings->logo}}" alt="ambeco">
    </div>
    <h1>{{$settings->app_name}}</h1>
    <div id="company" class="clearfix">
        <div>{{$settings->app_name}}</div>
        <div>  س.ت :  {{$settings->phone}} </div>


    </div>
    <div id="project">
        <div><span>اسم العميل</span> <span >{{$data->user->name}}</span></div>
        <div><span>التاريخ</span> {{$data->created_at}}</div>
        <div><span>رقم العميل</span> {{$data->user->id}}</div>
        <div><span>رقم الطلب</span> {{$data->id}}</div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th>رقم المكان</th>
            <th>اسم المكان</th>
            <th>وصف المكان</th>
            <th class="service">الخدمة المقدمة</th>
            <th class="service">الكمية</th>
            <th>السعر</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="service">{{$data->place->id}}</td>
            <td class="name">{{$data->place->name}}</td>
            <td class="desc">{{$data->place->desc}}</td>
            <td class="unit">{{$data->place->present_type}} </td>
            <td class="qty">{{$data->place->quantity}}</td>
            <td class="total">{{$data->place->price}}</td>
        </tr>


        <tr>
            <td class="grand total">{{$data->place->price * $data->place->quantity}} ر.س </td>
            <td colspan="7" class="grand total">الإجمالى </td>


        </tbody>
    </table>

    <div class="signature">
        <div id="project">
            <div><span>توقيع المسئول </span> ...................................................</div>
            <div><span>البائع </span> ...................................................</div>
            <div><span>التوقيع  </span>...................................................</div>

        </div>
        <div id="project2">
            <div><span> المستلم </span> ...................................................</div>
            <div><span> التوقيع</span> ...................................................</div>


        </div>
    </div>
</main>


</body>
</html>
