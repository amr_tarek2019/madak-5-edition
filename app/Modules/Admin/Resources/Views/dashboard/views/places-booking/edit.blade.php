@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.edit place booking data')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.places bookings')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.edit place booking data')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                <p>{{ $error }}</p>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

            </div>

        @endforeach
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Basic form control</h5>
                    </div>
                    <form class="form theme-form" method="POST" action="{{route('places.reservations.update',$reservation->id)}}">
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">{{trans('translate.date from')}}</label>
                                <div class="col-sm-9">
                                    <input class="form-control digits" type="date" name="date_from" id="date_from" value="{{$reservation->date_from}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">{{trans('translate.date to')}}</label>
                                <div class="col-sm-9">
                                    <input class="form-control digits" type="date" name="date_to" id="date_to"  value="{{$reservation->date_to}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.status')}}</label>
                                        <select class="form-control digits" id="status" name="status">
                                            <option value="0" {{ isset($reservation) && $reservation->status == 0 ? 'selected'  :'' }}>accept without deposit</option>
                                            <option value="1" {{ isset($reservation) && $reservation->status == 1 ? 'selected'  :'' }}>accept with deposit</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">{{trans('translate.submit')}}</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->






@endsection
