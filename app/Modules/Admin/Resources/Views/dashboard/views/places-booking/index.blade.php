@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.places bookings')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.places bookings')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.PLACES BOOKINGS DATATABLE')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.BOOKINGS DATA')}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('translate.name')}}</th>
                                    <th>{{trans('translate.place')}}</th>
                                    <th>{{trans('translate.date from')}}</th>
                                    <th>{{trans('translate.date to')}}</th>
                                    <th>{{trans('translate.status')}}</th>
                                    <th>{{trans('translate.created at')}}</th>
                                    <th>{{trans('translate.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($reservations as $key=> $reservation)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$reservation->user->name}}</td>
                                    <td>{{$reservation->place->name}}</td>
                                    <td>{{$reservation->date_from}}</td>
                                    <td>{{$reservation->date_to}}</td>
                                    <td>
                                        @if($reservation->status == true)
                                            <span class="label label-info">accept without deposit</span>
                                        @else
                                            <span class="label label-danger">accept with deposit</span>
                                        @endif
                                    </td>
                                    <td>{{$reservation->created_at}}</td>
                                    <td>
                                        <a href="{{route('places.reservations.show',$reservation->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-eye"></span></a>
                                        <a href="{{route('places.reservations.edit',$reservation->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-edit"></span></a>
                                        <a href="{{route('places.reservations.invoice',$reservation->id)}}" target="_blank" class="btn btn-success btn-lg active"> <span class="fa fa-print"></span></a>

                                        <form id="delete-form-{{ $reservation->id }}" action="{{ route('places.reservations.destroy',$reservation->id) }}" style="display: none;" method="POST">
                                            @csrf
                                        </form>
                                        <button type="button" class="btn btn-success btn-lg active"
                                                onclick="if(confirm('{{trans('translate.are you sure ? yo want to delete this field ?')}}')){
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $reservation->id }}').submit();
                                                    }else {
                                                    event.preventDefault();
                                                    }"

                                        > <span class="fa fa-trash"></span></button>
                                    </td>
                                </tr>
                                 @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Zero Configuration  Ends-->

        </div>
    </div>
    <!-- Container-fluid Ends-->






@endsection
