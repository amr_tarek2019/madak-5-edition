@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.services')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">{{trans('translate.services')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.service table')}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('translate.provider')}}</th>
                                    <th>{{trans('translate.category')}}</th>
                                    <th>{{trans('translate.subcategories')}}</th>
                                    <th>{{trans('translate.subsubcategories')}}</th>
                                    <th>{{trans('translate.city')}}</th>
                                    <th>{{trans('translate.gender')}}</th>
                                    <th>{{trans('translate.present type')}}</th>
                                    <th>{{trans('translate.pricebefore')}}</th>
                                    <th>{{trans('translate.priceafter')}}</th>
                                    <th>{{trans('translate.quantity')}}</th>
                                    <th>{{trans('translate.address')}}</th>
                                    <th>{{trans('translate.date')}}</th>
                                    <th>{{trans('translate.created at')}}</th>
                                    <th>{{trans('translate.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($services as $key=> $service)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$service->provider->user->name}}</td>
                                        <td>{{$service->serviceCategory->name}}</td>
                                        <td>{{$service->ServiceSubcategory->name}}</td>
                                        <td>{{$service->servicesSubSubCategory->name}}</td>
                                        <td>{{$service->city->name}}</td>
                                        <td>
                                            @if($service->gender == 0)
                                                <span class="label label-info">male</span>
                                            @elseif($service->gender == 1)
                                                <span class="label label-danger">female</span>
                                            @else
                                                <span class="label label-warning">both</span>
                                            @endif
                                        </td>
                                        <td>{{$service->present_type}}</td>
                                        <td>{{$service->price_before}}</td>
                                        <td>{{$service->price_after}}</td>
                                        <td>{{$service->quantity}}</td>
                                        <td>{{$service->address}}</td>
                                        <td>{{$service->date}}</td>
                                        <td>{{$service->created_at}}</td>
                                        <td>
                                            <a href="{{route('services.show',$service->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-eye"></span></a>
                                            <a href="{{route('services.edit',$service->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-edit"></span></a>
                                            <form id="delete-form-{{ $service->id }}" action="{{ route('services.destroy',$service->id) }}" style="display: none;" method="POST">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-success btn-lg active"
                                                    onclick="if(confirm('{{trans('translate.are you sure ? yo want to delete this field ?')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $service->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"

                                            > <span class="fa fa-trash"></span></button>
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Zero Configuration  Ends-->



        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection

