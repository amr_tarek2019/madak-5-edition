@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.edit service data')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.services')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.edit service data')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                <p>{{ $error }}</p>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

            </div>

        @endforeach
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Basic form control</h5>
                    </div>
                    <form class="form theme-form" method="POST" action="{{route('services.update',$service->id)}}">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.category')}}</label>
                                        <select class="form-control digits" name="service_category_id">
                                            @foreach($categories as $category)
                                                <option {{ $category->id == $service->serviceCategory->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.subcategories')}}</label>
                                        <select class="form-control digits" name="service_subcategory_id">
                                            @foreach($subcategories as $subcategory)
                                                <option {{ $subcategory->id == $service->ServiceSubcategory->id ? 'selected' : '' }} value="{{ $subcategory->id }}">{{ $subcategory->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.subsubcategories')}}</label>
                                        <select class="form-control digits" name="service_subsubcategory_id">
                                            @foreach($subsubcategories as $subsubcategory)
                                                <option {{ $subsubcategory->id == $service->servicesSubSubCategory->id ? 'selected' : '' }} value="{{ $subsubcategory->id }}">{{ $subsubcategory->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.city')}}</label>
                                        <select class="form-control digits" name="city_id">
                                            @foreach($cities as $city)
                                                <option {{ $city->id == $service->city->id ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.gender')}}</label>
                                        <select class="form-control digits" id="gender" name="gender">
                                            <option value="0" {{ isset($service) && $service->gender == 0 ? 'selected'  :'' }}>male</option>
                                            <option value="1" {{ isset($service) && $service->gender == 1 ? 'selected'  :'' }}>female</option>
                                            <option value="2" {{ isset($service) && $service->gender == 2 ? 'selected'  :'' }}>both</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.present type')}}</label>
                                        <input class="form-control" id="present_type" type="text" name="present_type" value="{{$service->present_type}}" serviceholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.pricebefore')}}</label>
                                        <input class="form-control" id="price_before" name="price_before" type="number" value="{{$service->price_before}}" serviceholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.priceafter')}}</label>
                                        <input class="form-control" id="price_after" name="price_after" type="number" value="{{$service->price_after}}" serviceholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.quantity')}}</label>
                                        <input class="form-control" id="quantity" name="quantity" type="number" value="{{$service->quantity}}" serviceholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">{{trans('translate.date')}}</label>
                                <div class="col-sm-9">
                                    <input class="form-control digits" type="date" name="date" id="date" value="{{$service->date}}">
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.address')}}</label>
                                        <input class="form-control" id="address" type="text" name="address" value="{{$service->address}}" serviceholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('translate.description')}}</label>
                                        <textarea class="form-control" id="desc" name="desc" rows="3">{{$service->desc}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">{{trans('translate.submit')}}</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->






@endsection
