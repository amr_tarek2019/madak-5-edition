@extends('admin::dashboard.layouts.app')
@section('content')

<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col">
                <div class="page-header-left">
                    <h3>{{trans('translate.USER DATA')}}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item">{{trans('translate.Users')}}</li>
                        <li class="breadcrumb-item active">{{trans('translate.USER DATA')}}</li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h5>Basic form control</h5>
                </div>
                <form class="form theme-form">
                    <div class="card-body">

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">{{trans('translate.name')}}</label>
                                    <input class="form-control" id="name" readonly type="text" name="name" value="{{$user->name}}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlInput1">{{trans('translate.email')}}</label>
                                    <input class="form-control" id="email" readonly type="email" name="email" value="{{$user->email}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleInputPassword2">{{trans('translate.phone')}}</label>
                                    <input class="form-control" id="phone" readonly type="tel" name="phone" value="{{$user->phone}}">
                                </div>
                            </div>
                        </div>

                        <div class="user-image">
                            <div class="avatar"><img alt="" src="{{$user->image}}" data-original-title="" width="100px" title=""></div>
                        </div>
                        <br>

{{--                        <div class="mapouter">--}}
{{--                            <div class="gmap_canvas">--}}
{{--                                <iframe width="600" height="500" id="gmap_canvas"--}}
{{--                                        src="https://maps.google.com/maps?q={{$user->lat}},{{$user->lng}}&ie=UTF8&iwloc=&output=embed"--}}
{{--                                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>--}}
{{--                                <a href="https://www.maps-generator.org"></a>">google maps de</a></div>--}}
{{--                            <style>.mapouter{position:relative;text-align:right;height:400px;width:100px;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style>--}}
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <iframe width="600" height="500" id="gmap_canvas"
                                            src="https://maps.google.com/maps?q={{$user->lat}},{{$user->lng}}&ie=UTF8&iwloc=&output=embed"
                                            frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-primary" href="{{route('users.index')}}">{{trans('translate.back')}}</a>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
