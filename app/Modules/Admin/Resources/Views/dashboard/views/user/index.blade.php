@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3{{trans('translate.USERS TABLE')}}></h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">{{trans('translate.Users')}}</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('flash::message')
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('translate.USERS TABLE')}}</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>{{trans('translate.name')}}</th>
                                        <th>{{trans('translate.email')}}</th>
                                        <th>{{trans('translate.phone')}}</th>
                                        <th>{{trans('translate.status')}}</th>
                                        <th>{{trans('translate.created at')}}</th>
                                        <th>{{trans('translate.actions')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key=>$user)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>{{$user->phone}}</td>
                                            <td>
                                                <div class="media-body text-right icon-state">
                                                <label class="switch">
                                                    <input onchange="updateUserStatus(this)" value="{{ $user->id }}" type="checkbox"
                                                    <?php if($user->user_status == 1) echo "checked";?>>
                                                    <span class="switch-state bg-primary"></span>
                                                </label>
                                                </div>
                                            </td>
                                            <td>{{$user->created_at}}</td>
                                            <td>
                                                <a href="{{ route('users.show',$user->id) }}" class="btn btn-success btn-lg active"> <span class="fa fa-eye"></span></a>

                                                <form id="delete-form-{{ $user->id }}" action="{{ route('users.destroy',$user->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-success btn-lg active" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $user->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><span class="fa fa-trash"></span></button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
@section('myjsfile')
    <script>
        function updateUserStatus(elUser){
            if(elUser.checked){
                var user_status = 1;
            }
            else{
                var user_status = 0;
            }
            $.post('{{ route('users.status',isset($user) ? $user->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, user_status:user_status}, function(data){
                if(data == 1){
                    alert('{{trans('translate.status changed successfully')}}');
                }
                else{
                    alert('{{trans('translate.error in change status')}}');
                }
            });
        }
    </script>
@endsection
