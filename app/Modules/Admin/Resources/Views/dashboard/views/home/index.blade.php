@extends('admin::dashboard.layouts.app')
@section('content')
<div class="container-fluid">
    <div class="page-header">
        <div class="row">
            <div class="col">
                <div class="page-header-left">
                    <h3>{{trans('translate.Dashboard')}}</h3>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('dashboard.index')}}"><i data-feather="home"></i></a></li>
                        <li class="breadcrumb-item active">{{trans('translate.Dashboard')}}</li>
                    </ol>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Container-fluid starts-->
<div class="container-fluid">
    <div class="row">
        <div class="col-xl-8 xl-100">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="chart-widget-dashboard">
                                <div class="media">
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-0 f-w-600"><i data-feather="users"></i><span class="counter"> {{ \App\Models\User::where('user_type','user')->get()->count() }}</span></h5>
                                        <p>{{trans('translate.Total Users')}}</p>
                                    </div><i data-feather="tag"></i>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="small-chart-gradient-1"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="chart-widget-dashboard">
                                <div class="media">
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-0 f-w-600"><i data-feather="users"></i><span class="counter"> {{ \App\Models\User::where('user_type','provider')->get()->count() }}</span></h5>
                                        <p>{{trans('translate.Total Providers')}}</p>
                                    </div><i data-feather="shopping-cart"></i>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="small-chart-gradient-2"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="chart-widget-dashboard">
                                <div class="media">
                                    <div class="media-body">
                                        <h5 class="mt-0 mb-0 f-w-600"><i data-feather="users"></i><span class="counter">{{\Illuminate\Support\Facades\Auth::guard('admin')->user()->count()}}</span></h5>
                                        <p>{{trans('translate.Total Admins')}}</p>
                                    </div><i data-feather="sun"></i>
                                </div>
                                <div class="dashboard-chart-container">
                                    <div class="small-chart-gradient-3"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('translate.TOTAL PLACES ORDERS')}}</h5>
                        </div>
                        <div class="card-body charts-box">
                            <div class="flot-chart-container">
                                <div class="flot-chart-placeholder" id="placesBooking"></div>
                            </div>
                            <div class="code-box-copy">
                                <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>{{trans('translate.TOTAL PREPARATIONS ORDERS')}}</h5>
                        </div>
                        <div class="card-body charts-box">
                            <div class="flot-chart-container">
                                <div class="flot-chart-placeholder" id="preparationsBooking"></div>
                            </div>
                            <div class="code-box-copy">
                                <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                                <pre><code class="language-html" id="example-head">&lt;!-- Cod Box Copy begin --&gt;
  &lt;div class="card-body charts-box"&gt;
    &lt;div class="flot-chart-container"&gt;
      &lt;div id="graph123" class="flot-chart-placeholder"&gt;&lt;/div&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 xl-100">
            <div class="card">
                <div class="card-header">
                    <h5>{{trans('translate.LAST 4 CONTACT US MESSAGES')}}</h5>

                </div>
                <div class="card-body activity-scroll">
                    <div class="activity">
                        @foreach (\App\Models\Contact::orderBy('id', 'desc')->take(4)->get() as $key => $msg)
                        <div class="media">
                            <div class="gradient-round m-r-30 gradient-line-1"><i data-feather="mail"></i></div>
                            <div class="media-body">
                                <h6>{{$msg->name}} <span class="pull-right f-14">{{$msg->created_at}}</span></h6>
                                <p>{{$msg->message}}</p>
                            </div>
                        </div>
                            @endforeach

                    </div>
                    <div class="code-box-copy">
                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head1" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                        <pre><code class="language-html" id="example-head1">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class="activity"&gt;
  &lt;div class="media"&gt;
    &lt;div class="gradient-round m-r-30 gradient-line-1"&gt;
      &lt;i data-feather="shopping-bag"&gt;&lt;/i&gt;
    &lt;/div&gt;
    &lt;div class="media-body"&gt;
      &lt;h6&gt;New Sale &lt;span class="pull-right f-14"&gt;New&lt;/span&gt;&lt;/h6&gt;
      &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="media"&gt;
    &lt;div class="gradient-round m-r-30 gradient-line-1"&gt;
      &lt;i data-feather="message-circle"&gt;&lt;/i&gt;
    &lt;/div&gt;
    &lt;div class="media-body"&gt;
      &lt;h6&gt;New Message &lt;span class="pull-right f-14"&gt;14m Ago&lt;/span&gt;&lt;/h6&gt;
      &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="media"&gt;
    &lt;div class="gradient-round m-r-30 small-line"&gt;
      &lt;i data-feather="minus-circle"&gt;&lt;/i&gt;
    &lt;/div&gt;
    &lt;div class="media-body"&gt;
      &lt;h6&gt;New Report &lt;span class="pull-right f-14"&gt;14m Ago&lt;/span&gt;&lt;/h6&gt;
      &lt;p&gt;Lorem Ipsum is simply dummy text.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="media"&gt;
    &lt;div class="gradient-round m-r-30 gradient-line-1"&gt;
      &lt;i data-feather="shopping-bag"&gt;&lt;/i&gt;
    &lt;/div&gt;
    &lt;div class="media-body"&gt;
      &lt;h6&gt;New Sale &lt;span class="pull-right f-14"&gt;14m Ago&lt;/span&gt;&lt;/h6&gt;
      &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/div&gt;
  &lt;div class="media"&gt;
    &lt;div class="gradient-round m-r-30 medium-line"&gt;
      &lt;i data-feather="tag"&gt;&lt;/i&gt;
    &lt;/div&gt;
    &lt;div class="media-body"&gt;
      &lt;h6&gt;New Visits &lt;span class="pull-right f-14"&gt;14m Ago&lt;/span&gt;&lt;/h6&gt;
      &lt;p&gt;Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry.&lt;/p&gt;
    &lt;/div&gt;
  &lt;/div&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6">
            <div class="card height-equal">
                <div class="card-header card-header-border">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>{{trans('translate.LAST 4 USERS')}}</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="new-users">
                        @foreach (\App\Models\User::where('user_type','user')->orderBy('id', 'desc')->take(5)->get() as $key => $user)
                        <div class="media">
                            <img class="rounded-circle image-radius m-r-15" src="{{$user->image}}" alt="">
                            <div class="media-body">
                                <h6 class="mb-0 f-w-700">{{$user->name}}</h6>
                                <p>{{$user->email}}</p>
                            </div><span class="pull-right">
                          <a class="btn btn-pill btn-outline-light" href="{{route('users.show',$user->id)}}">show</a></span>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>


        <div class="col-xl-6">
            <div class="card height-equal">
                <div class="card-header card-header-border">
                    <div class="row">
                        <div class="col-sm-6">
                            <h5>{{trans('translate.LAST 4 PROVIDERS')}}</h5>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="new-users">
                        @foreach (\App\Models\User::where('user_type','provider')->orderBy('id', 'desc')->take(5)->get() as $key => $user)
                            <div class="media">
                                <img class="rounded-circle image-radius m-r-15" src="{{$user->image}}" alt="">
                                <div class="media-body">
                                    <h6 class="mb-0 f-w-700">{{$user->name}}</h6>
                                    <p>{{$user->email}}</p>
                                </div><span class="pull-right">
                          <a class="btn btn-pill btn-outline-light" href="{{route('users.show',$user->id)}}">show</a></span>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-5 xl-100">
            <div class="card">
                <div class="card-header">
                    <h5>{{trans('translate.LAST 4 PLACES')}}</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive sellers">
                        <table class="table table-bordernone">
                            <thead>
                            <tr>
                                <th scope="col">{{trans('translate.name')}}</th>
                                <th scope="col">{{trans('translate.present type')}}</th>
                                <th scope="col">{{trans('translate.price')}}</th>
                                <th scope="col">{{trans('translate.quantity')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach (\App\Models\Place::orderBy('id', 'desc')->take(4)->get() as $key => $place)

                                <tr>
                                <td>
                                    <div class="d-inline-block align-middle">
                                        <img class="img-radius img-30 align-top m-r-15 rounded-circle" src="{{$place->main_image}}" alt="">
                                        <div class="d-inline-block">
                                            <p>{{$place->name}}</p>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <p>{{$place->present_type}}</p>
                                </td>
                                <td>
                                    <p>{{$place->price}}</p>
                                </td>
                                <td>
                                    <p>{{$place->quantity}}</p>
                                </td>
                            </tr>
@endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="code-box-copy">
                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head1" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                        <pre><code class="language-html" id="example-head1">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class="table-responsive sellers"&gt;
  &lt;table class="table table-bordernone"&gt;
    &lt;thead&gt;
      &lt;tr&gt;
        &lt;th scope="col"&gt;Name&lt;/th&gt;
        &lt;th scope="col"&gt;Sale&lt;/th&gt;
        &lt;th scope="col"&gt;Stock&lt;/th&gt;
        &lt;th scope="col"&gt;Categories&lt;/th&gt;
      &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/6.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/2.png" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/3.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/4.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/5.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/15.png" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
    &lt;/tbody&gt;
  &lt;/table&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-xl-5 xl-100">
            <div class="card">
                <div class="card-header">
                    <h5>{{trans('translate.LAST 4 PREPARATIONS')}}</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive sellers">
                        <table class="table table-bordernone">
                            <thead>
                            <tr>
                                <th scope="col">{{trans('translate.image')}}</th>
                                <th scope="col">{{trans('translate.present type')}}</th>
                                <th scope="col">{{trans('translate.price')}}</th>
                                <th scope="col">{{trans('translate.quantity')}}</th>
                                <th scope="col">{{trans('translate.address')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach (\App\Models\Service::orderBy('id', 'desc')->take(4)->get() as $key => $prep)

                                <tr>
                                    <td>
                                        <div class="d-inline-block align-middle">
                                            <img class="img-radius img-30 align-top m-r-15 rounded-circle" src="{{$prep->image}}" alt="">
                                        </div>
                                    </td>
                                    <td>
                                        <p>{{$prep->present_type}}</p>
                                    </td>
                                    <td>
                                        <p>{{$prep->price}}</p>
                                    </td>
                                    <td>
                                        <p>{{$prep->quantity}}</p>
                                    </td>
                                    <td>
                                        <p>{{$prep->address}}</p>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="code-box-copy">
                        <button class="code-box-copy__btn btn-clipboard" data-clipboard-target="#example-head1" title="Copy"><i class="icofont icofont-copy-alt"></i></button>
                        <pre><code class="language-html" id="example-head1">&lt;!-- Cod Box Copy begin --&gt;
&lt;div class="table-responsive sellers"&gt;
  &lt;table class="table table-bordernone"&gt;
    &lt;thead&gt;
      &lt;tr&gt;
        &lt;th scope="col"&gt;Name&lt;/th&gt;
        &lt;th scope="col"&gt;Sale&lt;/th&gt;
        &lt;th scope="col"&gt;Stock&lt;/th&gt;
        &lt;th scope="col"&gt;Categories&lt;/th&gt;
      &lt;/tr&gt;
    &lt;/thead&gt;
    &lt;tbody&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/6.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/2.png" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/3.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/4.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/5.jpg" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
      &lt;tr&gt;
        &lt;td&gt;
          &lt;div class="d-inline-block align-middle"&gt;
            &lt;img src="../assets/images/user/15.png" class="img-radius img-30 align-top m-r-15 rounded-circle" alt=""&gt;
            &lt;div class="d-inline-block"&gt;
              &lt;p&gt;Alana Slacker&lt;/p&gt;
            &lt;/div&gt;
          &lt;/div&gt;
        &lt;/td&gt;
        &lt;td&gt;&lt;p&gt;8956&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;54&lt;/p&gt;&lt;/td&gt;
        &lt;td&gt;&lt;p&gt;Product No: 1&lt;/p&gt;&lt;/td&gt;
      &lt;/tr&gt;
    &lt;/tbody&gt;
  &lt;/table&gt;
&lt;/div&gt;
&lt;!-- Cod Box Copy end --&gt;</code></pre>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Container-fluid Ends-->
@endsection
@section('myjsfile')
    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'placesBooking',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbersOfPlacesOrders as $order)

                { day: '{{$order->date}}', value: '{{$order->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>


    <script>
        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'preparationsBooking',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                    @foreach($numbersOfPreparationsOrders as $order)

                { day: '{{$order->date}}', value: '{{$order->count}}' },
                @endforeach
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'day',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });
    </script>

@stop
