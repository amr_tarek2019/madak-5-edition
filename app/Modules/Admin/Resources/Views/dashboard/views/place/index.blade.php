@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.place')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">{{trans('translate.place')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.place Table')}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('translate.provider')}}</th>
                                    <th>{{trans('translate.category')}}</th>
                                    <th>{{trans('translate.city')}}</th>
                                    <th>{{trans('translate.gender')}}</th>
                                    <th>{{trans('translate.name')}}</th>
                                    <th>{{trans('translate.present type')}}</th>
                                    <th>{{trans('translate.price')}}</th>
                                    <th>{{trans('translate.quantity')}}</th>
                                    <th>{{trans('translate.date from')}}</th>
                                    <th>{{trans('translate.date to')}}</th>
                                    <th>{{trans('translate.address')}}</th>
                                    <th>{{trans('translate.created at')}}</th>
                                    <th>{{trans('translate.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($places as $key=> $place)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$place->provider->user->name}}</td>
                                        <td>{{$place->placeCategory->name}}</td>
                                        <td>{{$place->city->name}}</td>
                                        <td>
                                            @if($place->gender == 0)
                                                <span class="label label-info">male</span>
                                            @elseif($place->gender == 1)
                                                <span class="label label-danger">female</span>
                                            @else
                                                <span class="label label-warning">both</span>
                                            @endif
                                        </td>
                                        <td>{{$place->name}}</td>
                                        <td>{{$place->present_type}}</td>
                                        <td>{{$place->price}}</td>
                                        <td>{{$place->quantity}}</td>
                                        <td>{{$place->date_from}}</td>
                                        <td>{{$place->date_to}}</td>
                                        <td>{{$place->address}}</td>
                                        <td>{{$place->created_at}}</td>
                                        <td>
                                            <a href="{{route('places.show',$place->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-eye"></span></a>
                                            <a href="{{route('places.edit',$place->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-edit"></span></a>
                                            <form id="delete-form-{{ $place->id }}" action="{{ route('places.destroy',$place->id) }}" style="display: none;" method="POST">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-success btn-lg active"
                                                    onclick="if(confirm('{{trans('translate.are you sure ? yo want to delete this field ?')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $place->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"

                                            > <span class="fa fa-trash"></span></button>
                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Zero Configuration  Ends-->



        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection

