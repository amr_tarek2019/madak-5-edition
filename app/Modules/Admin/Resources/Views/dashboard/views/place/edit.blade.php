@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.edit place data')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.place')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.edit place data')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger inverse alert-dismissible fade show" role="alert"><i class="icon-thumb-down"></i>

                <p>{{ $error }}</p>
                <button class="close" type="button" data-dismiss="alert" aria-label="Close" data-original-title="" title=""><span aria-hidden="true">×</span></button>

            </div>

        @endforeach
    @endif
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Basic form control</h5>
                    </div>
                    <form class="form theme-form" method="POST" action="{{route('places.update',$place->id)}}">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.category')}}</label>
                                        <select class="form-control digits" name="place_category_id">
                                            @foreach($categories as $category)
                                                <option {{ $category->id == $place->placeCategory->id ? 'selected' : '' }} value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.city')}}</label>
                                        <select class="form-control digits" name="city_id">
                                            @foreach($cities as $city)
                                                <option {{ $city->id == $place->city->id ? 'selected' : '' }} value="{{ $city->id }}">{{ $city->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect9">{{trans('translate.gender')}}</label>
                                        <select class="form-control digits" id="gender" name="gender">
                                            <option value="0" {{ isset($place) && $place->gender == 0 ? 'selected'  :'' }}>male</option>
                                            <option value="1" {{ isset($place) && $place->gender == 1 ? 'selected'  :'' }}>female</option>
                                            <option value="2" {{ isset($place) && $place->gender == 2 ? 'selected'  :'' }}>both</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.name')}}</label>
                                        <input class="form-control" id="name" type="text" name="name" value="{{$place->name}}" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.present type')}}</label>
                                        <input class="form-control" id="present_type" type="text" name="present_type" value="{{$place->present_type}}" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.price')}}</label>
                                        <input class="form-control" id="price" name="price" type="number" value="{{$place->price}}" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.quantity')}}</label>
                                        <input class="form-control" id="quantity" name="quantity" type="number" value="{{$place->quantity}}" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">{{trans('translate.date from')}}</label>
                                <div class="col-sm-9">
                                    <input class="form-control digits" type="date" name="date_from" id="date_from" value="{{$place->date_from}}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">{{trans('translate.date to')}}</label>
                                <div class="col-sm-9">
                                    <input class="form-control digits" type="date" name="date_to" id="date_to"  value="{{$place->date_to}}">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.address')}}</label>
                                        <input class="form-control" id="address" type="text" name="address" value="{{$place->address}}" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('translate.description')}}</label>
                                        <textarea class="form-control" id="desc" name="desc" rows="3">{{$place->desc}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-primary" type="submit">{{trans('translate.submit')}}</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid Ends-->






@endsection
