@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.place data')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.place')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.place data')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Basic form info</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('translate.category')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="text" value="{{$place->placeCategory->name}}" readonly placeholder="name@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.city')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->city->name}}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.gender')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="@if($place->gender==0)male@elseif($place->gender==1)female @else both @endif" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.name')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->name}}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.present type')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->present_type}}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.price')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->price}}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.quantity')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->quantity}}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.date from')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->date_from }}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.date to')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->date_to }}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.address')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" value="{{$place->address }}" readonly type="text" placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group mb-0">
                                        <label for="exampleFormControlTextarea4">{{trans('translate.description')}}</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea4" rows="5">{{$place->desc}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.providers data')}}</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.name')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" value="{{$place->provider->user->name}}" readonly type="text">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.email')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" value="{{$place->provider->user->email}}" readonly type="text">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.phone')}}</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" value="{{$place->provider->user->phone}}" readonly type="text">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">{{trans('translate.image')}}</label>
                                        <div class="col-sm-9">

                                            <img src="{{$place->provider->user->image}}" style="width:70px; height:70px; ">
                                         </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <iframe width="600" height="500" id="gmap_canvas"
                                    src="https://maps.google.com/maps?q={{$place->lat}},{{$place->lng}}&ie=UTF8&iwloc=&output=embed"
                                    frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>                                </div>
                    </div>
                </div>


                <div class="card-footer">
                    <a class="btn btn-light" href="{{route('places.index')}}">{{trans('translate.back')}}</a>

                </div>
            </div>


        </div>
    </div>
    <!-- Container-fluid Ends-->






@endsection
