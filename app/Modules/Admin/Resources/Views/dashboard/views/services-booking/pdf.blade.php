<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Example 1</title>
    <link rel="stylesheet" href="{{asset('admin/invoice/style.css')}}" media="all" />
</head>
<body onload="window.print();">
<header class="clearfix">
    <div id="logo">
        <img src="{{$settings->logo}}" alt="ambeco">
    </div>
    <h1>{{$settings->app_name}}</h1>
    <div id="company" class="clearfix">
        <div>{{$settings->app_name}}</div>
        <div>  س.ت :  {{$settings->phone}} </div>


    </div>
    <div id="project">
        <div><span>اسم العميل</span> <span >{{$data->user->name}}</span></div>
        <div><span>التاريخ</span> {{$data->created_at}}</div>
        <div><span>رقم العميل</span> {{$data->user->id}}</div>
        <div><span>رقم الطلب</span> {{$data->id}}</div>
    </div>
</header>
<main>
    <table>
        <thead>
        <tr>
            <th class="service">رقم الصنف </th>
            <th class="service">البيان</th>
            <th class="service">الرسالة</th>
            <th class="service">الكمية</th>
        </tr>
        </thead>
        <tbody>
        @foreach($servicesReservation as $serviceReservation)
        <tr>
            <td class="service">{{$serviceReservation->subSubCategory->id}}</td>
            <td class="desc">{{$serviceReservation->subSubCategory->name}}</td>
            <td class="unit">{{$data->message}} </td>
            <td class="qty">{{$serviceReservation->quantity}}</td>
        </tr>
        @endforeach



        <tr>
            <td class="grand total">{{$data->total}}</td>
            <td colspan="7" class="grand total">الإجمالى </td>



        </tr>
        </tbody>
    </table>

    <div class="signature">
        <div id="project">
            <div><span>توقيع المسئول </span> ...................................................</div>
            <div><span>البائع </span> ...................................................</div>
            <div><span>التوقيع  </span>...................................................</div>

        </div>
        <div id="project2">
            <div><span> المستلم </span> ...................................................</div>
            <div><span> التوقيع</span> ...................................................</div>


        </div>
    </div>
</main>


</body>
</html>
