@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.SERVICES BOOKING DETAILS')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">{{trans('translate.services bookings')}}</li>
                            <li class="breadcrumb-item active">{{trans('translate.SERVICES BOOKING DETAILS')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.USER DATA')}}</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('translate.name')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="text" readonly value="{{$reservation->user->name}}" placeholder="name@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">{{trans('translate.email')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="email" readonly value="{{$reservation->user->email}}" placeholder="name@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.phone')}}</label>
                                        <input class="form-control" id="exampleInputPassword2" type="tel"  readonly value="{{$reservation->user->phone}}"  placeholder="Password">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword2">{{trans('translate.image')}}</label>
                                        <img src="{{$reservation->user->image}}" style="width:70px; height:70px; ">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
              <div class="card">
                  <div class="card-header">
                    <h5>{{trans('translate.SERVICES DATA')}}</h5>
                  </div>
                  <div class="card-body">
                    <div class="table-responsive">
                      <table class="display" id="basic-1">
                        <thead>
                          <tr>
                            <th>#</th>
                            <th>{{trans('translate.service')}}</th>
                            <th>{{trans('translate.quantity')}}</th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach($servicesReservation as $key=>$serviceReservation)
                          <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$serviceReservation->subSubCategory->name}}</td>
                            <td>{{$serviceReservation->quantity}}</td>
                          </tr>
                        @endforeach

                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.RESERVATION DATA')}}</h5>
                    </div>
                    <form class="form theme-form">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput5">{{trans('translate.date')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="text" value="{{$reservation->date}}" readonly placeholder="name@example.com">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword6">{{trans('translate.total')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="text" value="{{$reservation->total}}" readonly placeholder="Password">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword6">{{trans('translate.message')}}</label>
                                        <textarea class="form-control" id="exampleFormControlInput1" type="text" name="message" readonly placeholder="Password">
                                        {{$reservation->message}}
                                        </textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="exampleInputPassword6">{{trans('translate.status')}}</label>
                                        <input class="form-control" id="exampleFormControlInput1" type="text" value="@if($reservation->status==0)accept without deposit @else accept with deposit @endif" readonly placeholder="Password">
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>

                        <div class="card-footer">
                            <a class="btn btn-light" href="{{route('places.reservations.index')}}">{{trans('translate.back')}}</a>
                        </div>

                </div>

            </div>
        </div>

    <!-- Container-fluid Ends-->






@endsection
