@extends('admin::dashboard.layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <div class="page-header-left">
                        <h3>{{trans('translate.Service Subcategory')}}</h3>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item active">{{trans('translate.Service Subcategory')}}</li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Container-fluid starts-->
    <div class="container-fluid">
        <div class="row">
            <!-- Zero Configuration  Starts-->
            <div class="col-sm-12">
                <a href="{{ route('services.subcategories.create') }}" class="btn btn-primary">{{trans('translate.add new')}}</a>
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h5>{{trans('translate.SERVICE SUBSUBCATEGORY DATA')}}</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('translate.name')}}</th>
                                    <th>{{trans('translate.category')}}</th>
                                    <th>{{trans('translate.image')}}</th>
                                    <th>{{trans('translate.status')}}</th>
                                    <th>{{trans('translate.created at')}}</th>
                                    <th>{{trans('translate.actions')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($subcategories as $key=> $subcategory)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$subcategory->name}}</td>
                                        <td>{{$subcategory->serviceCategory->name}}</td>
                                        <td><img src="{{$subcategory->image}}" style="width:70px; height:70px; "></td>
                                        <td>
                                            <div class="media-body text-left icon-state">
                                                <label class="switch">
                                                    <input onchange="updateServiceSubCategoryStatus(this)" value="{{ $subcategory->id }}" type="checkbox"
                                                    <?php if($subcategory->status == 1) echo "checked";?>>
                                                    <span class="switch-state bg-primary"></span>
                                                </label>
                                            </div>
                                        </td>
                                        <td>{{$subcategory->created_at}}</td>
                                        <td>
                                            <a href="{{route('services.subcategories.edit',$subcategory->id)}}" class="btn btn-success btn-lg active"> <span class="fa fa-edit"></span></a>
                                            <form id="delete-form-{{ $subcategory->id }}" action="{{ route('services.subcategories.destroy',$subcategory->id) }}" style="display: none;" method="POST">
                                                @csrf
                                            </form>
                                            <button type="button" class="btn btn-success btn-lg active"
                                                    onclick="if(confirm('{{trans('translate.are you sure ? yo want to delete this field ?')}}')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $subcategory->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"

                                            > <span class="fa fa-trash"></span></button>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Zero Configuration  Ends-->



        </div>
    </div>
    <!-- Container-fluid Ends-->

@endsection
@section('myjsfile')
    <script>
        function updateServiceSubCategoryStatus(elUser){
            if(elUser.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('services.subcategories.status',isset($subcategory) ? $subcategory->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, status:status}, function(data){
                if(data == 1){
                    alert('{{trans('translate.status changed successfully')}}');
                }
                else{
                    alert('{{trans('translate.errorinchangestatus')}}');
                }
            });
        }
    </script>
@endsection
