<?php

namespace App\Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_category_id'=>'required',
            'service_subcategory_id'=>'required',
            'service_subsubcategory_id'=>'required',
            'city_id'=>'required',
            'gender'=>'required',
            'present_type'=>'required',
            'price_before'=>'required',
            'price_after'=>'required',
            'quantity'=>'required',
            'date'=>'required',
            'address'=>'required',
            'desc'=>'required',
        ];
    }
}
