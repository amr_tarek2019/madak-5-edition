<?php

namespace App\Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PlaceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'place_category_id'=>'required',
            'city_id'=>'required',
            'gender'=>'required',
            'name'=>'required',
            'present_type'=>'required',
            'price'=>'required',
            'quantity'=>'required',
            'date_from'=>'required',
            'date_to'=>'required',
//            'lat'=>'required',
//            'lng'=>'required',
            'address'=>'required',
            'desc'=>'required',
//            'main_image'=>'required',
//            'images'=>'required',
        ];
    }
}
