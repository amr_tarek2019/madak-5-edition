<?php

namespace App\Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceSubcategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'en.name' =>'required',
            'ar.name'=>'required',
            'image'=>'required',
            'service_category_id'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'ar.name.required' => 'لم يتم ادخال الاسم ',
            'en.name.required'  => 'لم يتم ادخال البلد باللغة الاسم',
            'service_category_id.required'=>'لم يتم اختيار القسم',
        ];
    }
}
