<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\ServiceCategory;
use App\Models\ServiceSubcategory;
use App\Modules\Admin\Http\Requests\ServiceCategoryRequest;
use App\Modules\Admin\Http\Requests\ServiceSubcategoryRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ServiceSubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategories = ServiceSubcategory::get();
        return view('admin::dashboard.views.service-subcategory.index',compact('subcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=ServiceCategory::all();
        return view('admin::dashboard.views.service-subcategory.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceSubcategoryRequest $request)
    {
        $data = $request->validated();
        ServiceSubcategory::create($data);
        flash(__('Your service subcategory has been added successfully!'))->success();
        return redirect()->route('services.subcategories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories= ServiceCategory::all();
        $subcategory = ServiceSubcategory::find($id);
        return view('admin::dashboard.views.service-subcategory.edit',compact('categories','subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceSubcategoryRequest $request, $id)
    {
        $subcategory= ServiceSubcategory::find($id);
        $data = $request->validated();
        $subcategory->update($data);
        flash(__('Your service subcategory has been updated successfully!'))->success();
        return redirect()->route('services.subcategories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subcategory = ServiceSubcategory::find($id);
        $subcategory->delete();
        flash(__('Your service subcategory has been deleted successfully!'))->success();
        return back();
    }

    public function updateStatus(Request $request)
    {
        $subcategory = ServiceSubcategory::findOrFail($request->id);
        $subcategory->status = $request->status;
        if($subcategory->save()){
            return 1;
        }
        return 0;
    }
}
