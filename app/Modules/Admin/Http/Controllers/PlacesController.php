<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\City;
use App\Models\Place;
use App\Models\PlaceCategory;
use App\Modules\Admin\Http\Requests\PlaceRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class PlacesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $places = Place::all();
        return view('admin::dashboard.views.place.index',compact('places'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $place = Place::find($id);
        return view('admin::dashboard.views.place.show',compact('place'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=PlaceCategory::all();
        $cities=City::all();
        $place = Place::find($id);
        return view('admin::dashboard.views.place.edit',compact('place','categories','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlaceRequest $request, $id)
    {
        $place = Place::find($id);
        $data = $request->validated();
        $place->update($data);
        flash(__('Your place has been updated successfully!'))->success();
        return redirect()->route('places.reservations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Place::find($id)->delete();
        flash(__('Your place has been deleted successfully!'))->success();
        return back();
    }
}
