<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\ServiceProviderRequest;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ServiceProviderRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests=ServiceProviderRequest::all();
        return view('admin::dashboard.views.service-provider-requests.index',compact('requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $requestProvider = ServiceProviderRequest::find($id);
        return view('admin::dashboard.views.service-provider-requests.edit',compact('requestProvider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requestProvider = ServiceProviderRequest::find($id);
        $requestProvider->status=$request->status;



        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'phone'=>'required',
        ]);
        $user=User::where('email',$request->email)->orWhere('phone',$request->phone)->exists();
        if ($user){
            flash(__('this user is created before!'))->success();
            return back();
        }
        $createuser=  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone'=>$request->phone,
            'user_type'=>'provider',
            'user_status'=>'1',
            'status'=>'1',
            'jwt_token'=>'0',
            'lat'=>'0',
            'lng'=>'0',
            'firebase_token'=>'0'
        ]);

        $requestProvider->save();
        if ($createuser->save()){
        flash(__('Your service provider request has been updated successfully!'))->success();
        return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ServiceProviderRequest::find($id)->delete();
        flash(__('Your service provider request has been deleted successfully!'))->success();
        return back();
    }
}
