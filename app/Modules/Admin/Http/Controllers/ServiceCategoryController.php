<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\ServiceCategory;
use App\Modules\Admin\Http\Requests\ServiceCategoryRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ServiceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = ServiceCategory::all();

        return view('admin::dashboard.views.service-category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::dashboard.views.service-category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceCategoryRequest $request)
    {
        $data = $request->validated();
        ServiceCategory::create($data);
        flash(__('Your service category has been added successfully!'))->success();
        return redirect()->route('services.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ServiceCategory::find($id);
        return view('admin::dashboard.views.service-category.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceCategoryRequest $request, $id)
    {
        $category= ServiceCategory::find($id);
        $data = $request->validated();
        $category->update($data);
        flash(__('Your service category has been updated successfully!'))->success();
        return redirect()->route('services.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = ServiceCategory::find($id);
        $category->delete();
        flash(__('Your service category has been deleted successfully!'))->success();
        return back();
    }

    public function updateStatus(Request $request)
    {
        $category = ServiceCategory::findOrFail($request->id);
        $category->status = $request->status;
        if($category->save()){
            return 1;
        }
        return 0;
    }
}
