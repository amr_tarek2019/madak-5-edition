<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\Admin;
use App\Modules\Admin\Http\Requests\AdminRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login_form(){
        return view('admin::login.login');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){

        $credentials = array(
            'email' => $request->email,
            'password' =>$request->password
        );
        if (Auth::guard('admin')->attempt($credentials)) {
            return redirect()->route('dashboard.index');
        }
        else{
            return redirect()->route('login')->with('error','البريد الاكتروني أو كلمة السر غير صحيحة');

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(){
        Auth::logout();
        return redirect()->route('login');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        return view('admin::dashboard.views.profile.index')->with('admin',Auth::guard('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateProfile(Request $request,$id)
    {
        $user=Admin::where('id',$id)->first();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->image=$request->image;
        if($user->password!=$request->password)
        {
            $user->password=$request->password;
        }
        if($user->save()){
            flash(__('Your Profile has been updated successfully!'))->success();
            return back();
        }
        flash(__('Sorry! Something went wrong.'))->error();
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function backup()
    {
        //ENTER THE RELEVANT INFO BELOW
        $mysqlHostName      = env('DB_HOST');
        $mysqlUserName      = env('DB_USERNAME');
        $mysqlPassword      = env('DB_PASSWORD');
        $DbName             = env('DB_DATABASE');
        $backup_name        = "backup.sql";
        $tables             = array(
            "admins","chats","chat_images","cities","city_translations","contacts","departments",
            "departments_translations", "issues","issues_translations","migrations",
            "model_has_permissions","model_has_roles","notifications","notification_translations","password_resets",
            "permissions","places","places_booking","places_categories","places_categories_translations","places_images"
        ,"places_orders","places_preparations","preparations_order","preparation_booking","providers","role_has_permissions",
        "ratings","roles","services","services_categories","services_cat_translations","services_orders","services_subcategories",
         "service_provider_request","service_subcat_translation","sub_subcategories","sub_subcategories_trans",
            "users","verifications"
        ); //here your tables...

        $connect = new \PDO("mysql:host=$mysqlHostName;dbname=$DbName;charset=utf8", "$mysqlUserName", "$mysqlPassword",array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
        $get_all_table_query = "SHOW TABLES";
        $statement = $connect->prepare($get_all_table_query);
        $statement->execute();
        $result = $statement->fetchAll();


        $output = '';
        foreach($tables as $table)
        {
            $show_table_query = "SHOW CREATE TABLE " . $table . "";
            $statement = $connect->prepare($show_table_query);
            $statement->execute();
            $show_table_result = $statement->fetchAll();

            foreach($show_table_result as $show_table_row)
            {
                $output .= "\n\n" . $show_table_row["Create Table"] . ";\n\n";
            }
            $select_query = "SELECT * FROM " . $table . "";
            $statement = $connect->prepare($select_query);
            $statement->execute();
            $total_row = $statement->rowCount();

            for($count=0; $count<$total_row; $count++)
            {
                $single_result = $statement->fetch(\PDO::FETCH_ASSOC);
                $table_column_array = array_keys($single_result);
                $table_value_array = array_values($single_result);
                $output .= "\nINSERT INTO $table (";
                $output .= "" . implode(", ", $table_column_array) . ") VALUES (";
                $output .= "'" . implode("','", $table_value_array) . "');\n";
            }
        }
        $file_name = 'database_backup_on_' . date('y-m-d') . '.sql';
        $file_handle = fopen($file_name, 'w+');
        fwrite($file_handle, $output);
        fclose($file_handle);
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file_name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file_name));
        ob_clean();
        flush();
        readfile($file_name);
        unlink($file_name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins=Admin::where('id','!=' , Auth::guard('admin')->user()->id)->with('roles')->get();
//        $admins=Admin::where('id','!=' , Auth::guard('admin')->user()->id)->get();
        return view('admin::dashboard.views.admin.index',compact('admins'));
    }

    public function create()
    {
        $allroles=Role::where('id' , '!=' , 1)->get();
        return view('admin::dashboard.views.admin.create',compact('allroles'));
    }

    public function store(AdminRequest $request)
    {
        $data=$request->validated();
        $data['password']=$request->password;
        $admin=Admin::create($data);
        $admin->assignRole($request->role);
        flash(__('Your Admin has been created successfully!'))->success();
        return redirect()->route('admins.index');
    }

    public function edit($id)
    {
        $admin=Admin::where('id',$id)->first();
        $admin['roles']=DB::table('model_has_roles')->where('model_id',$id)->where('model_type','App\Models\Admin')->select('role_id')->pluck('role_id');
        $roles=Role::where('id' , '!=' , 1)->get();
        return view('admin::dashboard.views.admin.edit',compact('roles','admin'));
    }

    public function update(AdminRequest $request, $id)
    {
        $admin=Admin::where('id',$id)->first();
        $admin->name=$request->name;
        $admin->email=$request->email;
        $admin->image=$request->image;
        if($admin->password!=$request->password){

            $admin->password=$request->password;
        }
        if($request->roles) {
            $admin->roles()->sync($request->role);
        }
        $admin->save();
        flash(__('Your Admin has been updated successfully!'))->success();
        return redirect()->route('admins.index');
    }

    public function destroy($id)
    {
        $admin=Admin::where('id',$id)->first();
        $admin->delete();
        flash(__('Your Admin has been deleted successfully!'))->success();
        return redirect()->route('admins.index');
    }



}
