<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\PlaceBooking;
use App\Models\PreparationBooking;
use App\Models\PreparationOrder;
use App\Modules\Admin\Http\Requests\ServiceReservationRequest;
use Illuminate\Http\Request;
use App\Models\Settings;

use App\Http\Controllers\Controller;

class ServicesOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations=PreparationBooking::get();
        return view('admin::dashboard.views.services-booking.index',compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservation = PreparationBooking::find($id);
        $getReservationId=PreparationBooking::where('id',$reservation->id)->pluck('id');
        $servicesReservation=PreparationOrder::whereIn('preparation_booking_id',$getReservationId)->get();
        return view('admin::dashboard.views.services-booking.show',compact('reservation','servicesReservation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reservation = PreparationBooking::find($id);
        return view('admin::dashboard.views.services-booking.edit',compact('reservation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceReservationRequest $request, $id)
    {
        $reservation = PreparationBooking::find($id);
        $data = $request->validated();
        $reservation->update($data);
        flash(__('Your reservation has been updated successfully!'))->success();
        return redirect()->route('services.reservations.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PreparationBooking::find($id)->delete();
        flash(__('Your booking has been deleted successfully!'))->success();
        return back();
    }

    function generatePdf($id) {
        $settings=Settings::first();
        $data = PreparationBooking::findOrFail($id);
        $getReservationId=PreparationBooking::where('id',$data->id)->pluck('id');
        $servicesReservation=PreparationOrder::whereIn('preparation_booking_id',$getReservationId)->get();
        return view('admin::dashboard.views.services-booking.pdf',compact('data','settings','servicesReservation'));
    }
}
