<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\PlaceCategory;
use App\Modules\Admin\Http\Requests\PlaceCategoryRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class PlacesCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories=PlaceCategory::get();
        return view('admin::dashboard.views.places-categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::dashboard.views.places-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PlaceCategoryRequest $request)
    {
        $data=$request->validated();
        $category= PlaceCategory::create($data);
        if($category->save()){
            flash(__('Your place category has been created successfully!'))->success();
            return back();
        }else{
            flash(__('Sorry! Something went wrong.'))->error();
            return back();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = PlaceCategory::find($id);
        return view('admin::dashboard.views.places-categories.edit',compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlaceCategoryRequest $request, $id)
    {
        $category = PlaceCategory::find($id);
        $data = $request->validated();
        $category->update($data);
        flash(__('Your place category has been updated successfully!'))->success();
        return redirect()->route('places.categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PlaceCategory::find($id)->delete();
        flash(__('Your place category has been deleted successfully!'))->success();
        return back();
    }

    public function updateStatus(Request $request)
    {
        $category = PlaceCategory::findOrFail($request->id);
        $category->status = $request->status;
        if($category->save()){
            return 1;
        }
        return 0;
    }
}
