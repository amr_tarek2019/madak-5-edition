<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\PlaceBooking;
use App\Modules\Admin\Http\Requests\PlaceReservationRequest;
use Illuminate\Http\Request;
use App\Models\Settings;

use App\Http\Controllers\Controller;

class PlacesOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations=PlaceBooking::get();
        return view('admin::dashboard.views.places-booking.index',compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservation = PlaceBooking::find($id);
        return view('admin::dashboard.views.places-booking.show',compact('reservation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reservation = PlaceBooking::find($id);
        return view('admin::dashboard.views.places-booking.edit',compact('reservation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PlaceReservationRequest $request, $id)
    {
        $reservation = PlaceBooking::find($id);
        $data = $request->validated();
        $reservation->update($data);
        flash(__('Your reservation has been updated successfully!'))->success();
        return redirect()->route('places.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PlaceBooking::find($id)->delete();
        flash(__('Your booking has been deleted successfully!'))->success();
        return back();
    }

    function generatePdf($id) {
        $settings=Settings::first();
        $data = PlaceBooking::findOrFail($id);
        return view('admin::dashboard.views.places-booking.pdf',compact('data','settings'));
    }
}
