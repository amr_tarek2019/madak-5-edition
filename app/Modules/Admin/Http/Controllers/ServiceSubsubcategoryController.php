<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\ServiceSubcategory;
use App\Models\SubSubCategory;
use App\Modules\Admin\Http\Requests\ServiceSubsubcategoryRequest;
use App\Modules\Admin\Http\Requests\SubSubCategoryRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class ServiceSubsubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subsubcategories = SubSubCategory::get();
        return view('admin::dashboard.views.service-subsubcategory.index',compact('subsubcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subcategories=ServiceSubcategory::all();
        return view('admin::dashboard.views.service-subsubcategory.create',compact('subcategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceSubsubcategoryRequest $request)
    {
        $data = $request->validated();
        SubSubCategory::create($data);
        flash(__('Your service subsubcategory has been added successfully!'))->success();
        return redirect()->route('services.subsubcategories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategories= ServiceSubcategory::all();
        $subsubcategory = SubSubCategory::find($id);
        return view('admin::dashboard.views.service-subsubcategory.edit',compact('subcategories','subsubcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceSubsubcategoryRequest $request, $id)
    {
        $subsubcategory= SubSubCategory::find($id);
        $data = $request->validated();
        $subsubcategory->update($data);
        flash(__('Your service subsubcategory has been updated successfully!'))->success();
        return redirect()->route('services.subsubcategories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subsubcategory = SubSubCategory::find($id);
        $subsubcategory->delete();
        flash(__('Your service subsubcategory has been deleted successfully!'))->success();
        return back();
    }

    public function updateStatus(Request $request)
    {
        $subsubcategory = SubSubCategory::findOrFail($request->id);
        $subsubcategory->status = $request->status;
        if($subsubcategory->save()){
            return 1;
        }
        return 0;
    }
}
