<?php

namespace App\Modules\Admin\Http\Controllers;

use App\Models\Issue;
use App\Modules\Admin\Http\Requests\IssueRequest;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class IssuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $issues=Issue::all();
        return view('admin::dashboard.views.issue.index',compact('issues'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin::dashboard.views.issue.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(IssueRequest $request)
    {
        $data = $request->validated();
        $issues = Issue::create($data);
        if($issues->save()){
            flash(__('Your issue has been created successfully!'))->success();
            return back();
        }else{
            flash(__('Sorry! Something went wrong.'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $issue = Issue::find($id);
        return view('admin::dashboard.views.issue.edit',compact('issue'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(IssueRequest $request, $id)
    {
        $issue=Issue::find($id);
        $data = $request->validated();
        $issue->update($data);
        flash(__('Your issue has been updated successfully!'))->success();
        return redirect()->route('issues.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Issue::find($id)->delete();
        flash(__('Your issue has been deleted successfully!'))->success();
        return back();
    }

    public function updateStatus(Request $request)
    {
        $issue = Issue::findOrFail($request->id);
        $issue->status = $request->status;
        if($issue->save()){
            return 1;
        }
        return 0;
    }
}
