<?php

namespace App\Modules\Auth\Http\Controllers\Api;

use App\Models\User;
use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    protected $authObject;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authObject = $authRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required',
                'password' => 'required',
                'firebase_token' => 'required',

            ]
        );

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $user = User::where('email',$request->email)->first();

        if (!$user) {
            return response()->json(res_msg($request->header('lang'), failed(), 404, 'user_not_found'));
        }

        $check = Hash::check($request->password, $user->password);
        if ($check) {

            if ($user->user_status == '0') {
                $this->authObject->Sendverificationcode($user);
                return response()->json(res_msg($request->header('lang'), failed(), 405, 'user_not_verified'));
            }
            $jwt = str_random(20);
            $user->update(array('jwt_token' => $jwt,'firebase_token'=>$request->firebase_token));


            // $user['country'] = $this->authObject->GetCountryNameById($user->FK_countries_id, $request->header('lang'));
            return response()->json(res_msg($request->header('lang'), success(), 200, 'logged_in', $user));
        } else {
            return response()->json(res_msg($request->header('lang'), failed(), 401, 'invalid_password'));
        }


    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function UserProfile()
    {
        $lang = getallheaders()['Lang'];
        $user = User::select('id','name','email','jwt_token','image','firebase_token'
            ,'phone','password')->whereJwtToken(getallheaders()['Jwt'])->first();
        return response()->json(res_msg($lang, success(), 200, 'user_profile', $user));


    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function UpdateProfile(Request $request)
    {
        $lang = $request->header('Lang');
        $data = $request->all();
        $user = User::select('id','name','email','jwt_token','image','firebase_token'
            ,'phone','password')->whereJwtToken(getallheaders()['Jwt'])->first();
        if ($user) {

            $validator = Validator::make($data,
                array(
                    'email' => 'unique:users,email,'.$user->id.',id',
                    'phone' => 'unique:users,phone,' . $user->id . ',id',
                )
            );


            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
            }

            $user->update($data);

            //$city = $this->authObject->GetCountryNameById($user->FK_countries_id, $request->header('Lang'));
            // $user['city'] = $city;

            return response()->json(res_msg($lang, success(), 200, 'updated', $user));
        } else {


            return response()->json(res_msg($lang, failed(), 404, 'user_not_found'));


        }


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateNotificationStatus(Request $request)
    {
        $lang = $request->header('Lang');


        $user = User::select('id')->whereJwtToken(getallheaders()['Jwt'])->first();

        if ($user) {
            $validator = Validator::make($request->all(), [
                'notification_status'=>'required',
            ]);

            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
            }

            $user->update(array_merge($request->all()));

            return response()->json(res_msg($lang, success(), 200, 'updated notification status', $user));
        } else {


            return response()->json(res_msg($lang, failed(), 404, 'user_not_found'));


        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateLocation(Request $request)
    {
        $lang = $request->header('Lang');
        $data = $request->all();
        $user = User::whereJwtToken(getallheaders()['Jwt'])->first();
        if ($user) {

            $validator = Validator::make($data,
                array(
                    'lat' =>'required',
                    'lng' =>'required',
                )
            );
            if ($validator->fails()) {
                return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
            }
            $user->update($data);
            return response()->json(res_msg($lang, success(), 200, 'updated', $user));
        } else {
            return response()->json(res_msg($lang, failed(), 404, 'user_not_found'));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SignUp(Request $request)
    {

        $validator = Validator::make($request->all(),
            array(
                'name' => 'required',
                'email' => 'required',
                'password' => 'required',
                'confirm_password' => 'required',
                'firebase_token'=>'required',
                'lat'=>'required',
                'lng'=>'required',
                'phone'=>'required'
            )
        );

        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }

        $emialex = User::where('email',$request->email)->get();
        $mobileex = User::where('phone',$request->phone)->get();
        if (isset($emialex) && count($emialex) > 0) {
            return response()->json(res_msg($request->header('lang'), failed(), 401, 'email_exist'));

        }
        if (isset($mobileex) && count($mobileex) > 0) {
            return response()->json(res_msg($request->header('lang'), failed(), 401, 'phone_exist'));
        }
        $data = $request->all();
        if ($request->password !=null && ($request->password == $request->confirm_password)) {
            $data['password'] = $request->password;
        }else{
            return response()->json(res_msg($request->header('lang'), failed(), 401, 'password not confirmed with password confirmation'));
        }
        $data['jwt_token'] = str_random(20);
        $user = User::create($data);

        $msg = $this->authObject->Sendverificationcode($user);

        return response(res_msg($request->header('lang'), success(), 200, $msg));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function SendVerificationCode(Request $request)
    {

        $validator = Validator::make($request->all(),
            ['email' => 'required',]);
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $flag = $this->authObject->Sendverificationcode($request);
        if ($flag == 'code_sent') {
            return response(res_msg($request->header('Lang'), success(), 200, 'code_sent'));
        } else {
            return response(res_msg($request->header('Lang'), failed(), 404, 'user_not_found'));
        }
    }

    public function VerifyCode(Request $request)
    {
        $validator = Validator::make($request->all(),
            ['verification_code' => 'required']
        );
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }
        $flag = $this->authObject->VerifyCode($request);

        if ($flag=='activated') {
            return response(res_msg($request->header('Lang'), success(), 200, 'activated'));
        } elseif ($flag == 'invalid_code') {
            return response(res_msg($request->header('Lang'), failed(), 401, 'invalid_code'));
        } elseif ($flag == 'user_already_verified') {
            return response(res_msg($request->header('Lang'), failed(), 405, 'user_already_verified'));
        }
    }

    public function ChangePassword(Request $request)
    {
        $lang = $request->header('Lang');
        $validator = Validator::make($request->all(), array('password' => 'required',));
        if ($validator->fails()) {
            return response()->json(['status' => 'error', 'msg' => $validator->messages()]);
        }

        $user = $this->authObject->checkjwt(getallheaders()['Jwt']);
        if (isset($user->id)) {
            $request['id'] = $user->id;
            $flag = $this->authObject->Changepassword($request);
            if (isset($flag->id)) {
                return response(res_msg($lang, success(), 200, 'password_changed'));
            } elseif ($flag == "invalid_oldpassword") {
                return response(res_msg($lang, failed(),401, 'invalid_oldpassword'));
            }
        } else {
            return response()->json(res_msg($lang, failed(), 404, 'user_not_found'));
        }

    }
}
