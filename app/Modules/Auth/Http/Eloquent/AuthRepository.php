<?php


namespace App\Modules\Auth\Http\Eloquent;


use App\Modules\Auth\Http\Interfaces\AuthRepositoryInterface;
use App\Models\User;
use App\Models\Verification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthRepository implements AuthRepositoryInterface
{
    protected $user_Ob;
    protected $verification_Ob;
    public function __construct(User $user,Verification $verification)
    {
        $this->user_Ob=$user;
        $this->verification_Ob=$verification;

    }

    public function SendVerificationCode($request)
    {
        $usertoverify=$this->user_Ob->where('email',$request->email)->first();
        if (isset($usertoverify)){
            $usertoverify->user_status=0;
            $usertoverify->save();

//            $this->verification_Ob->id=$usertoverify->id;
            $this->verification_Ob->user_id=$usertoverify->id;
            $digits = 4;
            $code=rand(pow(10, $digits-1), pow(10, $digits)-1);

            $this->verification_Ob->verify_code= $code;
            $this->verification_Ob->save();
//            $data = array('name'=>$usertoverify->users_name,'code'=>$code);
//
//            Mail::send('mail', $data, function($message)use($usertoverify) {
//                $message->to($usertoverify->email)->subject
//                ('Verification Code');
//                $message->from('info@pariseast.net','Pariseast Application');
//            });

            return 'code_sent';
        }else{
            return "user_not_found";
        }



    }
    public function VerifyCode($request)
    {
        $verificationcode=$this->verification_Ob->where('verify_code',$request->verification_code)->first();
        if (isset($verificationcode)){
            $usertoverify=$this->user_Ob->where('id',$verificationcode->user_id)->first();
            if($usertoverify->user_status == 0){
                $verificationcode->delete();
                $usertoverify->user_status=1;
                $usertoverify->save();
                return  'activated';

            }else{
                $verificationcode->delete();
                return 'user_already_verified';}
        }else{return 'invalid_code';}

    }
    public function CheckJwt($jwt)
    {
        $user=User::where('jwt_token',$jwt)->first();
        if (isset($user->id)){
            return $user;
        }else{
            return "false";
        }
    }
//    public function GetCountryNameById($country_id=null,$lang = "en")
//    {
//        $country=Country::find($country_id);
//        return $country->getTranslation($lang);
//
//    }
//    public function GetCityNameById($city_id=null,$lang = "en")
//    {
//        $city=City::find($city_id);
//        return $city->getTranslation($lang);
//
//    }
    public function ChangePassword($request){
        $userToChangePassword=$this->user_Ob->where('id',$request->id)->first();
        if(isset($request->old_password)){
            $check = Hash::check($request->old_password, $userToChangePassword->password);
            if (!$check){return 'invalid_oldpassword';}
        }
        $userToChangePassword->password= Hash::make($request->password);
        $userToChangePassword->save();
        return $userToChangePassword;
    }


}
