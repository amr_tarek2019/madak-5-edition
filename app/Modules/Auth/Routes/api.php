<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::get('/auth', function (Request $request) {
//    // return $request->auth();
//})->middleware('auth:api');
Route::group(['prefix' => 'Auth'], function () {
    Route::post('/login','Api\AuthController@login');
    Route::get('/profile','Api\AuthController@UserProfile');
    Route::post('/updateprofile','Api\AuthController@UpdateProfile');
    Route::post('/updatenotificationstatus','Api\AuthController@UpdateNotificationStatus');
    Route::post('/updatelocation','Api\AuthController@updateLocation');
    Route::post('/signup','Api\AuthController@SignUp');
    Route::post('/sendverifycode','Api\AuthController@SendVerificationCode');
    Route::post('/verifycode','Api\AuthController@VerifyCode');
    Route::post('/changepassword','Api\AuthController@ChangePassword');
});
