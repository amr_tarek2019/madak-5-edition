<?php

namespace App\Modules\Notification\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('notification', 'Resources/Lang', 'app'), 'notification');
        $this->loadViewsFrom(module_path('notification', 'Resources/Views', 'app'), 'notification');
        $this->loadMigrationsFrom(module_path('notification', 'Database/Migrations', 'app'), 'notification');
        $this->loadConfigsFrom(module_path('notification', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('notification', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
