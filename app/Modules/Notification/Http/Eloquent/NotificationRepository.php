<?php
namespace App\Modules\Notification\Http\Eloquent;

use App\Models\Notification;
use App\Models\User;
use App\Modules\Notification\Http\Interfaces\NotificationRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class NotificationRepository implements NotificationRepositoryInterface
{
    public function GetNotifications($lang,$user_id )
    {

        $notifications = Notification::where('user_id',$user_id)->orderBy('id','asc')->get();

        $allnotifications = array();
        $i = 0;
        foreach ($notifications as $notification) {
            $allnotifications[$i] = array(
                'id'=>$notification->id,
                'title'=>$notification->translate($lang)->title,
                'desc' => $notification->translate($lang)->text,
                'icon' => $notification->icon,
                'user_id' => $notification->user_id,
                'date' => $notification->created_at);
            $i++;
        }
        return $allnotifications;
    }
}
