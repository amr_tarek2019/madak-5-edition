<?php
namespace App\Modules\Notification\Http\Interfaces;

interface NotificationRepositoryInterface
{
    public function GetNotifications($lang,$user_id );
}
