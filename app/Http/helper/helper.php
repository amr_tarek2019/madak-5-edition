<?php

use function foo\func;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;


function unique_file($fileName)
{
    return time() . uniqid().'-'.$fileName;
}
function admin()
{
    return Auth::guard('admin')->user();
}


function success()
{
    return 'success';
}

function error()
{
    return 'error';
}
function expired()
{
    return 'expired';
}


function failed()
{
    return 'failed';
}


function res_msg($lang,$status,$code,$key,$data=null)
{

    $response['code'] = $code;
    $response['status']=$status;

    $response['msg'] = Config::get('response.'.$key.'.'.$lang);

    if ($data!=null){
        $response['data'] = $data;

    }

    return $response;
}
function res_msg_for_orders($lang,$status,$code,$key,$data=null)
{

    $response['code'] = $code;

    $response['msg'] = Config::get('response.'.$key.'.'.$lang);
    if ($data!=null){
        $response['data'] = $data;

    }
    else{
        $response['data']=[];

    }

    return $response;
}


function Cpanel_path($filename = '')
{
    $path="madak/resources/".$filename;
    return url($path);
}
function CpanelCss_path($filename = '')
{
    $path="madak/resources/assets/cpanel/css/".$filename;
    return url($path);
}
function CpanelJs_path($filename = '')
{
    $path="madak/resources/assets/cpanel/js/".$filename;
    return url($path);
}
function CpanelImages_path($filename = '')
{
    $path="madak/resources/assets/cpanel/img/".$filename;
    return url($path);
}
function GetLastUrl($url)
{
    $link_array = explode('/',$url);
    return  end($link_array);
}
function DeleteImg($img=null,$path)
{
    unlink($path.'/'.$img);
    return "done";
}
function CpanelImagesview_path($filename = '')
{
    $path="madak/resources/assets/img/".$filename;
    return url($path);
}
function CpanelImages_upload_path($filename = '')
{
    $path="assets/cpanel/img/".$filename;
    return resource_path($path);
}
function SiteCss_path($filename = '')
{
    $path="madak/resources/assets/site/css/".$filename;
    return url($path);
}
function SiteJs_path($filename = '')
{
    $path="madak/resources/assets/site/js/".$filename;
    return url($path);
}
function SiteImages_path($filename = '')
{
    $path="madak/resources/assets/img/".$filename;
    return url($path);

}
function uploadimageapi($filetoupload,$pathtoupload){
    $image = base64_decode($filetoupload);
    $path = $pathtoupload;
    $img=Image::make($image);
    //////////////////////
    $extension ='jpg';
    $name = rand().substr(sha1(date('Hms')),rand(1,5),10);
    $imgname = date('y-m-d') . $name . "." . $extension;
    $completpath=$path."/".$imgname;
    /////////////////////////
    $img->save($completpath ,50);
    return $imgname;
}
function uploadimage($filetoupload,$pathtoupload){
    $path = $pathtoupload;
    $file=$filetoupload;
    $img=Image::make($filetoupload);
    //////////////////////
    $extension = $file->getClientOriginalExtension();
    $name = sha1($file->getClientOriginalName());
    $imgname = date('y-m-d-H-i-s') . $name . "." . $extension;
    $completpath=$path."/".$imgname;
    /////////////////////////
    $img->save($completpath ,50);
    return $imgname;
}
function uploadvideo($file,$pathtoupload){
    $extension = $file->getClientOriginalExtension();
    $name = sha1($file->getClientOriginalName());
    $videoname = date('y-m-d-H-i-s') . $name . "." . $extension;

    $file->move($pathtoupload, $videoname);
    return $videoname;

}
function autoinsert($model,$request,$arrayofindexes,$pathtoupload,$flag,$uploadimageflag){

    $insertmodalOB=new $model();
    foreach ($arrayofindexes as $value) {
        if(isset($request->$value)){
            $insertmodalOB->$value=$request->$value;
        }
    }
    if(isset($request->image)){
        if($uploadimageflag == "api"){
            $insertmodalOB->image=uploadimageapi($request->image,$pathtoupload);
        }elseif($uploadimageflag == "cpanel"){
            $insertmodalOB->image=uploadimage($request->image,$pathtoupload);
        }
    }

    if($flag == 1){
        $insertmodalOB->save();
        return $insertmodalOB;

    }
    return $insertmodalOB;
}
function autoupdate($model,$request,$arrayofindexes,$pathtoupload,$flag,$id,$uploadimageflag){
    $updatemodalOB=$model::find($id);
    foreach ($arrayofindexes as $value) {
        if(isset($request->$value)){
            $updatemodalOB->$value=$request->$value;
        }
    }

    if(isset($request->image)){
        deletefile($pathtoupload."/".$updatemodalOB->image);
        if($uploadimageflag == "api"){
            $updatemodalOB->image=uploadimageapi($request->image,$pathtoupload);
        }elseif($uploadimageflag == "cpanel"){
            $updatemodalOB->image=uploadimage($request->image,$pathtoupload);
        }
    }

    if($flag == 1){
        $updatemodalOB->save();
        return $updatemodalOB;

    }
    return $updatemodalOB;
}


function filterbylatlng($mylat,$mylng,$radius,$model,$condition = null ){




    $haversine = "(6371 * acos(cos(radians($mylat))
                        * cos(radians($model.lat))
                        * cos(radians($model.lng)
                        - radians($mylng))
                        + sin(radians($mylat))
                        * sin(radians($model.lat))))";
    if($condition == null ){
        $datainradiusrange= DB::table($model)->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])->get() ;
    }else{
        $datainradiusrange= DB::table($model)->select('*')
            ->selectRaw("{$haversine} AS distance")
            ->whereRaw("{$haversine} < ?", [$radius])->where($condition)->get() ;
    }

    return $datainradiusrange;













    $haversine = "(6371 * acos(cos(radians($mylat))
                       * cos(radians($model.lat))
                       * cos(radians($model.long)
                       - radians($mylng))
                       + sin(radians($mylat))
                       * sin(radians($model.lat))))";
    return DB::table($model)->select('*')
        ->selectRaw("{$haversine} AS distance")
        ->whereRaw("{$haversine} < ?", [$radius])->$condition->get() ;


}
function localization($languagekey){
    Session::put('sitelanguage', $languagekey);
}
function changelocale(){
    if(Session::get('sitelanguage')){
        App::setLocale(Session::get('sitelanguage'));
    }
    else{
        App::setLocale('en');
    }
}







function autodelete($model,$imagepath,$id,$flag){
    $deletemodalOB=$model::find($id);
    if(isset($deletemodalOB->image)){
        $path=$imagepath.'/'.$deletemodalOB->image;
        deletefile($path);
    }
    if($flag == 1){
        $deletemodalOB->delete();
    }else{
        return $deletemodalOB;
    }
}

function deletefile($path){
    $completpath=$path;
    \File::delete($completpath);
}

function sendnotification($url,$auth,$playerid,$appid,$notificationcontent,$notificationtitle){

    $content = array("en" => $notificationcontent);
    $headings = array("en" => $notificationtitle);
    $fields = array('app_id' => $appid,'include_player_ids' => array($playerid),'contents' => $content,'headings' => $headings);
    $fields = json_encode($fields);
    return   Curl($url,$auth,$fields);
}
function FilterUsersByLatLng($Ids,$lat,$lng)
{
    $haversine = "(6371 * acos(cos(radians($lat))
                        * cos(radians(users.lat))
                        * cos(radians(users.lng)
                        - radians($lng))
                        + sin(radians($lat))
                        * sin(radians(users.lat))))";

    return DB::table('users')->select('*')
        ->selectRaw("{$haversine} AS distance")
        ->whereRaw("{$haversine} < ?", [1])->leftJoin('products','users.users_id','products.FK_users_id')->leftJoin('product_images','products.id','product_images.FK_products_id')->where('product_images.default_image',1)->whereIn('users_id',$Ids)

        ->select('products.id','products_name','products_price','products_created_at','product_images.img','products.FK_users_id')
        ->get() ;



}
function Curl($url,$fields){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8','Authorization: $auth'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, True);

    return  curl_exec($ch);

}

function unreadMsg()
{
    return \App\Models\Contact::where('view',0)->get();
}

function countUnreadMsg()
{
    return \App\Models\Contact::where('view',0)->count();
}

?>
