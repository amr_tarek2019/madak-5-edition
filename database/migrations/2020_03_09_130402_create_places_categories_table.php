<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('department_id')->default(1)->unsigned();
            $table->string('image')->default('default.png');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('department_id')
                ->references('id')->on('departments')
                ->onDelete('cascade');
        });
        Schema::create('places_categories_translations', function(Blueprint $table)
        {
            $table->increments('place_category_trans_id');
            $table->bigInteger('place_category_id')->unsigned();
            $table->string('locale', 2)->index();
            $table->string('name');

            $table->unique(['place_category_id','locale']);
            $table->foreign('place_category_id')->references('id')->on('places_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places_categories');
    }
}
