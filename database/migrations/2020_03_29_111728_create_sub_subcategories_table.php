<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_subcategories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_subcategory_id')->unsigned();
            $table->string('image');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('service_subcategory_id')
                ->references('id')->on('services_subcategories')
                ->onDelete('cascade');
        });
        Schema::create('sub_subcategories_trans', function (Blueprint $table) {
            $table->increments('sub_subcategory_trans_id');
            $table->bigInteger('sub_sub_category_id')->unsigned();
            $table->string('locale', 2)->index();
            $table->string('name');

            $table->unique(['sub_sub_category_id', 'locale']);
            $table->foreign('sub_sub_category_id')->references('id')->on('sub_subcategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_subcategories');
    }
}
