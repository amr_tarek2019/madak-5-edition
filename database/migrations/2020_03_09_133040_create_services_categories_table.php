<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('department_id')->default(2)->unsigned();
//            $table->string('name');
            $table->string('image')->default('default.png');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('department_id')
                ->references('id')->on('departments')
                ->onDelete('cascade');
        });
        Schema::create('services_cat_translations', function(Blueprint $table)
        {
            $table->increments('service_cat_trans_id');
            $table->bigInteger('service_category_id')->unsigned();
            $table->string('locale', 2)->index();
            $table->string('name');

            $table->unique(['service_category_id','locale']);
            $table->foreign('service_category_id')->references('id')->on('services_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_categories');
    }
}
