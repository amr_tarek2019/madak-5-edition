<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesSubcategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_subcategories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('service_category_id')->unsigned();
//            $table->string('name');
            $table->string('image');
            $table->boolean('status')->default(1);
            $table->timestamps();
            $table->foreign('service_category_id')
                ->references('id')->on('services_categories')
                ->onDelete('cascade');
        });
        Schema::create('service_subcat_translation', function(Blueprint $table)
        {
            $table->increments('service_subcat_trans_id');
            $table->bigInteger('service_subcategory_id')->unsigned();
            $table->string('locale', 2)->index();
            $table->string('name');

            $table->unique(['service_subcategory_id','locale']);
            $table->foreign('service_subcategory_id')->references('id')->on('services_subcategories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_subcategories');
    }
}
