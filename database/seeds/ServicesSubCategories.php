<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ServicesSubCategories extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $ids = [1,2,3,4,5,6,7,8,9,10];
        for($i = 0 ;$i< 10 ;$i++){
            $array = [
                'service_category_id'=>$ids[rand(0,10)],
                'image'=>'default.png',
                'name' => $faker->name,
            ];
            \App\Models\ServiceSubcategory::create($array);
        }
    }
}
