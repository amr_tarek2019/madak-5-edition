<?php

use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name'=> 'mostafa',
            'email' => 'mostafa@gmail.com',
            'password' => bcrypt('123456'),
            'phone' => '01011110021',
            'lat'=>'31.0123',
            'lng'=>'29.4567',
            'firebase_token'=>'zxcvbnm123',
            'jwt_token'=>'asdfghjkl456',
            'image'=>'default.png',
            'verify_code'=>'null',
            'user_type'=>'user',
            'notification_status'=>'1',
            'user_status'=>'1',
            'status'=>'1'
        ]);

        \App\Models\User::create([
            'name'=> '2amgad',
            'email' => '2amgad@gmail.com',
            'password' => bcrypt('987654321'),
            'phone' => '900700100',
            'lat'=>'31.0123',
            'lng'=>'29.4567',
            'firebase_token'=>'zxcvbnm123',
            'jwt_token'=>'asdfghjkl456',
            'image'=>'default.png',
            'verify_code'=>'null',
            'user_type'=>'provider',
            'notification_status'=>'1',
            'user_status'=>'1',
            'status'=>'1'
        ]);
    }
}
