<?php

use Illuminate\Database\Seeder;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Admin::create([
            'name'           => 'admin',
            'email'          => 'admin@admin.com',
            'password'       => \Illuminate\Support\Facades\Hash::make('123456'),
        ]);
    }
}
