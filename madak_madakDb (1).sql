-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 02, 2020 at 11:21 AM
-- Server version: 5.6.47
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `madak_madakDb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `image`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$DuuAv4viWD9Lcd0SZyQIJOMrjzFfPXwE4kHI1HfucM26/zQz4yBl6', '1587991799.jpg', '2020-04-12 04:42:04', '2020-04-27 10:49:59'),
(2, 'amaal', 'amaal@admin.com', '$2y$10$YGCGt04F6Fox21QjzOdUtO2OvMkNN5X7njCaLLHdJnzrFGgS/2pO2', '1587992477.jpg', '2020-04-17 22:00:00', '2020-04-27 11:01:17');

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sender` bigint(20) UNSIGNED NOT NULL,
  `receiver` bigint(20) UNSIGNED NOT NULL,
  `place_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `chat_images`
--

CREATE TABLE `chat_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `chat_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-04-14 22:00:00', '2020-04-14 22:00:00'),
(2, 0, '2020-04-14 22:00:00', '2020-04-14 21:18:32'),
(3, 1, '2020-04-14 22:00:00', '2020-04-14 22:00:00'),
(4, 0, '2020-04-14 22:00:00', '2020-04-14 21:17:47'),
(6, 1, '2020-04-14 20:51:27', '2020-04-14 20:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `city_translations`
--

CREATE TABLE `city_translations` (
  `cities_trans_id` int(10) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city_translations`
--

INSERT INTO `city_translations` (`cities_trans_id`, `city_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'Abbassia'),
(2, 1, 'ar', 'العباسية'),
(3, 2, 'en', 'Ain Shams'),
(4, 2, 'ar', 'عين شمس'),
(5, 3, 'en', 'Nasr City'),
(6, 3, 'ar', 'مدينة نصر'),
(7, 4, 'en', 'Helwan'),
(8, 4, 'ar', 'حلوان'),
(11, 6, 'en', 'elnozha'),
(12, 6, 'ar', 'النزهة');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `issue_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `user_id`, `issue_id`, `name`, `email`, `message`, `view`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2amgad', '2amgad@yahoo.com', 'test', 1, '2020-03-30 10:42:49', '2020-04-14 21:52:01'),
(2, 2, 1, '2amgad', '2amgad@yahoo.com', 'test', 0, '2020-04-06 09:36:24', '2020-04-06 09:36:24'),
(3, 2, 1, 'melnagar271@gmail.com', 'mostafa', 'tedt', 0, '2020-04-06 09:44:44', '2020-04-06 09:44:44'),
(4, 1, 1, '2amgad', '2amgad@yahoo.com', 'test', 0, '2020-04-29 08:38:51', '2020-04-29 08:38:51'),
(5, 1, 1, '2amgad', '2amgad@yahoo.com', 'test', 0, '2020-04-29 08:40:48', '2020-04-29 08:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `image`, `created_at`, `updated_at`) VALUES
(1, 'default.png', '2020-03-31 22:00:00', NULL),
(2, 'default.png', '2020-03-31 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `departments_translations`
--

CREATE TABLE `departments_translations` (
  `department_trans_id` int(10) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments_translations`
--

INSERT INTO `departments_translations` (`department_trans_id`, `department_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'places'),
(2, 1, 'ar', 'اماكن'),
(3, 2, 'en', 'preparations'),
(4, 2, 'ar', 'تحضيرات');

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE `issues` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issues`
--

INSERT INTO `issues` (`id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-03-25 22:00:00', NULL),
(2, 0, '2020-03-25 22:00:00', NULL),
(3, 1, '2020-03-25 22:00:00', NULL),
(4, 0, '2020-03-25 22:00:00', NULL),
(5, 1, '2020-03-25 22:00:00', NULL),
(6, 0, '2020-03-25 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `issues_translations`
--

CREATE TABLE `issues_translations` (
  `issue_trans_id` int(10) UNSIGNED NOT NULL,
  `issue_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `issues_translations`
--

INSERT INTO `issues_translations` (`issue_trans_id`, `issue_id`, `locale`, `text`) VALUES
(1, 1, 'en', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'),
(2, 1, 'ar', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.'),
(3, 2, 'en', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(4, 2, 'ar', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.\r\n\r\n'),
(5, 3, 'en', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(6, 3, 'ar', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.\r\n\r\n'),
(7, 4, 'en', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n'),
(8, 4, 'ar', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.\r\n\r\n'),
(9, 5, 'en', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(10, 5, 'ar', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.\r\n\r\n'),
(11, 6, 'en', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(12, 6, 'ar', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2020_03_09_140151_create_contacts_table', 1),
(17, '2020_03_09_142039_create_chat_images_table', 1),
(19, '2020_03_09_142921_create_places_booking_table', 1),
(25, '2020_03_09_140436_create_chats_table', 3),
(29, '2020_03_09_135818_create_issues_table', 4),
(31, '2020_03_09_135700_create_cities_table', 5),
(32, '2020_03_09_135936_create_notifications_table', 6),
(53, '2020_03_31_141041_create_genders_table', 16),
(55, '2020_03_31_145416_create_departments_table', 17),
(58, '2020_03_09_131416_create_places_table', 20),
(61, '2020_03_09_130402_create_places_categories_table', 22),
(62, '2020_03_09_132023_create_places_images_table', 23),
(63, '2020_03_31_082241_create_places_orders_table', 24),
(64, '2020_04_04_141822_create_places_preparations_table', 25),
(65, '2020_03_09_143707_create_preparations_order_table', 26),
(66, '2020_03_09_143322_create_preparation_booking_table', 27),
(67, '2020_03_09_124629_create_providers_table', 28),
(68, '2020_03_09_142335_create_ratings_table', 29),
(69, '2020_03_09_134522_create_services_table', 30),
(70, '2020_03_09_133040_create_services_categories_table', 31),
(71, '2020_03_31_083255_create_services_orders_table', 32),
(72, '2020_03_09_133258_create_services_subcategories_table', 33),
(73, '2020_03_09_144113_create_service_provider_request_table', 34),
(74, '2020_03_29_111728_create_sub_subcategories_table', 35),
(75, '2020_03_17_142738_create_verifications_table', 36),
(76, '2020_04_11_113212_create_admins_table', 37),
(77, '2020_04_11_121340_create_permission_tables', 38);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\Admin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\Admin', 1),
(1, 'App\\Models\\Admin', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `icon`, `created_at`, `updated_at`) VALUES
(1, 2, 'default.png', NULL, NULL),
(2, 2, 'default.png', NULL, NULL),
(3, 1, 'http://127.0.0.1:8000/madak/resources/assets/img/user/defaultuser.png', '2020-03-30 12:32:13', '2020-03-30 12:32:13');

-- --------------------------------------------------------

--
-- Table structure for table `notification_translations`
--

CREATE TABLE `notification_translations` (
  `notifications_trans_id` int(10) UNSIGNED NOT NULL,
  `notification_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notification_translations`
--

INSERT INTO `notification_translations` (`notifications_trans_id`, `notification_id`, `locale`, `title`, `text`) VALUES
(1, 1, 'en', 'What is Lorem Ipsum?', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'),
(2, 1, 'ar', 'ما هو \"لوريم إيبسوم\" ؟\r\n', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.\r\n\r\n'),
(3, 2, 'en', 'What is Lorem Ipsum?\r\n', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n'),
(4, 2, 'ar', 'ما هو \"لوريم إيبسوم\" ؟\r\n', 'لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق \"ليتراسيت\" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل \"ألدوس بايج مايكر\" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.\r\n\r\n'),
(5, 3, 'en', 'title', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'admin', '2020-04-10 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `places`
--

CREATE TABLE `places` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` bigint(20) UNSIGNED NOT NULL,
  `place_category_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `gender` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `present_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `main_image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places`
--

INSERT INTO `places` (`id`, `provider_id`, `place_category_id`, `city_id`, `gender`, `name`, `present_type`, `price`, `quantity`, `date_from`, `date_to`, `lat`, `lng`, `address`, `desc`, `main_image`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 2, 1, 'test', 'test', '200', '2', '01', '15', '31.1234', '29.9876', 'test', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n', 'default.png', '2020-03-31 22:00:00', NULL),
(3, 1, 1, 1, 1, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15860107895e889aa516215.png', '2020-04-04 12:33:09', '2020-04-04 12:33:09'),
(4, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862914335e8ce2e914eda.png', '2020-04-07 18:30:34', '2020-04-07 18:30:34'),
(5, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862916545e8ce3c6bd39f.png', '2020-04-07 18:34:15', '2020-04-07 18:34:15'),
(6, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862918065e8ce45ecb9b2.png', '2020-04-07 18:36:47', '2020-04-07 18:36:47'),
(7, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862918885e8ce4b0e1da2.png', '2020-04-07 18:38:09', '2020-04-07 18:38:09'),
(8, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862919735e8ce50598adc.png', '2020-04-07 18:39:33', '2020-04-07 18:39:33'),
(9, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862919955e8ce51b582e1.png', '2020-04-07 18:39:55', '2020-04-07 18:39:55'),
(10, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '', '2020-04-07 19:30:30', '2020-04-07 19:30:30'),
(11, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '', '2020-04-07 19:32:12', '2020-04-07 19:32:12'),
(12, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '', '2020-04-07 19:33:12', '2020-04-07 19:33:12'),
(13, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '', '2020-04-07 19:34:45', '2020-04-07 19:34:45'),
(15, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '', '2020-04-07 19:35:16', '2020-04-07 19:35:16'),
(17, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '', '2020-04-07 19:36:41', '2020-04-07 19:36:41'),
(18, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862972705e8cf9b6ae2b6.png', '2020-04-07 20:07:50', '2020-04-07 20:07:50'),
(19, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862974345e8cfa5aa9a0d.png', '2020-04-07 20:10:34', '2020-04-07 20:10:34'),
(20, 1, 1, 1, 0, 'grand hotel resort', 'wedding', '50000', '300', '2020/03/30', '2020/04/15', '31.0123', '29.4567', 'nasr city', 'test', '15862974655e8cfa79c9a16.png', '2020-04-07 20:11:06', '2020-04-07 20:11:06');

-- --------------------------------------------------------

--
-- Table structure for table `places_booking`
--

CREATE TABLE `places_booking` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `place_id` bigint(20) UNSIGNED NOT NULL,
  `date_from` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places_booking`
--

INSERT INTO `places_booking` (`id`, `user_id`, `place_id`, `date_from`, `date_to`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2020/04/29', '2020/05/29', 0, '2020-04-29 07:30:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `places_categories`
--

CREATE TABLE `places_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places_categories`
--

INSERT INTO `places_categories` (`id`, `department_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'default.png', 1, '2020-04-06 22:00:00', '2020-04-19 12:07:57'),
(2, 1, 'default.png', 1, '2020-04-06 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `places_categories_translations`
--

CREATE TABLE `places_categories_translations` (
  `place_category_trans_id` int(10) UNSIGNED NOT NULL,
  `place_category_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places_categories_translations`
--

INSERT INTO `places_categories_translations` (`place_category_trans_id`, `place_category_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'hotels'),
(2, 1, 'ar', 'فنادق'),
(3, 2, 'en', 'palaces'),
(4, 2, 'ar', 'قصور');

-- --------------------------------------------------------

--
-- Table structure for table `places_images`
--

CREATE TABLE `places_images` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `place_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places_images`
--

INSERT INTO `places_images` (`id`, `place_id`, `image`, `created_at`, `updated_at`) VALUES
(1, 4, '15862914355e8ce2eb7f54b.png', '2020-04-07 18:30:36', '2020-04-07 18:30:36'),
(2, 4, '15862914365e8ce2eca1548.png', '2020-04-07 18:30:36', '2020-04-07 18:30:36'),
(3, 5, '15862916555e8ce3c74d907.png', '2020-04-07 18:34:15', '2020-04-07 18:34:15'),
(4, 5, '15862916565e8ce3c81947a.png', '2020-04-07 18:34:16', '2020-04-07 18:34:16'),
(5, 6, '15862918075e8ce45f4dcb0.png', '2020-04-07 18:36:47', '2020-04-07 18:36:47'),
(6, 6, '15862918075e8ce45f9efcc.png', '2020-04-07 18:36:47', '2020-04-07 18:36:47'),
(7, 7, '15862918895e8ce4b1e079b.png', '2020-04-07 18:38:10', '2020-04-07 18:38:10'),
(8, 7, '15862918915e8ce4b310878.png', '2020-04-07 18:38:11', '2020-04-07 18:38:11'),
(9, 8, '15862919745e8ce50634012.png', '2020-04-07 18:39:34', '2020-04-07 18:39:34'),
(10, 8, '15862919745e8ce506b236d.png', '2020-04-07 18:39:35', '2020-04-07 18:39:35'),
(11, 9, '15862919955e8ce51bae87a.png', '2020-04-07 18:39:55', '2020-04-07 18:39:55'),
(12, 9, '15862919965e8ce51c1d43e.png', '2020-04-07 18:39:56', '2020-04-07 18:39:56'),
(13, 10, '15862950305e8cf0f6eb0af.png', '2020-04-07 19:30:31', '2020-04-07 19:30:31'),
(14, 10, '15862950315e8cf0f7bc9c9.png', '2020-04-07 19:30:31', '2020-04-07 19:30:31'),
(15, 11, '15862951335e8cf15d83497.png', '2020-04-07 19:32:13', '2020-04-07 19:32:13'),
(16, 11, '15862951345e8cf15e67984.png', '2020-04-07 19:32:14', '2020-04-07 19:32:14'),
(17, 12, '15862951935e8cf199062ea.png', '2020-04-07 19:33:13', '2020-04-07 19:33:13'),
(18, 12, '15862951935e8cf19945975.png', '2020-04-07 19:33:13', '2020-04-07 19:33:13'),
(19, 17, '15862954015e8cf26990cbe.png', '2020-04-07 19:36:41', '2020-04-07 19:36:41'),
(20, 17, '15862954015e8cf269cb9e1.png', '2020-04-07 19:36:41', '2020-04-07 19:36:41'),
(21, 18, '15862972715e8cf9b707bc5.png', '2020-04-07 20:07:51', '2020-04-07 20:07:51'),
(22, 18, '15862972715e8cf9b79925a.png', '2020-04-07 20:07:52', '2020-04-07 20:07:52');

-- --------------------------------------------------------

--
-- Table structure for table `places_orders`
--

CREATE TABLE `places_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places_orders`
--

INSERT INTO `places_orders` (`id`, `user_id`, `provider_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2020-04-29 07:31:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `places_preparations`
--

CREATE TABLE `places_preparations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `place_id` bigint(20) UNSIGNED NOT NULL,
  `preparations_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `places_preparations`
--

INSERT INTO `places_preparations` (`id`, `place_id`, `preparations_id`, `created_at`, `updated_at`) VALUES
(1, 2, 5, '2020-04-29 07:31:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `preparations_order`
--

CREATE TABLE `preparations_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `preparation_booking_id` bigint(20) UNSIGNED NOT NULL,
  `services_sub_sub_category_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `preparations_order`
--

INSERT INTO `preparations_order` (`id`, `preparation_booking_id`, `services_sub_sub_category_id`, `quantity`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '100', '2020-04-29 07:32:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `preparation_booking`
--

CREATE TABLE `preparation_booking` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `preparation_booking`
--

INSERT INTO `preparation_booking` (`id`, `user_id`, `message`, `date`, `total`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'test', '2020/04/29', '100', 1, '2020-04-29 07:32:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `providers`
--

CREATE TABLE `providers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `providers`
--

INSERT INTO `providers` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2, '2020-04-06 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `place_id` bigint(20) UNSIGNED NOT NULL,
  `rate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `review` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'admin', NULL, NULL),
(2, 'create admin', 'admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` bigint(20) UNSIGNED NOT NULL,
  `service_category_id` bigint(20) UNSIGNED NOT NULL,
  `service_subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `service_sub_sub_category_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `gender` int(11) NOT NULL,
  `present_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_before` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_after` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `provider_id`, `service_category_id`, `service_subcategory_id`, `service_sub_sub_category_id`, `city_id`, `gender`, `present_type`, `price_before`, `price_after`, `quantity`, `lat`, `lng`, `address`, `date`, `image`, `desc`, `created_at`, `updated_at`) VALUES
(5, 1, 1, 1, 1, 1, 0, 'test', '150', '100', '2', '30.01234', '31.56789', 'test', '2020/04/22', 'default.png', 'test', '2020-04-21 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services_categories`
--

CREATE TABLE `services_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `department_id` bigint(20) UNSIGNED NOT NULL DEFAULT '2',
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default.png',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services_categories`
--

INSERT INTO `services_categories` (`id`, `department_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'default.png', 1, '2020-04-06 22:00:00', NULL),
(2, 2, 'default.png', 1, '2020-04-06 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services_cat_translations`
--

CREATE TABLE `services_cat_translations` (
  `service_cat_trans_id` int(10) UNSIGNED NOT NULL,
  `service_category_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services_cat_translations`
--

INSERT INTO `services_cat_translations` (`service_cat_trans_id`, `service_category_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'meals'),
(2, 1, 'ar', 'وجبات'),
(3, 2, 'en', 'decorations'),
(4, 2, 'ar', 'ديكورات');

-- --------------------------------------------------------

--
-- Table structure for table `services_orders`
--

CREATE TABLE `services_orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `provider_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services_subcategories`
--

CREATE TABLE `services_subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_category_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services_subcategories`
--

INSERT INTO `services_subcategories` (`id`, `service_category_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'default.png', 1, '2020-04-06 22:00:00', '2020-04-21 22:41:11'),
(2, 2, 'default.png', 1, '2020-04-06 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `service_provider_request`
--

CREATE TABLE `service_provider_request` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_provider_request`
--

INSERT INTO `service_provider_request` (`id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(2, 8, 1, '2020-04-14 22:00:00', '2020-04-15 11:17:21');

-- --------------------------------------------------------

--
-- Table structure for table `service_subcat_translation`
--

CREATE TABLE `service_subcat_translation` (
  `service_subcat_trans_id` int(10) UNSIGNED NOT NULL,
  `service_subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `service_subcat_translation`
--

INSERT INTO `service_subcat_translation` (`service_subcat_trans_id`, `service_subcategory_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'barber'),
(2, 1, 'ar', 'حلاق'),
(3, 2, 'en', 'test'),
(4, 2, 'ar', 'تيست');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) UNSIGNED NOT NULL,
  `app_name` varchar(191) NOT NULL,
  `logo` varchar(191) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `app_name`, `logo`, `phone`, `created_at`, `updated_at`) VALUES
(1, 'madak', '1588148620.png', '0101111002', '2020-04-29 08:23:40', '2020-04-29 06:23:40');

-- --------------------------------------------------------

--
-- Table structure for table `sub_subcategories`
--

CREATE TABLE `sub_subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_subcategory_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_subcategories`
--

INSERT INTO `sub_subcategories` (`id`, `service_subcategory_id`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'default.png', 1, '2020-04-06 22:00:00', NULL),
(2, 1, 'default.png', 1, '2020-04-06 22:00:00', NULL),
(3, 2, 'default.png', 1, '2020-04-06 22:00:00', NULL),
(4, 1, 'default.png', 1, '2020-04-06 22:00:00', NULL),
(5, 2, 'default.png', 1, '2020-04-06 22:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_subcategories_trans`
--

CREATE TABLE `sub_subcategories_trans` (
  `sub_subcategory_trans_id` int(10) UNSIGNED NOT NULL,
  `sub_sub_category_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_subcategories_trans`
--

INSERT INTO `sub_subcategories_trans` (`sub_subcategory_trans_id`, `sub_sub_category_id`, `locale`, `name`) VALUES
(1, 1, 'en', 'test'),
(2, 1, 'ar', 'test'),
(3, 2, 'en', 'test'),
(4, 2, 'ar', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firebase_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jwt_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `notification_status` tinyint(1) NOT NULL DEFAULT '1',
  `user_status` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `email_verified_at`, `password`, `lat`, `lng`, `firebase_token`, `jwt_token`, `image`, `user_type`, `notification_status`, `user_status`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mostafa', 'mostafa@gmail.com', '01011110021', NULL, '$2y$10$srDVgNltzU9.IFe6gEqqMOesNhXhkbcNg93jEYSvitZQc80GH.14K', '30.9876', '29.5432', 'zxcvbnm123', 'asdfghjkl456', '', 'user', 1, 1, 1, NULL, '2020-03-21 13:38:58', '2020-03-29 12:05:07'),
(2, '2amgad', '2amgad@gmail.com', '900700100', NULL, '$2y$10$FlWOzecze06j3TG0g7wjJeBY8hBCVsRoBldQH02BkHxtDaWky6RWO', '30.1263167', '31.3752096', '123456789zxcvbnm', '58wglipC3Hw2ScjkcYIU', '15855717895e81e7cda099e.png', 'provider', 1, 1, 1, NULL, '2020-03-21 13:38:58', '2020-04-07 18:02:34'),
(8, 'amaal', 'amaal@yahoo.com', '01018472418', NULL, '$2y$10$c.H06jbZyP0JipKKUfbBbeig8d6ucZ4EM4NCQ0YV18d0P3xFSFKM6', '29.839102', '31.349626', '987654321mnbvcxz', '0BU9ZRkbft1yV1o5vLky', '15855717895e81e7cda099e.png', 'user', 1, 1, 1, NULL, '2020-03-31 09:47:04', '2020-04-29 11:15:55'),
(9, 'Tefa2', '2', '01121805384', NULL, '$2y$10$QcanHdB5w1Zkc9g7ujv.gu7eBGBx85H2APFTt3STnwkxFg0mWpDJa', '31.9876', '29.5432', 'SFP#$DFSA$@AFD', 'RdYuwEl1xpYA7Ouxdsj5', '', 'user', 1, 0, 1, NULL, '2020-04-29 07:35:16', '2020-04-29 07:35:16'),
(10, 'Tefa2', 'tofa@yahoo.com', '01121805388', NULL, '$2y$10$J1gOdLPzzsa8kE7aZPzcX.NEW931U1rxTgj2.3OUTyoxTEuTrgFUy', '31.9876', '29.5432', 'SFP#$DFSA$@AFD', 'MTrdwEA2HHtWeHczLeRi', '', 'user', 1, 0, 1, NULL, '2020-04-29 07:35:33', '2020-04-29 07:35:33'),
(11, 'Tefa2', 'tofa1@yahoo.com', '01121805398', NULL, '$2y$10$9xGDetGHPF3BhBQQ/GnI9OkAd.QF7i925SfylyCE8SuhxS.Pxglpi', '31.0123', '29.4567', '123456789zxcvbnm', 'e8degIdq1yr70g91g6QT', '', 'user', 1, 1, 1, NULL, '2020-04-29 07:36:44', '2020-04-29 08:02:49'),
(12, 'sdf', 'alisdfm@yahoo.com', '011218053982', NULL, '$2y$10$wSU5SnwBOtEfOxiX3.6ufuEJSeFTURpj0ynKXiHglFwnz9Sezn2oW', '31.9876', '29.5432', 'SFP#$DFSA$@AFD', 'cfp6HMUQkZo0RrzJpxSL', '', 'user', 1, 0, 1, NULL, '2020-04-29 07:56:50', '2020-04-29 08:02:05'),
(13, 'mostafa elnagar', 'melnagar271@gmail.com', '01090624441', NULL, '$2y$10$0savwHYzLk7e0skecu2huuHHH6wguIUvHF5Lppy5Fp99DE27MX46C', '0', '0', 'cjpWqO4Qidg:APA91bGC7Ew55vXwjJkdsRF5wzzj4cXDlgJhjq4vwIRTCcjnW4FPf8Wxo_obcZwn1wDttYGN0TLY_GuXv7YI792YeOdqsLAYfTo9AhcdW3gBFKPzjcNwLjL5yerf3xMacUo4lOh9s2jA', 'GRzYghumlqeAJowqUdTN', '', 'user', 1, 1, 1, NULL, '2020-04-29 10:39:28', '2020-04-29 11:09:46'),
(14, 'tesr', 'ophthalmicapp@gmail.com', '12587', NULL, '$2y$10$BuIw6LVbHDtOJhDdxtfAe.Y2jKekEMjh.4KDsw9VI6OwKRrvBD4/S', '0', '0', 'cNWscuNJwBY:APA91bGOkdInTUht6CtBcmpzQBNQRRGKxWaBp5fJGbDle3vX3B9LcksoG-vsBZOvyUwzri7Qd1VN0jZp2oXf1n5xGYNbDUeRl-0U41GQrUn_-Khmx5T6mTV48Ivsm5ou0j3zunOjjyDn', 'JspT3lR7JjDjMFhnteEv', '', 'user', 1, 1, 1, NULL, '2020-04-29 10:42:16', '2020-04-29 10:42:32');

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

CREATE TABLE `verifications` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `verify_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verifications`
--

INSERT INTO `verifications` (`id`, `user_id`, `verify_code`, `created_at`, `updated_at`) VALUES
(1, 9, '1937', '2020-04-29 07:35:16', '2020-04-29 07:35:16'),
(2, 10, '3268', '2020-04-29 07:35:33', '2020-04-29 07:35:33'),
(3, 10, '6893', '2020-04-29 07:35:41', '2020-04-29 07:35:41'),
(4, 10, '9674', '2020-04-29 07:35:54', '2020-04-29 07:35:54'),
(5, 10, '3928', '2020-04-29 07:36:02', '2020-04-29 07:36:02'),
(6, 10, '3132', '2020-04-29 07:36:26', '2020-04-29 07:36:26'),
(7, 11, '7361', '2020-04-29 07:36:44', '2020-04-29 07:36:44'),
(10, 12, '2816', '2020-04-29 08:02:05', '2020-04-29 08:02:05'),
(11, 13, '5088', '2020-04-29 10:39:28', '2020-04-29 10:39:28'),
(12, 13, '4864', '2020-04-29 10:39:37', '2020-04-29 10:39:37');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chats_sender_foreign` (`sender`),
  ADD KEY `chats_receiver_foreign` (`receiver`),
  ADD KEY `chats_place_id_foreign` (`place_id`);

--
-- Indexes for table `chat_images`
--
ALTER TABLE `chat_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chat_images_chat_id_foreign` (`chat_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city_translations`
--
ALTER TABLE `city_translations`
  ADD PRIMARY KEY (`cities_trans_id`),
  ADD UNIQUE KEY `city_translations_city_id_locale_unique` (`city_id`,`locale`),
  ADD KEY `city_translations_locale_index` (`locale`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contacts_user_id_foreign` (`user_id`),
  ADD KEY `contacts_issue_id_foreign` (`issue_id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments_translations`
--
ALTER TABLE `departments_translations`
  ADD PRIMARY KEY (`department_trans_id`),
  ADD UNIQUE KEY `departments_translations_department_id_locale_unique` (`department_id`,`locale`),
  ADD KEY `departments_translations_locale_index` (`locale`);

--
-- Indexes for table `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issues_translations`
--
ALTER TABLE `issues_translations`
  ADD PRIMARY KEY (`issue_trans_id`),
  ADD UNIQUE KEY `issues_translations_issue_id_locale_unique` (`issue_id`,`locale`),
  ADD KEY `issues_translations_locale_index` (`locale`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`);

--
-- Indexes for table `notification_translations`
--
ALTER TABLE `notification_translations`
  ADD PRIMARY KEY (`notifications_trans_id`),
  ADD UNIQUE KEY `notification_translations_notification_id_locale_unique` (`notification_id`,`locale`),
  ADD KEY `notification_translations_locale_index` (`locale`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_provider_id_foreign` (`provider_id`),
  ADD KEY `places_place_category_id_foreign` (`place_category_id`),
  ADD KEY `places_city_id_foreign` (`city_id`);

--
-- Indexes for table `places_booking`
--
ALTER TABLE `places_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_booking_user_id_foreign` (`user_id`),
  ADD KEY `places_booking_place_id_foreign` (`place_id`);

--
-- Indexes for table `places_categories`
--
ALTER TABLE `places_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_categories_department_id_foreign` (`department_id`);

--
-- Indexes for table `places_categories_translations`
--
ALTER TABLE `places_categories_translations`
  ADD PRIMARY KEY (`place_category_trans_id`),
  ADD UNIQUE KEY `places_categories_translations_place_category_id_locale_unique` (`place_category_id`,`locale`),
  ADD KEY `places_categories_translations_locale_index` (`locale`);

--
-- Indexes for table `places_images`
--
ALTER TABLE `places_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_images_place_id_foreign` (`place_id`);

--
-- Indexes for table `places_orders`
--
ALTER TABLE `places_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_orders_user_id_foreign` (`user_id`),
  ADD KEY `places_orders_provider_id_foreign` (`provider_id`),
  ADD KEY `places_orders_order_id_foreign` (`order_id`);

--
-- Indexes for table `places_preparations`
--
ALTER TABLE `places_preparations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `places_preparations_place_id_foreign` (`place_id`),
  ADD KEY `places_preparations_preparations_id_foreign` (`preparations_id`);

--
-- Indexes for table `preparations_order`
--
ALTER TABLE `preparations_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `preparations_order_preparation_booking_id_foreign` (`preparation_booking_id`),
  ADD KEY `preparations_order_services_sub_sub_category_id_foreign` (`services_sub_sub_category_id`);

--
-- Indexes for table `preparation_booking`
--
ALTER TABLE `preparation_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `preparation_booking_user_id_foreign` (`user_id`);

--
-- Indexes for table `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `providers_user_id_foreign` (`user_id`);

--
-- Indexes for table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ratings_user_id_foreign` (`user_id`),
  ADD KEY `ratings_place_id_foreign` (`place_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_provider_id_foreign` (`provider_id`),
  ADD KEY `services_service_category_id_foreign` (`service_category_id`),
  ADD KEY `services_service_subcategory_id_foreign` (`service_subcategory_id`),
  ADD KEY `services_service_sub_sub_category_id_foreign` (`service_sub_sub_category_id`),
  ADD KEY `services_city_id_foreign` (`city_id`);

--
-- Indexes for table `services_categories`
--
ALTER TABLE `services_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_categories_department_id_foreign` (`department_id`);

--
-- Indexes for table `services_cat_translations`
--
ALTER TABLE `services_cat_translations`
  ADD PRIMARY KEY (`service_cat_trans_id`),
  ADD UNIQUE KEY `services_cat_translations_service_category_id_locale_unique` (`service_category_id`,`locale`),
  ADD KEY `services_cat_translations_locale_index` (`locale`);

--
-- Indexes for table `services_orders`
--
ALTER TABLE `services_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_orders_user_id_foreign` (`user_id`),
  ADD KEY `services_orders_provider_id_foreign` (`provider_id`),
  ADD KEY `services_orders_order_id_foreign` (`order_id`);

--
-- Indexes for table `services_subcategories`
--
ALTER TABLE `services_subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_subcategories_service_category_id_foreign` (`service_category_id`);

--
-- Indexes for table `service_provider_request`
--
ALTER TABLE `service_provider_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_provider_request_user_id_foreign` (`user_id`);

--
-- Indexes for table `service_subcat_translation`
--
ALTER TABLE `service_subcat_translation`
  ADD PRIMARY KEY (`service_subcat_trans_id`),
  ADD UNIQUE KEY `service_subcat_translation_service_subcategory_id_locale_unique` (`service_subcategory_id`,`locale`),
  ADD KEY `service_subcat_translation_locale_index` (`locale`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_subcategories`
--
ALTER TABLE `sub_subcategories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_subcategories_service_subcategory_id_foreign` (`service_subcategory_id`);

--
-- Indexes for table `sub_subcategories_trans`
--
ALTER TABLE `sub_subcategories_trans`
  ADD PRIMARY KEY (`sub_subcategory_trans_id`),
  ADD UNIQUE KEY `sub_subcategories_trans_sub_sub_category_id_locale_unique` (`sub_sub_category_id`,`locale`),
  ADD KEY `sub_subcategories_trans_locale_index` (`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `verifications_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `chat_images`
--
ALTER TABLE `chat_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `city_translations`
--
ALTER TABLE `city_translations`
  MODIFY `cities_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `departments_translations`
--
ALTER TABLE `departments_translations`
  MODIFY `department_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `issues`
--
ALTER TABLE `issues`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `issues_translations`
--
ALTER TABLE `issues_translations`
  MODIFY `issue_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notification_translations`
--
ALTER TABLE `notification_translations`
  MODIFY `notifications_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `places`
--
ALTER TABLE `places`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `places_booking`
--
ALTER TABLE `places_booking`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `places_categories`
--
ALTER TABLE `places_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `places_categories_translations`
--
ALTER TABLE `places_categories_translations`
  MODIFY `place_category_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `places_images`
--
ALTER TABLE `places_images`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `places_orders`
--
ALTER TABLE `places_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `places_preparations`
--
ALTER TABLE `places_preparations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `preparations_order`
--
ALTER TABLE `preparations_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `preparation_booking`
--
ALTER TABLE `preparation_booking`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `providers`
--
ALTER TABLE `providers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `services_categories`
--
ALTER TABLE `services_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services_cat_translations`
--
ALTER TABLE `services_cat_translations`
  MODIFY `service_cat_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `services_orders`
--
ALTER TABLE `services_orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services_subcategories`
--
ALTER TABLE `services_subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_provider_request`
--
ALTER TABLE `service_provider_request`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_subcat_translation`
--
ALTER TABLE `service_subcat_translation`
  MODIFY `service_subcat_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sub_subcategories`
--
ALTER TABLE `sub_subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sub_subcategories_trans`
--
ALTER TABLE `sub_subcategories_trans`
  MODIFY `sub_subcategory_trans_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `chats`
--
ALTER TABLE `chats`
  ADD CONSTRAINT `chats_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chats_receiver_foreign` FOREIGN KEY (`receiver`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chats_sender_foreign` FOREIGN KEY (`sender`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `chat_images`
--
ALTER TABLE `chat_images`
  ADD CONSTRAINT `chat_images_chat_id_foreign` FOREIGN KEY (`chat_id`) REFERENCES `chats` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `city_translations`
--
ALTER TABLE `city_translations`
  ADD CONSTRAINT `city_translations_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_issue_id_foreign` FOREIGN KEY (`issue_id`) REFERENCES `issues` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `contacts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `departments_translations`
--
ALTER TABLE `departments_translations`
  ADD CONSTRAINT `departments_translations_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `issues_translations`
--
ALTER TABLE `issues_translations`
  ADD CONSTRAINT `issues_translations_issue_id_foreign` FOREIGN KEY (`issue_id`) REFERENCES `issues` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notification_translations`
--
ALTER TABLE `notification_translations`
  ADD CONSTRAINT `notification_translations_notification_id_foreign` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places`
--
ALTER TABLE `places`
  ADD CONSTRAINT `places_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `places_place_category_id_foreign` FOREIGN KEY (`place_category_id`) REFERENCES `places_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `places_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places_booking`
--
ALTER TABLE `places_booking`
  ADD CONSTRAINT `places_booking_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `places_booking_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places_categories`
--
ALTER TABLE `places_categories`
  ADD CONSTRAINT `places_categories_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places_categories_translations`
--
ALTER TABLE `places_categories_translations`
  ADD CONSTRAINT `places_categories_translations_place_category_id_foreign` FOREIGN KEY (`place_category_id`) REFERENCES `places_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places_images`
--
ALTER TABLE `places_images`
  ADD CONSTRAINT `places_images_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places_orders`
--
ALTER TABLE `places_orders`
  ADD CONSTRAINT `places_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `places_booking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `places_orders_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `places_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `places_preparations`
--
ALTER TABLE `places_preparations`
  ADD CONSTRAINT `places_preparations_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `places_preparations_preparations_id_foreign` FOREIGN KEY (`preparations_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `preparations_order`
--
ALTER TABLE `preparations_order`
  ADD CONSTRAINT `preparations_order_preparation_booking_id_foreign` FOREIGN KEY (`preparation_booking_id`) REFERENCES `preparation_booking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `preparations_order_services_sub_sub_category_id_foreign` FOREIGN KEY (`services_sub_sub_category_id`) REFERENCES `sub_subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `preparation_booking`
--
ALTER TABLE `preparation_booking`
  ADD CONSTRAINT `preparation_booking_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `providers`
--
ALTER TABLE `providers`
  ADD CONSTRAINT `providers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ratings`
--
ALTER TABLE `ratings`
  ADD CONSTRAINT `ratings_place_id_foreign` FOREIGN KEY (`place_id`) REFERENCES `places` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `ratings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_service_category_id_foreign` FOREIGN KEY (`service_category_id`) REFERENCES `services_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_service_sub_sub_category_id_foreign` FOREIGN KEY (`service_sub_sub_category_id`) REFERENCES `sub_subcategories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_service_subcategory_id_foreign` FOREIGN KEY (`service_subcategory_id`) REFERENCES `services_subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services_categories`
--
ALTER TABLE `services_categories`
  ADD CONSTRAINT `services_categories_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services_cat_translations`
--
ALTER TABLE `services_cat_translations`
  ADD CONSTRAINT `services_cat_translations_service_category_id_foreign` FOREIGN KEY (`service_category_id`) REFERENCES `services_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services_orders`
--
ALTER TABLE `services_orders`
  ADD CONSTRAINT `services_orders_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `preparation_booking` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_orders_provider_id_foreign` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `services_subcategories`
--
ALTER TABLE `services_subcategories`
  ADD CONSTRAINT `services_subcategories_service_category_id_foreign` FOREIGN KEY (`service_category_id`) REFERENCES `services_categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_provider_request`
--
ALTER TABLE `service_provider_request`
  ADD CONSTRAINT `service_provider_request_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `service_subcat_translation`
--
ALTER TABLE `service_subcat_translation`
  ADD CONSTRAINT `service_subcat_translation_service_subcategory_id_foreign` FOREIGN KEY (`service_subcategory_id`) REFERENCES `services_subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_subcategories`
--
ALTER TABLE `sub_subcategories`
  ADD CONSTRAINT `sub_subcategories_service_subcategory_id_foreign` FOREIGN KEY (`service_subcategory_id`) REFERENCES `services_subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `sub_subcategories_trans`
--
ALTER TABLE `sub_subcategories_trans`
  ADD CONSTRAINT `sub_subcategories_trans_sub_sub_category_id_foreign` FOREIGN KEY (`sub_sub_category_id`) REFERENCES `sub_subcategories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `verifications`
--
ALTER TABLE `verifications`
  ADD CONSTRAINT `verifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
