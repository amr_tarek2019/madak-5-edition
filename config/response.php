<?php

return
    [
        'email_exist' =>
            [
                'ar' => 'البريد الإلكتروني موجود من قبل',
                'en' => 'Email already exists',
            ],
        'phone_exist' =>
            [
                'ar' => 'الهاتف موجود من قبل',
                'en' => 'Phone already exists',
            ],
        'registered' =>
            [
                'ar' => 'تم التسجيل بنجاح,الرجاء التفعيل',
                'en' => 'Registered Successfully,please activate',
            ],
        'activated' =>
            [
                'ar' => 'تم التفعيل بنجاح',
                'en' => 'Activated Successfully',
            ],
        'invalid_data' =>
            [
                'ar' => 'بيانات خاطئة',
                'en' => 'Invalid Data',
            ],
        'user_not_found' =>
            [
                'ar' => 'مستخدم غير موجود',
                'en' => 'User does\'nt exist',
            ],
        'user_not_verified' =>
            [
                'ar' => 'مستخدم غير مفعل',
                'en' => 'User not active',
            ],
        'invalid_email' =>
            [
                'ar' => 'خطأ في البريد الإلكتروني',
                'en' => 'Invalid Email',
            ],
        'invalid_password' =>
            [
                'ar' => 'خطأ في كلمة المرور',
                'en' => 'Invalid Password',
            ],
        'invalid_oldpassword' =>
            [
                'ar' => 'خطأ في كلمة المرور القديمه',
                'en' => 'Invalid Old Password',
            ],
        'password_changed' =>
            [
                'ar' => 'تم إعادة تعيين كلمة المرور بنجاح',
                'en' => 'Password has been reset successfully',
            ],
        'invalid_code' =>
            [
                'ar' => 'كود غير صحيح',
                'en' => 'Invalid Code',
            ],
        'logged_in' =>
            [
                'ar' => 'تم الدخول',
                'en' => 'Logged In',
            ],
        'code_sent' =>
            [
                'ar' => 'تم إرسال الكود',
                'en' => 'Code Sent Successfully',
            ],
        'all_categories' =>
        [
            'ar' => 'كل الفئات',
            'en' => 'all categories',
        ],
        'all_subcategories' =>
            [
                'ar' => 'كل الفئات الفرعيه',
                'en' => 'all sub categories',
            ],
        'all_services' =>
        [
            'ar' => 'كل الخدمات',
            'en' => 'all services',
        ],
        'all_cities' =>
            [
                'ar' => 'كل المدن فى دوله واحده',
                'en' => 'all cities in one country',
            ],
        'done' =>
            [
                'ar' => 'تم',
                'en' => 'Done',
            ],
        'suggestion_sent' =>
            [
                'ar' => 'تم الارسال بنجاح',
                'en' => 'Sent Successfully',
            ],
        'all_notifications' =>
            [
                'ar' => 'كل الاشعارات',
                'en' => 'all notificaions ',
            ],
        'success' =>
            [
                'ar' => 'تمت العملية بنجاح',
                'en' => 'Done Successfully',
            ],
        'failed' =>
            [
                'ar' => 'فشلت العملية',
                'en' => 'Operation Failed',
            ],
        'order_status' =>
            [
                'ar' => 'تم تعديل حالة الطلب بنجاح',
                'en' => 'Order Status Updated Sucessfully',
            ],
        'order_not_found' =>
            [
                'ar' => 'لا يوجد لديك طلبات',
                'en' => 'you don\'nt have orders yet',
            ],
        'user_already_verified' =>
            [
                'ar' => 'هذا المستخدم مفعل مسبقا',
                'en' => 'This User Is Verified Already',
            ],
        'user_profile' =>
            [
                'ar' => 'البيانات الشخصيه',
                'en' => 'profile data',
            ],
        'Jwt_expired' =>
            [
                'en' => 'Session Is Expired',
                'ar' => 'تم انتهاء رمز الدخول'
            ],
        'invalid_date' =>
            [
                'ar' => 'خطأ فى التاريخ',
                'en' => 'invalid date',
            ],
        'stored' =>
            [
                'ar' => 'تمت الإضافة بنجاح',
                'en' => 'Stored Successfully',
            ],
        'updated' =>
            [
                'ar' => 'تم التعديل بنجاح',
                'en' => 'Updated Successfully',
            ],
        'deleted' =>
            [
                'ar' => 'تمت الحذف بنجاح',
                'en' => 'Deleted Successfully',
            ],
        'error' =>
            [
                'ar' => 'حدث خطأ',
                'en' => 'Error Had Occurred',
            ],
        'confirmed' =>
            [
                'ar' => 'تمت الموافقة',
                'en' => 'Confirmed',
            ],
        'declined' =>
            [
                'ar' => 'مرفوض',
                'en' => 'Declined',
            ],
        'please_wait' =>
            [
                'ar' => 'تم الطلب بنجاح,سيتم إرسال الإشعار قبل ساعة من الوقت المحدد,الرجاء الإنتظار',
                'en' => 'Requested Successfully,notifications will be sent 1-hour before the time submitted,please wait',
            ],
        'search_resaults' =>
            [
                'ar' => 'نتائج البحث',
                'en' => 'search resaults',
            ],
        'one_shop' =>
            [
                'ar' => 'بيانات متجر واحد',
                'en' => 'one shop data',
            ],
        'no_one_shop' =>
            [
                'ar' => 'لا يوجد متجر ',
                'en' => 'no shop data',
            ],
        'rate_done' =>
            [
                'ar' => 'تم التقيم',
                'en' => 'rate done',
            ],
        'rate_error' =>
            [
                'ar' => 'خطأ لقد قيمت هذا الطلب مسبقا',
                'en' => 'Error you already rated this order',
            ],
        'request_sent'=>
            [
                'ar'=>'تم ارسال طلب تقديم خدمة بنجاح',
                'en'=>'service provider request sent successfully'
            ],
        'all_issues'=>
        [
            'ar'=>'كل المشكلات',
            'en'=>'all issues'
        ],
        'contact_message_sent'=>
            [
                'ar' => 'تم الارسال بنجاح',
                'en' => 'Sent Successfully',
            ],
        'all_departments'=>
        [
            'ar'=>'كل الاقسام',
            'en'=>'all departments'
        ],
        'place_added'=>
        [
            'ar'=>'تم اضافة مكان بنجاح',
            'en'=>'place added successfully'
        ],
        'service_added'=>
        [
            'ar'=>'تم اضافة الخدمة بنجاح',
            'en'=>'service added successfully'
        ],
        'all_subsubcategories'=>
        [
            'ar'=>'جميع الفئات الفرعية',
            'en'=>'all sub subcategories'
        ],
        'all_provider_places_categories'=>
        [
            'ar'=>'جميع فئات مزود الخدمة',
            'en'=>'all provider places categories'
        ],
        'all_places_categories_items'=>
        [
            'ar'=>'جميع فئات فئات الأماكن',
            'en'=>'all places categories items'
        ],
        'place_details'=>
        [
            'ar'=>'تفاصيل المكان',
            'en'=>'place details'
        ],
        'all_services_subcategories'=>
        [
            'ar'=>'جميع الفئات الفرعية للخدمات',
            'en'=>'all services subcategories'
        ],
        'all_services_subsubcategories'=>
        [
            'ar'=>'جميع الفئات الفرعية للخدمات',
            'en'=>'all services sub subcategories'
        ],
        'all_services_items_of_subsubcategories'=>
        [
            'ar'=>'جميع بنود الخدمات للفئات الفرعية الفرعية',
            'en'=>'all services items of sub subcategories'
        ],
        'service_details'=>
        [
            'ar'=>'تفاصيل الخدمة',
            'en'=>'service details'
        ],
        'all_orders'=>
        [
            'ar'=>'جميع الطلبات',
            'en'=>'all orders'
        ],
        'place_order_details'=>
            [
                'ar'=>'تفاصيل مكان الطلب',
                'en'=>'place order details'
            ],
        'preparations_order_details'=>
        [
            'ar'=>'تفاصيل أمر الاستعدادات',
            'en'=>'preparations order details'
        ],
        'all_preparations_categories'=>
        [
            'ar'=>'جميع فئات الاستعدادات',
            'en'=>'all preparations categories'
        ],
        'all_department_items'=>
        [
            'ar'=>'جميع بنود القسم',
            'en'=>'all department items'
        ],
        'one_item'=>
        [
            'ar'=>'شيء واحد',
            'en'=>'one item'
        ],
        'result'=>
        [
            'ar'=>'نتيجة',
            'en'=>'result'
        ],
        'all_places'=>
        [
            'ar'=>'جميع الأماكن',
            'en'=>'all places'
        ],
        'place_reviews_details'=>
        [
            'ar'=>'تفاصيل مكان الاستعراضات',
            'en'=>'place reviews details'
        ],
        'order_details'=>
        [
            'ar'=>'تفاصيل الطلب',
            'en'=>'order details'
        ],
        'all_preparations_subcategories'=>
        [
            'ar'=>'جميع الفئات الفرعية الاستعدادات',
            'en'=>'all preparations subcategories'
        ],
        'all_preparations_subsubcategories'=>
        [
            'ar'=>'جميع الفئات الفرعية للتحضيرات الفرعية',
            'en'=>'all preparations sub subcategories'
        ]















    ];
